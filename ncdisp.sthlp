{smcl}
{*}
{hline}
{title:Title}
{phang}
{bf:ncdisp} {hline 2} Display the structure of a NetCDF file and retrieve information about a specific variable

{hline}

{title:Syntax}
{phang}
{cmd:ncdisp} {it:varname} {cmd:using} {it:file(string)}

{title:Description}
{pstd}
{cmd:ncdisp} is used to display the structure of a NetCDF file and retrieve information about a specific variable. It reads the file path provided as an argument and prints the structure of the NetCDF file along with details about the specified variable.

{title:Arguments}
{phang}
{opt varname} specifies any valid Stata expression.

{phang}
{opt file} specifies the path to the NetCDF file.

{title:Returns}
{phang}
{opt varname} returns the name of the variable.

{phang}
{opt dimensions} returns the dimensions of the variable.

{phang}
{opt coordinates} returns the coordinate axes of the variable.

{phang}
{opt datatype} returns the data type of the variable.

{title:Examples}
{phang}
{cmd:. ncdisp varname using "path/to/file.nc"}

{title:Author}
{phang}
Unknown

{title:Date}
{phang}
2024-07-28