* Friday, May 24, 2024 at 17:39:40
/*
    setproxy.ado
    
    This Stata program sets the HTTP proxy settings for Stata. It allows the user to specify the proxy server and port number.
    
    Syntax:
        setproxy on ip:port
        setproxy on port
        setproxy on
        setproxy off
    
    Arguments:
        - on: Specifies that the proxy settings should be turned on.
        - ip:port: Specifies the port number of the proxy server.
        - port: Specifies the port number of the proxy server. If the IP address is not specified, the default is localhost.
        - off: Specifies that the proxy settings should be turned off.
    
    Example usage:
        setproxy on 8080
        setproxy on 127.0.0.1:8080
        setproxy on
        setproxy off
    
    Author:
        Kerry Du
        kerrydu@xmu.edu.cn
    
    Version:
        1.0
    
    Date:
        Friday, May 24, 2024 at 17:39:53
*/

cap program drop setproxy
program define setproxy
version 16
args on port
if "`on'" == "on" & "`port'" != "" {
    gettoken host port : port, parse(":")
    if "`port'" == "" {
        local port `host'
    }
    else{
        gettoken pars port:port, parse(":")
        if "`pars'" != ":" {
            di as err "server ip address and port number must be separated by a colon(:)"
            exit 198
        }
    }
    confirm integer number `port'
    set httpproxy on
    if "`host'" == "" set httpproxyhost "localhost"
    else set httpproxyhost "`host'"
    writeport `host' `port'
    set httpproxyport `port'
}
else if "`on'" == "on" & "`port'" == "" {
    set httpproxy on
    readhostport
    set httpproxyhost "`r(host)'"
    set httpproxyport `r(port)'
}
else{
    set httpproxy off
}

end

cap program drop writeport
program define writeport
version 16
args host port
if "`port'" == "" {
    local port `host'
    local host localhost
}
if `"`port'"' == "" {
    di as err "port number not specified"
    exit 198
}
if `port'<0 {
    di as err "port number must be a positive integer"
    exit 198
}
local filename =  c(sysdir_plus) + "r/readhostport.ado"
file open myfile using `"`filename'"', write text replace
file write myfile "cap program drop readhostport" _n
file write myfile "program define readhostport, rclass" _n
file write myfile `"return local host  `host'"' _n
file write myfile "return scalar port = `port'" _n
file write myfile "end" _n
file close myfile
end
