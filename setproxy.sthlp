{smcl}
{* Juli 8, 2009 @ 10:42:32 UK}{...}
{cmd:help setproxy}

{hline}


    setproxy.ado
    
    This Stata program sets the HTTP proxy settings for Stata.
    It allows the user to specify the proxy server and port number.
    
    Syntax:
        setproxy on ip:port
        setproxy on port
        setproxy on
        setproxy off
    
    Arguments:
        - on: Specifies that the proxy settings should be turned on.
        - ip:port: Specifies the port number of the proxy server.
        - port: Specifies the port number of the proxy server. 
                If the IP address is not specified, the default is localhost.
        - off: Specifies that the proxy settings should be turned off.
    
    Example usage:
        setproxy on 8080
        setproxy on 127.0.0.1:8080
        setproxy on
        setproxy off
    
    Author:
        Kerry Du
        kerrydu@xmu.edu.cn
    
    Version:
        1.0
    
    Date:
        Friday, May 24, 2024 at 17:39:53