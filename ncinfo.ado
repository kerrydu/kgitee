/*
    Program Name: ncinfo
    Author: Unknown
    Date: 2024-07-28

    Description:
    This program provides information about a NetCDF file. It reads the file path provided as an argument and prints the structure of the NetCDF file.

    Syntax:
    ncinfo anything

    Arguments:
    - anything: The file path of the NetCDF file.

    Example:
    ncinfo "path/to/netcdf/file.nc"
*/
*! 2024-07-28
cap program drop ncinfo
program define ncinfo
version 18
local path `c(sysdir_plus)' 
cap findfile netcdfAll-5.6.0.jar,path(`"`path'"')
if _rc {
    ncreadinit
}
syntax anything
removequotes,file(`anything')
local file `r(file)'
di _n
java clear
java: /cp "netcdfAll-5.6.0.jar"
java: /open "StNetCDFReader.java"
java: NetCDFReader.printNetCDFStructure("`file'");

end

cap program drop removequotes
program define removequotes,rclass
version 16
syntax, file(string) 
return local file `file'
end


program define ncreadinit

findfile netcdfAll-5.6.0.jar
local fn  `"`r(fn)'"'
local path `c(sysdir_plus)' 
copy `fn' `"`path'/netcdfAll-5.6.0.jar"',replace
findfile StNetCDFReader.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'/StNetCDFReader.java"',replace

end