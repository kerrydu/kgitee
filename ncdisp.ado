/*
    Program: ncdisp
    Author: Unknown
    Date: 2024-07-28

    Description:
    This program is used to display the structure of a NetCDF file and retrieve information about a specific variable.

    Syntax:
    ncdisp anything using file(string)

    Arguments:
    - anything: Any valid Stata expression.
    - file: The path to the NetCDF file.

    Returns:
    - varname: The name of the variable.
    - dimensions: The dimensions of the variable.
    - coordinates: The coordinate axes of the variable.
    - datatype: The data type of the variable.

    Example:
    ncdisp varname using "path/to/file.nc"
*/
*! 2024-07-28
cap program drop ncdisp
program define ncdisp,rclass
version 18

local path `c(sysdir_plus)' 
cap findfile netcdfAll-5.6.0.jar,path(`"`path'"')
if _rc {
    ncreadinit
}

syntax anything using/ 
removequotes,file(`anything')
local varname `r(file)'
removequotes,file(`using')
local file `r(file)'
//di _n 
java clear
java: /cp "netcdfAll-5.6.0.jar"
java: /open "StNetCDFReader.java"
java: NetCDFReader.printVarStructure("`file'","`varname'");

return local varname `varname'
return local dimensions `dimensions' 
return local coordinates `coordAxes' 
return local datatype `datatype'
end

cap program drop removequotes
program define removequotes,rclass
version 16
syntax, file(string) 
return local file `file'
end


program define ncreadinit

findfile netcdfAll-5.6.0.jar
local fn  `"`r(fn)'"'
local path `c(sysdir_plus)' 
copy `fn' `"`path'/netcdfAll-5.6.0.jar"',replace
findfile StNetCDFReader.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'/StNetCDFReader.java"',replace

end
