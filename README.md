 ##  kgitee 

​          list/install Stata packages in gitee.com/kerrydu

* 安装方法

```
net install kgitee, from(https://gitee.com/kerrydu/kgitee/raw/master) replace
```

* 使用方法

  索引gitee.com/kerrydu上的所有命令

  > kgitee 

  通过kgitee安装Stata命令

  > kgitee pkgname, [replace force]

​	其中，pkgname为kgitee显示的程序包名称，replace和force是可选的选项。

* 致谢

  > kgitee所提供的程序是国家自然科学基金项目（编号：72473119，72074184 和 71603148）开展过程中所开发的，我们对国家自然科学基金委的资助表示感谢。在此，我们也感谢读者对程序bugs的反馈和建议。

  

---

* 目前提供的命令：

  >    1. kgitee: list/install Stata packages in gitee.com/kerrydu 
  >    2. gitee: Install Stata package in Gitee 
  >    3. xtplfc: Stata  module to estimate partially linear functional-coefficient panel data models  
  >    4. mepi: Malmquist Energy Productivity Index in Stata 
  >    5. gtfpch: Total Factor Productivity with Undesirable Outputs in Stata 
  >    6. malmq2: Malmquist Productivity index in Stata  
  >    7. sbmeff: Slacks-based Measure of Efficiency in Stata  
  >    8. ddfeff: Directional Distance Function for Efficiency/Productivity Analysis in Stata 
  >    9. installpkg: install Stata packages from zipfiles or pkgfiles in directory 
  >    10. txls: transform excel xls/xlsx files 
  >    11. xlstocsv:  convert excel xls/xlsx files to csv files 
  >    12. calk: Calculate capital stock by the perpetual inventory method 
  >    13. calrgdp: Calculate real GDP  
  >    14. segmindex: Estimate regional market segmentation index  
  >    15. permute2: Simulation of estimating treatment effect by randomly matching treatment policy with individuals  
  >    16. placebotest: placebosim for unblanced panel data  
  >    17. dflp: Estimating input/output/directional distance using LP techniques  
  >    18. updatecmd: an automatic routine  to update user-written package  
  >    19. deadual: SBM/NDDF dual models 
  >    20. sopen: open anything with Stata
  >    21. mwithdistance: merge data within given distance
  >    22. xtsfsp: Fitting Spatial Panel SF models in STATA
  >    23. hanoitower: Play Tower of Hanoi in Stata
  >    24. sdsfe: Spatial Durbin stochastic frontier model with endogeneous variables
  >    25. spsfe: Spatial autoregressive stochastic frontier model with endogeneous variables
  >    26. efficiencybook: Installing packages for efficiencybook
  >    27. labone: labels the variables using the contents from the specified rows in the data
  >    28. lmdi: Logarithmic Mean Divisia Index (LMDI) Decomposition
  >    29. translog: creates new variables for a translog function
  >    30. reshape3: converts data from wide to long and vice-versa
  >    31. psecta: logt test and Club convergence clustering (logtreg/psecta)
  >    32. setproxy: a fast way to set httpproxy in Stata
  >    33. framerge: 1:1, m:1, 1:m and m:m link between frames and copy variables from the using frames in Stata
  >    34. ncread: read nc files in Stata
  >    35. stgeocom: geographical computation in Stata

---

* 更新历史<Important>
