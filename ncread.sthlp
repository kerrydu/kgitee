{smcl}
{*}
{hline}
{title:Title}
{phang}
{bf:ncread} {hline 2} Read variables from a NetCDF file into Stata

{hline}

{title:Syntax}
{phang}
{cmd:ncread} {it:varname} {cmd:using} {it:filename} [{cmd:,} {opt NOCheck} {opt Count(numlist integer)} {opt Origin(numlist integer >0)}]

{title:Description}
{pstd}
{cmd:ncread} reads a variable from a NetCDF file and imports them into Stata. It supports reading variables by sections using the {opt origin()} and {opt count()} options.

{title:Options}
{phang}
{opt NOCheck} specifies whether to perform checks on the size of the variable. If specified, the program will not check the size and will import the variable as a single observation.

{phang}
{opt Count(numlist integer)} specifies the number of elements to read along each dimension. Must be used with the {opt Origin} option.

{phang}
{opt Origin(numlist integer >0)} specifies the starting index along each dimension. Must be used with the {opt Count} option.

{title:Remarks}
{pstd}
This program requires the "netcdfAll-5.6.0.jar" file to be in the Stata classpath.

{pstd}
The variable size limit is set to 50000. If the size exceeds this limit, it is recommended to read variables by sections using the {opt origin()} and {opt count()} options.

{pstd}
The {opt NOCheck} option allows ignoring the size limit, but it is not recommended and should be used with caution.

{title:Examples}
{phang}
{cmd:. ncread myvar using "path/to/file.nc", count(10 20) origin(1 1)}

{title:Author}
{phang}
Unknown

{title:Date}
{phang}
2024-07-28