*! version 1.0, By Kerry Du
* 匹配给定距离内的观测站数据
* master数据是 location_id  latitude longitude的截面数据
* using数据是  station_id  latitude longitude  timevars pollutiondata/weatherdata
* 语法 mwithdistance location_id  latitude longitude using xxx.dta, n(station_id  latitude longitude) d(80) time(year month date) 
* speed up the program: 限定using数据的时间窗口，e.g., 只保留 2018-2019
cap program drop mwithdistance
program define mwithdistance
version 14
cap which geonear
if _rc ssc install geonear 
syntax varlist using/, Neighbors(string) Distance(numlist >0 min=1 max=1) [time(namelist) MILE NEARcount(numlist)]
if "`nearcount'"=="" local nearcount 0
local mid: word 1 of `varlist'
local uid: word 1 of `neighbors'

preserve 

qui use `using',clear
if "`time'"!=""{
	tempvar tvar 
	qui egen `tvar' = group(`time')
	qui su `tvar'
	local T =r(max)
}
else{
	local T =1
}

qui keep `neighbors'
foreach c in `neighbors'{
	qui drop if missing(`c')
}
qui duplicates drop `uid', force
tempfile using2 
qui save `using2',replace
restore 

qui keep `varlist'
qui geonear `varlist' using `using2', neighbors(`neighbors') within(`distance') `miles'  long nearcount(`nearcount')
//qui kxnear `varlist' using `using2', neighbors(`neighbors') d(`distance') `miles'  
tempfile matchok
qui expand `T'
qui bys `mid' `uid': gen _t_var=_n
qui save `matchok',replace

qui use `using',clear

if "`time'"==""{
	tempvar time 
	qui gen int `time'=1
} 

qui egen _t_var=group(`time')

qui merge 1:m _t_var `uid' using `matchok', keep(3) nogen

order `mid' `uid' `time'

qui drop _t_var 

end


