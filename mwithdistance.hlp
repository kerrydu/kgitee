{smcl}
{* *! version 2.0.0  21feb2012}{...}
{cmd:help mwithdistance}
{hline}

{title:Title}

{phang}
{bf:mwithdistance} {hline 2} Finds nearest neighbors within given distance using geodetic distances.


{title:Syntax}

{p 8 16 2}
{cmd:mwithdistance} 
{it:baseid baselat baselon} 
{cmd:using} {it:nborfile}
{cmd:,}
{opt n:eighbors(nborid nborlat nborlon)}
{opt d:istance(#)}
[{it:time(namelist)} {opt near:count(#)} {it:miles}]

{synoptset 20 tabbed}{...}
{synopthdr:options}
{synoptline}
{synopt:{opt d:istance(#)}}return neighbors within a distance of {it:#}
from each {it:baseid}{p_end}
{synopt:{opt near:count(#)}}find the {it:#} nearest neighbors{p_end}
{synopt:{opt time(namelist)}}specify variables for time periods in the using data{p_end}
{synopt:{opt miles}}return distances in miles{p_end}
{synoptline}
{p2colreset}{...}


{title:Description}

{pstd}
For each location identified by {it:baseid} and with coordinates {it:baselat}
{it:baselon}, {cmd:mwithdistance} finds in {it:nborfile}, a Stata-format dataset, the
nearest neighbor(s), identified by {it:nborid} and located at {it:nborlat}
{it:nborlon}. Coordinates must be in signed decimal degrees, positive for north
and east, and negative for south and west. Latitudes ({it:baselat} and
{it:nborlat}) range from -90 to 90 and longitudes ({it:baselon} and
{it:nborlon}) from -180 to 180.

{pstd}
{cmd:mwithdistance} extend geonear command to the context that the using data is panel data. 
{stata ssc des geonear:geonear} is a standalone implementation of distance routines and is available from SSC. All distances are in kilometers and all distance variables
are prefixed with "{it:km_to_}" unless the {opt mi:les} option is specified (the
prefix changes to "{it:mi_to_}").


{title:Examples}

{pstd}
Simulate a dataset of 2000 Census Block Group centroids for Colorado.

        {cmd:.} {stata clear}
        {cmd:.} {stata set seed 123456}
        {cmd:.} {stata set obs 3278}
        {cmd:.} {stata gen bgid = _n}
        {cmd:.} {stata gen double bglat = 37 + (41 - 37) * uniform()}
        {cmd:.} {stata gen double bglon = -109 + (109 - 102) * uniform()}
        {cmd:.} {stata tempfile bg}
        {cmd:.} {stata save "`bg'"}
        
{pstd}
Create a new dataset that contains locations of cell towers in the State of
Colorado.

        {cmd:.} {stata clear}
        {cmd:.} {stata set obs 1000}
        {cmd:.} {stata gen ctid = _n}
        {cmd:.} {stata gen double ctlat = 37 + (41 - 37) * uniform()}
        {cmd:.} {stata gen double ctlon = -109 + (109 - 102) * uniform()}
        {cmd:.} {stata expand 5} 
        {cmd:.} {stata bys ctid: gen t=_n}        
        {cmd:.} {stata tempfile cell}
        {cmd:.} {stata save "`cell'"}


{pstd}
To find all towers within 10km of each tower:

        {cmd:.} {stata use "`cell'", clear}
        {cmd:.} {stata rename ctid ctid0}
        {cmd:.} {stata mwithdistance ctid0 ctlat ctlon using "`cell'", n(ctid ctlat ctlon) d(10)}


{pstd}
To find the nearest neighbor for each of these cell towers within 50km:

        {cmd:.} {stata mwithdistance ctid0 ctlat ctlon using "`cell'", n(ctid ctlat ctlon) d(10) near(1)}

{pstd}

{title:References and acknowledgements}

{pstd}
Many thanks to Chris Veness for the best web pages on how to compute
geodetic distances:

        {browse "http://www.movable-type.co.uk/scripts/latlong-vincenty.html"}
        {browse "http://www.movable-type.co.uk/scripts/latlong.html"}
        
{pstd}
The definition of the World Geodetic System 1984 is available from

        {browse "http://earth-info.nga.mil/GandG/publications/tr8350.2/wgs84fin.pdf"}
   
{pstd}
See Appendix A.1 for a list of reference ellipsoids. 

{pstd}
C. F. F. Karney, Geodesics on an ellipsoid of revolution, Feb. 2011; 
preprint {browse "http://arxiv.org/abs/1102.1215":arxiv:1102.1215.}

{pstd}
R. W. Sinnott, "Virtues of the Haversine", {it:Sky and Telescope} 68 (2), 159 (1984).
Thanks to the University of Michigan's Shapiro Science Library.

{pstd}
C. M. Thomas and W. E. Featherstone, 
Validation of Vincenty's Formulas for the Geodesic Using a New 
Fourth-Order Extension of Kivioja's Formula, {it:J. Surv. Engrg.} Volume 131, 
Issue 1, pp. 20-26 (February 2005), available for download from:

        {browse "http://www.cage.curtin.edu.au/~will/thomas-featherstone.pdf"}

{pstd}
Vincenty, T. (1975) Direct and inverse solutions of geodesics on the ellipsoid 
with application of nested equations, {it:Survey Review} 22(176): 88-93 is available
from:

        {browse "http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf"}


{title:Author}

{pstd}Robert Picard{p_end}
{pstd}picard@netbox.com{p_end}
