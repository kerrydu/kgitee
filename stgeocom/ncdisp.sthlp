{smcl}
{*}
{hline}
{title:Title}

{phang}
{bf:ncdisp} {hline 2} Display the structure of a NetCDF file and retrieve information about a specific variable

{hline}

{title:Syntax}

{phang}
{cmd:ncdisp} {it:varname} {cmd:using} {it:file(string)}

{title:Description}

{pstd}
{cmd:ncdisp} is used to display the information about 
a specific variable in a nc file. It reads the file path provided as an argument.

{title:Dependencies}

{pstd}
The {cmd:ncdisp} command requires the NetCDF Java library. Use ncread_init for setting up.


{title:Options}

{phang}
{opt varname} specifies any valid Stata expression.

{phang}
{opt file} specifies the path to the NetCDF file.

{title:Stored results}

{phang}
ncdisp stores the following in r():

{phang}
local

{phang}
{opt r(varname)} returns the name of the variable.

{phang}
{opt r(dimensions)} returns the dimensions of the variable.

{phang}
{opt r(coordinates)} returns the coordinate axes of the variable.

{phang}
{opt r(datatype)} returns the data type of the variable.

{title:Examples}

{phang}
{cmd:. ncdisp tas using "Hunan.nc"}

{title:Author}

{pstd}Kerry Du{p_end}
{pstd}Xiamen University{p_end}
{pstd}Email: kerrydu@xmu.edu.cn{p_end}

{title:Also see}

{psee}
Online:  {help ncread}
{p_end}