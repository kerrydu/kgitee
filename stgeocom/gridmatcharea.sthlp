{title:gridmatcharea}

{phang}
{bf:gridmatcharea} {hline 2} Matches coordinate points in the current dataset to areas in another frame

{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:gridmatcharea} {it:varlist}, {opt u:singframe(framename)}

{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:Required}
{synopt:{opt u:singframe(framename)}}Specify the name of the frame containing grid areas{p_end}
{synoptline}

{marker description}{...}
{title:Description}

{pstd}
{cmd:gridmatcharea} matches coordinate points in the current dataset to grid areas in a specified frame. The current dataset must contain only two coordinate variables, while the specified frame must contain an ID variable and two coordinate variables.

{marker options}{...}
{title:Options}

{phang}
{opt usingframe(framename)} Specifies the name of the frame containing grid areas. This frame must contain three variables: an ID variable and two coordinate variables.

{marker remarks}{...}
{title:Remarks}

{pstd}
This command requires a Java runtime environment and the jts-core-1.19.0.jar library file. Ensure these dependencies are properly installed and configured.

{marker examples}{...}
{title:Examples}

{phang}{cmd:. gridmatcharea lat lon, usingframe(area_data)}{p_end}

{marker author}{...}
{title:Author}

{pstd}Your Name{p_end}
{pstd}Your affiliation{p_end}
{pstd}Your email{p_end}
