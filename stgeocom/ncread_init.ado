
/*

cap findfile path_ncreadjar.ado 
if _rc {
	di as error "jar path NOT specified, use ncread_init for setting up"
	exit
	
}

path_ncreadjar
local path `r(path)'

cap findfile netcdfall-5.6.0.jar, path(`"`path'"')
if _rc {
	di as error "netcdfall-5.6.0.jar NOT found"
	di as error "use ncread_init for re-initializing"
	di as error "make sure netcdfall-5.6.0.jar exists in your specified directory"
	exit
}

adopath ++ `"`path'"'


*/
/*
	Program: ncread_init
	Version: 16.0

	Description:
	------------
	This program initializes the environment for reading NetCDF files using the netcdfAll-5.6.0.jar library. 
	It optionally downloads the jar file to a specified directory and sets up the path for the jar file.

	Syntax:
	-------
	ncread_init [anything], [download dir(string)]

	Options:
	--------
	download: 
		If specified, the program will download the netcdfAll-5.6.0.jar file.
	dir(string): 
		The directory where the jar file will be downloaded. If not specified, the current working directory is used.

	Example:
	--------
	. ncread_init, download dir("C:/mydir")

	Internal Program: wrtjarpath
	----------------------------
	This internal program writes the path of the jar file to an ado file.

	Syntax:
	-------
	wrtjarpath [anything], jar(string) adoname(string)

	Options:
	--------
	jar(string): 
		The name of the jar file.
	adoname(string): 
		The name of the ado file to be created.

	Example:
	--------
	. wrtjarpath "C:/mydir", jar(netcdfAll-5.6.0.jar) adoname(ncreadjar)
*/

program define ncread_init
version 16.0
syntax [anything] , [download dir(string)]

if "`download'"!=""{

	if `"`dir'"'!="" {
		mata: st_numscalar("r(exist)",direxists(`"`dir'"'))
		local exist = r(exist)
		if `exist'==0{
			di as error "Path Setting Failed..."
			di as error "dir {`dir'} NOT exist"
			exit 198
		}
	}
	if `"`dir'"'=="" local dir = c(pwd)
	di `"Downloading netcdfAll-5.6.0.jar..."'
	copy https://downloads.unidata.ucar.edu/netcdf-java/5.6.0/netcdfAll-5.6.0.jar `"`dir'/netcdfAll-5.6.0.jar"'
	if `"`anything'"'!=""{
        di as  "Warning: The first specified path is ignored as the jar file is downloaded to {`dir'}"
	}
	local anything `dir'
}

wrtjarpath `"`anything'"', jar(netcdfAll-5.6.0.jar) adoname(ncreadjar)

end


cap program drop wrtjarpath
program define wrtjarpath
version 16.0
syntax [anything] ,[jar(string)] adoname(string)

// confirm name `adoname'

if `"`anything'"'==""{
	local anything `c(pwd)'
}
else{
	mata: st_numscalar("r(exist)",direxists(`"`anything'"'))
    local exist = r(exist)
	if `exist'==0{
		di as error "Path Setting Failed..."
		di as error "dir {`anything'} NOT exist"
		exit 198
	}
}

if `"`jar'"'!==""{
	local fileexist = fileexists(`"`anything/`jar''"')
	if `fileexist'==0{
		di as error "Path Setting Failed..."
		di as error `"`jar' NOT found in {`anything'}"'
		exit
	}
	
}



local filename =  c(sysdir_plus) + "p/path_`adoname'.ado"
file open myfile using `"`filename'"', write text replace
file write myfile "cap program drop path_`adoname'" _n
file write myfile "program define path_`adoname', rclass" _n
file write myfile "version 16.0" _n
file write myfile `"return local path `anything'"' _n
file write myfile "end" _n
file close myfile
end
