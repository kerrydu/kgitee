cap program drop areaavg
program define areaavg
    version 18
    gettoken genvar 0:0, p(=)
    gettoken eq 0:0, p(=) 
    if "`eq'" != "" {
        di as error "Syntax error"
        exit 198
    }
    confirm newvar `genvar'

    syntax varlist(max=1 min=1 numeric),[ by(varlist) adjlat(varname)]
    if `"`by'"' == "" {
        tempvar by 
        qui gen byte `by' = 1
    }

    
    if `"`adjlat'"' != "" {
        tempvar weight
        local e2 = 0.00669437999014 // Earth's eccentricity squared
        qui gen double `weight' = cos(`adjlat'*_pi/180) * sqrt(1 - `e2' * sin(`adjlat'*_pi/180)^2)
        tempvar sumweight
        qui bys `by': egen double `sumweight' = total(`weight')
        qui replace `weight' = `weight'*`varlist'/`sumweight'
        qui bys `by': gen double `genvar' = total(`weight')
    }
    else {
        qui bys `by': gen double `genvar' = total(`varlist')
    }

    label var `genvar' "area average of `varlist'"

end