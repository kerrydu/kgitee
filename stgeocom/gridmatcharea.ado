cap program drop gridmatcharea
program define gridmatcharea 
//stgeocominit
version 18
syntax varlist(max=2 min=2 numeric), Usingframe(string)
order `varlist'
qui describe,simple
if  r(k) != 2 {
    di as error "current dataset must only have two coordinate variables"
    exit 198
}

local var1 : word 1 of `varlist'
local var2 : word 2 of `varlist'

qui pwf

local pwf = r(currentframe)

qui cwf `usingframe'

cap confirm var `var1'
if _rc {
    di as error "usingframe does not have variable `var1'"
    exit 198
}

cap confirm var `var2'
if _rc {
    di as error "usingframe does not have variable `var2'"
    exit 198
}

qui describe,simple 

if  r(k) != 3 {
    di as error "current dataset should have three variables, id and two coordinate variables"
    exit 198
}

qui ds

local vars `r(varlist)'
local cc `var1' `var2'
local id: list vars - cc 
local var_type : type `id'
if !ustrpos("`var_type'","str") qui tostring `id', replace
local var_type : type `id'
order `id' `var1' `var2'
qui cwf `pwf'
qui gen `var_type' `id' = ""
local usingframe `usingframe'
///////////////java////////////

java clear
java: /cp "jts-core-1.19.0.jar"
java: /open "StgridMatchArea.java"
//java: /open "E:/SynologyDrive/GitteGithub/stncdf/netcdf-java/gridMatchAeraCopy.java"
java: StgridMatchAera.run()

//////////////////////////////

end
