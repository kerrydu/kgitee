/*
    geotools_zonal_stats.ado - Calculate zonal statistics from GeoTIFF for Chinese city polygons

    Syntax:
    -------
    geotools_zonal_stats using rasterfile, shapefile(string) [band(integer 1) idvar(string) namevar(string) clear]
    
    Parameters:
    -----------
    - using: Path to the GeoTIFF file
    - shapefile: Path to the shapefile containing city polygons
    - band: Band number to use for calculation (default is 1)
    - idvar: Name of the variable in shapefile containing city IDs (default is "ID")
    - namevar: Name of the variable in shapefile containing city names (default is "NAME")
    - clear: Clear existing data in memory
*/

cap program drop gzonal_stats
program define gzonal_stats

version 18.0
checkdependencies

// 修改 syntax 行，添加 shpcrs 选项
syntax using/, shpfile(string) [band(integer 1) idvar(string) namevar(string) statistics(string) autofields shpcrs(string) clear]

// 在解析 idvar 和 namevar 之前添加以下代码，使用 autofields 选项自动探测字段
if "`autofields'"=="autofields" | ("`idvar'"=="" & "`namevar'"=="") {
    di as text "Attempting to auto-detect ID and name fields in shapefile..."
    // 在 Java 代码中会处理字段探测
    local idvar "AUTO"
    local namevar "AUTO"
}

// Default variable names if not specified
// if "`idvar'"=="" local idvar "ID"
// if "`namevar'"=="" local namevar "NAME"

// Default statistics is mean if not specified
if "`statistics'"=="" local statistics "mean"
local statistics = lower("`statistics'")

// Validate requested statistics
local valid_stats "mean min max range sum count variance stdev median nodata"
local invalid_stats ""
foreach stat in `statistics' {
    local valid 0
    foreach vstat in `valid_stats' {
        if "`stat'"=="`vstat'" {
            local valid 1
            continue, break
        }
    }
    if !`valid' {
        local invalid_stats "`invalid_stats' `stat'"
    }
}

if "`invalid_stats'" != "" {
    di as error "Warning: Invalid statistics requested: `invalid_stats'"
    di as error "Valid options are: `valid_stats'"
    di as error "Invalid statistics will be ignored."
}

// Ensure at least mean is included if all requested stats were invalid
local valid_requested 0
foreach stat in `statistics' {
    foreach vstat in `valid_stats' {
        if "`stat'"=="`vstat'" {
            local valid_requested 1
            continue, break
        }
    }
}

if !`valid_requested' {
    di as error "No valid statistics requested, defaulting to mean."
    local statistics "mean"
}

// Check if data in memory should be cleared
if "`clear'"!="clear" {
    qui describe
    if r(N) > 0 | r(k) > 0 {
        di as error "Data already in memory, use the clear option to overwrite"
        exit 198
    }
}
else {
    clear
}

// Normalize file paths
local using = subinstr(`"`using'"',"\","/",.)
local shapefile = subinstr(`"`shpfile'"',"\","/",.)

// Display operation information
di as text "Calculating zonal statistics..."
di as text "Raster file: `using'"
di as text "Shapefile: `shapefile'"
di as text "Band: `band'"
di as text "Statistics to calculate: `statistics'"

// 在 Java 方法调用前传递新参数
java: ZonalStatistics.calculateZonalStats("`using'", "`shapefile'", `band', "`idvar'", "`namevar'", "`statistics'", "`shpcrs'")

// Add labels based on the statistics requested
// label variable city_id "City ID"
// label variable city_name "City Name"

// Check if each statistic is in the requested list and label if present
foreach stat in mean min max range sum count variance stdev median nodata {
    cap confirm variable `stat'_value
    if !_rc {
        if "`stat'"=="mean" {
            label variable mean_value "Mean Raster Value"
        }
        else if "`stat'"=="min" {
            label variable min_value "Minimum Raster Value" 
        }
        else if "`stat'"=="max" {
            label variable max_value "Maximum Raster Value"
        }
        else if "`stat'"=="range" {
            label variable range_value "Range of Raster Values"
        }
        else if "`stat'"=="sum" {
            label variable sum_value "Sum of Raster Values"
        }
        else if "`stat'"=="count" {
            label variable count_value "Count of Valid Pixels"
        }
        else if "`stat'"=="variance" {
            label variable variance_value "Variance of Raster Values"
        }
        else if "`stat'"=="stdev" {
            label variable stdev_value "Standard Deviation of Raster Values"
        }
        else if "`stat'"=="median" {
            label variable median_value "Median Raster Value"
        }
        else if "`stat'"=="nodata" {
            label variable nodata_value "Count of NoData Pixels"
        }
    }
}

// Always label these variables if present
cap confirm variable valid_pixels
if !_rc {
    label variable valid_pixels "Number of Valid Pixels"
}

cap confirm variable total_pixels
if !_rc {
    label variable total_pixels "Total Pixels in City Area"
}

cap confirm variable coverage_pct
if !_rc {
    label variable coverage_pct "Percentage of Valid Coverage"
}

di as text "Zonal statistics calculation complete."
di as text "Results dataset contains {result:`=_N'} regions."

end

// Check for required GeoTools JAR dependencies
program define checkdependencies
version 18.0

local jars gt-main-32.0.jar gt-referencing-32.0.jar gt-epsg-hsql-32.0.jar gt-process-raster-32.0.jar
local jars `jars' gt-epsg-extension-32.0.jar gt-geotiff-32.0.jar gt-coverage-32.0.jar
local jars `jars' gt-shapefile-32.0.jar  gt-metadata-32.0.jar gt-api-32.0.jar

local rc 0
foreach jar in `jars'{
    cap findfile `jar'
    if _rc {
        local rc = 1
    }
}

if `rc'{
    path_geotoolsjar
    local path `r(path)'

    foreach jar in `jars' {
        cap findfile `jar', path(`"`path'"')
        if _rc {
            di as error "`jar' NOT found"
            di as error "Use geotools_init for re-initializing Java environment, help geotools_init"
            di as error "Make sure `jar' exists in your specified directory"
            exit
        }
    }

    qui adopath ++ `"`path'"'
}

end

// Java implementation for zonal statistics
java:
/cp gt-metadata-32.0.jar       
/cp gt-api-32.0.jar
/cp gt-main-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-epsg-extension-32.0.jar
/cp gt-geotiff-32.0.jar
/cp gt-coverage-32.0.jar
/cp gt-process-raster-32.0.jar
/cp gt-shapefile-32.0.jar
/cp gt-swing-32.0.jar
/cp jai_core-1.1.3.jar
/cp jai_imageio-1.1.jar
/cp gt-feature-pregeneralized-32.0.jar

// 删除未使用的导入语句，保留必要的导入
import com.stata.sfi.*;
import org.geotools.api.data.*;
import org.geotools.data.shapefile.*;
import org.geotools.data.simple.*;
import org.geotools.gce.geotiff.*;
import org.geotools.coverage.grid.*;
import org.geotools.api.feature.*;
import org.geotools.api.feature.simple.*;
import org.geotools.api.feature.type.PropertyDescriptor;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.api.referencing.crs.*;
import org.geotools.api.referencing.operation.*;
import org.geotools.referencing.*;
import org.geotools.geometry.jts.*;
import org.locationtech.jts.geom.*;
import org.geotools.process.raster.RasterZonalStatistics;
import org.geotools.api.referencing.FactoryException;
import org.geotools.geometry.jts.ReferencedEnvelope;
import java.awt.image.Raster;
import java.io.File;
import java.util.*;
import org.geotools.coverage.GridSampleDimension;
import org.geotools.coverage.grid.GridCoordinates2D;
import java.awt.geom.Point2D;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;

// 删除这些未使用的导入：
// import org.geotools.coverage.processing.*;
// import org.geotools.feature.simple.*;
// import org.geotools.feature.*;
// import org.geotools.coverage.processing.operation.*;
// import org.geotools.referencing.operation.*;
// import org.geotools.coverage.grid.io.*;
// import org.geotools.process.raster.*;
// import org.geotools.api.coverage.processing.Operation;
// import org.geotools.api.parameter.ParameterValueGroup;
// import java.nio.file.Path;
// import java.nio.file.Paths;
// import org.geotools.process.raster.RasterZonalStatistics2;
// import org.geotools.geometry.Position2D;
// import org.geotools.factory.CommonFactoryFinder;
// import org.geotools.geometry.jts.JTSFactoryFinder;
// import org.locationtech.jts.geom.GeometryFactory;
// import org.locationtech.jts.geom.Coordinate;
// import org.locationtech.jts.geom.Point;
// 在 Java 代码的开头添加字符集设置

public class ZonalStatistics {
    // 修改字符集设置部分
    static {
        // 设置默认编码
        System.setProperty("file.encoding", "UTF-8");
        
        // 设置默认 Locale
        try {
            java.util.Locale.setDefault(java.util.Locale.CHINA);
        } catch (Exception e) {
            // 忽略设置 Locale 的错误
        }
    }
    

    

    public static void calculateZonalStats(String rasterPath, String shapefilePath, 
                                          int bandIndex, String idField, String nameField,
                                          String statisticsStr, String shpcrs) {
        try {
            // Validate input files
            if (!new File(rasterPath).exists() || !new File(shapefilePath).exists()) {
                SFIToolkit.errorln("Input file not found");
                return;
            }
            CRS.reset("all");

            // Parse requested statistics
            Set<String> requestedStats = parseRequestedStatistics(statisticsStr);
            boolean calculateMean = requestedStats.contains("mean");
            boolean calculateMin = requestedStats.contains("min");
            boolean calculateMax = requestedStats.contains("max");
            boolean calculateRange = requestedStats.contains("range");
            boolean calculateSum = requestedStats.contains("sum");
            boolean calculateCount = requestedStats.contains("count");
            boolean calculateVariance = requestedStats.contains("variance");
            boolean calculateStdev = requestedStats.contains("stdev");
            boolean calculateMedian = requestedStats.contains("median");
            boolean calculateNodata = requestedStats.contains("nodata");

            // Load raster data
            SFIToolkit.displayln("Loading GeoTIFF: " + rasterPath);
            GeoTiffReader rasterReader = new GeoTiffReader(new File(rasterPath));
            GridCoverage2D coverage = rasterReader.read(null);
            CoordinateReferenceSystem rasterCRS = coverage.getCoordinateReferenceSystem();

            // Try to determine NoData value
            Double noDataValue = getNoDataValue(coverage, bandIndex - 1);
            if (noDataValue != null) {
                SFIToolkit.displayln("NoData value detected: " + noDataValue);
            } else {
                SFIToolkit.displayln("No explicit NoData value detected");
            }

            // Load shapefile
            SFIToolkit.displayln("Loading shapefile: " + shapefilePath);
            ShapefileDataStore dataStore = new ShapefileDataStore(new File(shapefilePath).toURI().toURL());
            dataStore.setCharset(java.nio.charset.Charset.forName("UTF-8")); // 设置字符编码
            SimpleFeatureSource source = dataStore.getFeatureSource();
            SimpleFeatureCollection features = source.getFeatures();
            CoordinateReferenceSystem shapeCRS = source.getSchema().getCoordinateReferenceSystem();

            // 在验证 schema 之前添加自动探测代码
            SimpleFeatureType schema = source.getSchema();
            
            // 如果请求自动探测字段
            if (idField.equals("AUTO") || nameField.equals("AUTO")) {
                // 获取所有可用字段
                String[] autoFields = detectFields(schema);
                
                // 如果需要自动探测 ID 字段
                if (idField.equals("AUTO")) {
                    idField = autoFields[0];
                    SFIToolkit.displayln("Auto-detected ID field: " + idField);
                }
                
                // 如果需要自动探测名称字段
                if (nameField.equals("AUTO")) {
                    nameField = autoFields[1];
                    SFIToolkit.displayln("Auto-detected name field: " + nameField);
                }
            }
            
            // 确保字段存在
            if (!isValidField(schema, idField)) {
                SFIToolkit.errorln("Invalid ID field: " + idField);
                return;
            }
            if (!isValidField(schema, nameField)) {
                SFIToolkit.errorln("Invalid Name field: " + nameField);
                return;
            }

            // 修改 calculateZonalStats 方法中的 CRS 处理部分

            // 将这部分代码:
            if (shapeCRS == null && shpcrs != null && !shpcrs.trim().isEmpty()) {
                try {
                    SFIToolkit.displayln("Shapefile CRS is undefined, using provided EPSG code: " + shpcrs);
                    shapeCRS = CRS.decode(shpcrs.trim());
                    SFIToolkit.displayln("Successfully set shapefile CRS to: " + shapeCRS.getName());
                } catch (Exception e) {
                    SFIToolkit.errorln("Failed to use provided CRS code: " + e.getMessage());
                }
            }

            if (shapeCRS == null || rasterCRS == null) {
                StringBuilder error = new StringBuilder("CRS Error: ");
                if (shapeCRS == null) {
                    error.append("Shapefile CRS is undefined. ");
                }
                if (rasterCRS == null) {
                    error.append("Raster CRS is undefined. ");
                }
                error.append("Both files must have defined coordinate reference systems.");
                SFIToolkit.errorln(error.toString());
                return; // Exit the method
            }

            // If we get here, both CRS are defined, so we can proceed with transformation if needed
            MathTransform transform = null;
            if (!CRS.equalsIgnoreMetadata(shapeCRS, rasterCRS)) {
                try {
                    SFIToolkit.displayln("CRS differs, configuring transformation...");
                    transform = CRS.findMathTransform(shapeCRS, rasterCRS, true);
                } catch (FactoryException fe) {
                    SFIToolkit.errorln("CRS transformation failed: " + fe.getMessage());
                    return; // Exit the method on transformation failure
                }
            }

            // Initialize Stata dataset with base columns
            Data.addVarInt("ID");
            Data.addVarStr("NAME", 100);
            
            // Add variables for requested statistics
            if (calculateMean) Data.addVarDouble("mean_value");
            if (calculateMin) Data.addVarDouble("min_value");
            if (calculateMax) Data.addVarDouble("max_value");
            if (calculateRange) Data.addVarDouble("range_value");
            if (calculateSum) Data.addVarDouble("sum_value");
            if (calculateCount) Data.addVarDouble("count_value");
            if (calculateVariance) Data.addVarDouble("variance_value");
            if (calculateStdev) Data.addVarDouble("stdev_value");
            if (calculateMedian) Data.addVarDouble("median_value");
            if (calculateNodata) Data.addVarLong("nodata_value");
            
            // Always add these variables for coverage information
            Data.addVarLong("valid_pixels");
            Data.addVarLong("total_pixels");
            Data.addVarDouble("coverage_pct");

            // Process features
            SimpleFeatureIterator iterator = features.features();
            int totalFeatures = features.size();
            int processed = 0;

            try {
                SFIToolkit.displayln("Total polygons to process: " + totalFeatures);
                
                // 预先设置数据集观测值数量
                Data.setObsCount(totalFeatures);
                
                // 使用 RasterZonalStatistics 而不是 RasterZonalStatistics2
                RasterZonalStatistics process = new RasterZonalStatistics();
                
                while (iterator.hasNext()) {
                    SimpleFeature feature = null;
                    try {
                        feature = iterator.next();
                        processed++;
                        
                        // Extract attributes
                        String regionName = getAttributeAsString(feature, nameField, "Region " + processed);
                        int regionId = parseRegionId(feature.getAttribute(idField), processed);

                        // Process geometry
                        Geometry geometry = (Geometry) feature.getDefaultGeometry();
                        if (geometry == null || geometry.isEmpty()) {
                            SFIToolkit.displayln("Skipping invalid geometry for record: " + processed);
                            continue;
                        }
                        geometry = geometry.copy();
                        if (transform != null) {
                            geometry = JTS.transform(geometry, transform);
                        }

                        // 创建单个要素的集合
                        SimpleFeatureCollection featureColl = new ListFeatureCollection(schema, Collections.singletonList(feature));

                        // 修改 execute 部分的代码:

                        // 修改这部分代码:
                        Map<String, Object> statsResult = null;
                        try {
                            // 检查几何体是否有效
                            if (geometry == null || geometry.isEmpty()) {
                                SFIToolkit.displayln("Skipping region " + regionId + ": Invalid geometry");
                                statsResult = new HashMap<>(); // 返回空的统计结果
                            } else {
                                // 修复几何体
                                geometry = geometry.buffer(0);
                                if (!geometry.isValid()) {
                                    SFIToolkit.displayln("Warning: Region " + regionId + " has invalid geometry. Attempting repair...");
                                    
                                    // 首先确保多边形闭合
                                    geometry = ensurePolygonClosed(geometry);
                                    
                                    // 然后执行现有的修复逻辑
                                    if (geometry instanceof Polygon) {
                                        try {
                                            List<Polygon> validPolys = JTS.makeValid((Polygon)geometry, true);
                                            if (validPolys.size() == 1) {
                                                geometry = validPolys.get(0);
                                            } else if (!validPolys.isEmpty()) {
                                                GeometryFactory factory = new GeometryFactory();
                                                Polygon[] polygonArray = validPolys.toArray(new Polygon[0]);
                                                geometry = factory.createMultiPolygon(polygonArray);
                                            }
                                        } catch (Exception e) {
                                            SFIToolkit.displayln("Error repairing Polygon: " + e.getMessage());
                                            geometry = geometry.buffer(0); // 回退到buffer(0)方法
                                        }
                                    } else if (geometry instanceof MultiPolygon) {
                                        // 对于MultiPolygon，需要单独处理每个polygon
                                        MultiPolygon multiPoly = (MultiPolygon)geometry;
                                        List<Polygon> validPolygons = new ArrayList<>();
                                        
                                        for (int i = 0; i < multiPoly.getNumGeometries(); i++) {
                                            Polygon poly = (Polygon)multiPoly.getGeometryN(i);
                                            try {
                                                List<Polygon> validPolys = JTS.makeValid(poly, true);
                                                validPolygons.addAll(validPolys);
                                            } catch (Exception e) {
                                                SFIToolkit.displayln("Error repairing part of MultiPolygon: " + e.getMessage());
                                                // 尝试使用buffer(0)修复单个polygon
                                                Geometry repaired = poly.buffer(0);
                                                if (repaired instanceof Polygon) {
                                                    validPolygons.add((Polygon)repaired);
                                                } else if (repaired instanceof MultiPolygon) {
                                                    for (int j = 0; j < ((MultiPolygon)repaired).getNumGeometries(); j++) {
                                                        validPolygons.add((Polygon)((MultiPolygon)repaired).getGeometryN(j));
                                                    }
                                                }
                                            }
                                        }
                                        
                                        if (!validPolygons.isEmpty()) {
                                            GeometryFactory factory = new GeometryFactory();
                                            Polygon[] polygonArray = validPolygons.toArray(new Polygon[0]);
                                            geometry = factory.createMultiPolygon(polygonArray);
                                        }
                                    } else {
                                        SFIToolkit.displayln("Warning: Cannot repair geometry type " + geometry.getGeometryType());
                                        // 尝试使用buffer(0)修复，这是一种常见的几何修复技术
                                        geometry = geometry.buffer(0);
                                    }
                                }
                                
                                // 应用坐标转换
                                if (transform != null) {
                                    try {
                                        geometry = JTS.transform(geometry, transform);
                                    } catch (Exception e) {
                                        SFIToolkit.displayln("Error transforming geometry for region " + regionId + ": " + e.getMessage());
                                        statsResult = new HashMap<>();
                                        return; // 跳过此区域
                                    }
                                }
                                
                                // 创建单个要素的集合
                                // 修复featureColl重复定义
                                // 删除第二个定义:
                                // 删除这行:
                                // SimpleFeatureCollection featureColl = new ListFeatureCollection(schema, Collections.singletonList(feature));

                                // 最初的定义仍然保留:
                                featureColl = new ListFeatureCollection(schema, Collections.singletonList(feature));
                                
                                try {
                                    // 使用 try-catch 包装 process.execute 调用
                                    SimpleFeatureCollection processResult = process.execute(
                                        coverage,         // 源栅格
                                        bandIndex - 1,    // 波段索引 (修正为0基索引)
                                        featureColl,      // 要素集合
                                        null              // 分类栅格（可选）
                                    );
                                    
                                    // 从处理结果中提取统计信息
                                    statsResult = extractStatistics(processResult, feature);
                                } catch (Exception e) {
                                    SFIToolkit.displayln("Error in zonal statistics calculation for region " + regionId + ": " + e.getMessage());
                                    statsResult = new HashMap<>(); // 返回空的统计结果
                                    // 打印更多调试信息
                                    SFIToolkit.displayln("Geometry type: " + geometry.getGeometryType());
                                    SFIToolkit.displayln("Geometry valid: " + geometry.isValid());
                                    SFIToolkit.displayln("Geometry area: " + geometry.getArea());
                                }
                            }
                            
                            // 保存结果到数据集
                            storeResults(processed-1, regionId, regionName, statsResult, requestedStats, 0, noDataValue);
                            
                        } catch (Exception e) {
                            SFIToolkit.displayln("General error processing region " + regionId + ": " + e.getMessage());
                            e.printStackTrace();
                        }

                        // 进度更新
                        if (processed % 10 == 0 || processed == totalFeatures) {
                            SFIToolkit.displayln(String.format("Progress: %d/%d (%.1f%%)", 
                                processed, totalFeatures, (processed*100.0/totalFeatures)));
                        }
                    } catch (Exception e) {
                        SFIToolkit.displayln("Error processing feature #" + processed + ": " + e.getMessage());
                    }
                }
            } finally {
                // SFIToolkit.displayln("Shapefile CRS: " + (shapeCRS != null ? shapeCRS.getName() : "NULL"));
                // SFIToolkit.displayln("Raster CRS: " + (rasterCRS != null ? rasterCRS.getName() : "NULL"));
                iterator.close();
                dataStore.dispose();
                rasterReader.dispose();
            }

        } catch (Exception e) {
            SFIToolkit.errorln("Critical error: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private static Double getNoDataValue(GridCoverage2D coverage, int bandIndex) {
        try {
            // 在GeoTools 32.0中获取NoData值的多种尝试方法
            
            // 尝试从覆盖范围属性中获取
            Object noDataObj = coverage.getProperty("GC_NODATA");
            if (noDataObj != null && noDataObj instanceof Number) {
                return ((Number)noDataObj).doubleValue();
            }
            
            // 尝试从样本维度获取
            try {
                GridSampleDimension dimension = coverage.getSampleDimension(bandIndex);
                if (dimension != null) {
                    double[] noDataValues = dimension.getNoDataValues();
                    if (noDataValues != null && noDataValues.length > 0) {
                        return noDataValues[0];
                    }
                }
            } catch (Exception e) {
                // 忽略，尝试下一个方法
            }
            
            // 尝试其他可能的属性名称
            Object customNoData = coverage.getProperty("NODATA");
            if (customNoData != null && customNoData instanceof Number) {
                return ((Number)customNoData).doubleValue();
            }
            
            return null;
        } catch (Exception e) {
            SFIToolkit.displayln("Warning: Error determining NoData value: " + e.getMessage());
            return null;
        }
    }

    private static Set<String> parseRequestedStatistics(String statisticsStr) {
        Set<String> stats = new HashSet<>();
        if (statisticsStr == null || statisticsStr.trim().isEmpty()) {
            stats.add("mean"); // Default is mean
            return stats;
        }
        
        // Add requested statistics
        for (String stat : statisticsStr.toLowerCase().split("\\s+")) {
            if (!stat.trim().isEmpty()) {
                stats.add(stat.trim());
            }
        }
        
        // If none were valid, default to mean
        if (stats.isEmpty()) {
            stats.add("mean");
        }
        
        return stats;
    }

    private static boolean isValidField(SimpleFeatureType schema, String fieldName) {
        return schema.getDescriptor(fieldName) != null;
    }

    private static int parseRegionId(Object idValue, int defaultId) {
        if (idValue == null) return defaultId;
        
        try {
            return Integer.parseInt(idValue.toString());
        } catch (Exception e) {
            return defaultId;
        }
    }

    private static String getAttributeAsString(SimpleFeature feature, String fieldName, String defaultValue) {
        Object value = feature.getAttribute(fieldName);
        return value != null ? value.toString() : defaultValue;
    }

    // 3. 修改 storeResults 方法，增加调试信息
    private static void storeResults(int rowIndex, int regionId, String regionName, 
                                    Map<String, Object> stats, Set<String> requestedStats, 
                                    long nodataCount, Double noDataValue) {
        try {
            // 打印调试信息
            SFIToolkit.displayln(String.format("Storing results for region %d (%s)", regionId, regionName));
            SFIToolkit.displayln("Available statistics: " + stats.keySet());
            
            // Store base city info
            Data.storeNum(1, rowIndex, regionId);
            Data.storeStr(2, rowIndex, regionName);
            
            int colIndex = 3;
            
            // Store requested statistics with debugging
            for (String stat : requestedStats) {
                Object value = stats.get(stat);
                if (value instanceof Number) {
                    double numValue = ((Number)value).doubleValue();
                    Data.storeNum(colIndex++, rowIndex, numValue);
                    SFIToolkit.displayln(String.format("Stored %s = %f", stat, numValue));
                } else {
                    Data.storeNum(colIndex++, rowIndex, Double.NaN);
                    SFIToolkit.displayln(String.format("No valid value for %s", stat));
                }
            }
        } catch (Exception e) {
            SFIToolkit.displayln("Error in storeResults: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // 添加字段探测方法
    private static String[] detectFields(SimpleFeatureType schema) {
        String idField = null;
        String nameField = null;
        
        List<String> possibleIdFields = Arrays.asList("ID", "FID", "OID", "GID", "OBJECTID", "CODE", "ID_", "GEOID");
        List<String> possibleNameFields = Arrays.asList("NAME", "NAME_", "NAME_CN", "CNAME", "REGION", "CITY", "LABEL", "ADMINNAME");
        
        // 获取所有属性名称
        List<String> attributeNames = new ArrayList<>();
        for (PropertyDescriptor pd : schema.getDescriptors()) {
            String name = pd.getName().toString();
            attributeNames.add(name);
        }
        
        SFIToolkit.displayln("Available fields in shapefile: " + String.join(", ", attributeNames));
        
        // 寻找可能的 ID 字段
        for (String field : possibleIdFields) {
            if (isValidField(schema, field)) {
                idField = field;
                break;
            }
        }
        
        // 如果没有找到匹配的 ID 字段，使用第一个整数类型字段
        if (idField == null) {
            for (PropertyDescriptor pd : schema.getDescriptors()) {
                Class<?> type = pd.getType().getBinding();
                String name = pd.getName().toString();
                if (Number.class.isAssignableFrom(type) && !name.equalsIgnoreCase("the_geom")) {
                    idField = name;
                    break;
                }
            }
        }
        
        // 如果还是没有找到，使用第一个属性
        if (idField == null && !attributeNames.isEmpty()) {
            for (String name : attributeNames) {
                if (!name.equalsIgnoreCase("the_geom") && !name.equalsIgnoreCase("geometry")) {
                    idField = name;
                    break;
                }
            }
        }
        
        // 寻找可能的名称字段
        for (String field : possibleNameFields) {
            if (isValidField(schema, field)) {
                nameField = field;
                break;
            }
        }
        
        // 如果没有找到匹配的名称字段，使用第一个字符串类型字段
        if (nameField == null) {
            for (PropertyDescriptor pd : schema.getDescriptors()) {
                Class<?> type = pd.getType().getBinding();
                String name = pd.getName().toString();
                if (String.class.isAssignableFrom(type) && !name.equalsIgnoreCase("the_geom") && !name.equals(idField)) {
                    nameField = name;
                    break;
                }
            }
        }
        
        // 如果还是没有找到，使用与 ID 不同的第一个非几何属性
        if (nameField == null && !attributeNames.isEmpty()) {
            for (String name : attributeNames) {
                if (!name.equalsIgnoreCase("the_geom") && !name.equalsIgnoreCase("geometry") && !name.equals(idField)) {
                    nameField = name;
                    break;
                }
            }
        }
        
        // 如果还是没有结果，回退到使用 ID 字段作为名称字段
        if (nameField == null) {
            nameField = idField;
        }
        
        return new String[]{idField, nameField};
    }

    // 2. 修改 extractStatistics 方法，增加调试信息和属性名映射
    private static Map<String, Object> extractStatistics(SimpleFeatureCollection processResult, SimpleFeature feature) {
        Map<String, Object> stats = new HashMap<>();
        try {
            if (processResult != null && processResult.size() > 0) {
                SimpleFeatureIterator iterator = processResult.features();
                try {
                    if (iterator.hasNext()) {
                        SimpleFeature resultFeature = iterator.next();
                        
                        // // 打印所有可用属性以进行调试
                        // SFIToolkit.displayln("Available statistics properties:");
                        // for (Property property : resultFeature.getProperties()) {
                        //     String name = property.getName().toString();
                        //     Object value = property.getValue();
                        //     SFIToolkit.displayln(String.format("Property: %s = %s", name, value));
                        // }
                        
                        // 使用正确的属性名称映射
                        Map<String, String> propertyMap = new HashMap<>();
                        propertyMap.put("zonal", "mean");  // 修改键名以匹配 GeoTools 输出
                        propertyMap.put("min", "min");
                        propertyMap.put("max", "max");
                        propertyMap.put("sum", "sum");
                        propertyMap.put("count", "count");
                        
                        // 提取统计量
                        for (Map.Entry<String, String> entry : propertyMap.entrySet()) {
                            try {
                                Object value = resultFeature.getAttribute(entry.getKey());
                                if (value instanceof Number) {
                                    stats.put(entry.getValue(), value);
                                } else if (value != null) {
                                    SFIToolkit.displayln("Warning: Non-numeric value for " + entry.getKey() + ": " + value);
                                }
                            } catch (Exception e) {
                                SFIToolkit.displayln("Error extracting " + entry.getKey() + ": " + e.getMessage());
                            }
                        }
                        
                        // 计算其他统计量
                        if (stats.containsKey("min") && stats.containsKey("max")) {
                            double min = ((Number)stats.get("min")).doubleValue();
                            double max = ((Number)stats.get("max")).doubleValue();
                            stats.put("range", max - min);
                        }
                        
                        if (stats.containsKey("count") && ((Number)stats.get("count")).doubleValue() > 0) {
                            // 添加覆盖率统计
                            stats.put("valid_pixels", stats.get("count"));
                            Double total = ((Number)resultFeature.getAttribute("total")).doubleValue();
                            if (total != null && total > 0) {
                                stats.put("total_pixels", total);
                                double coverage = (((Number)stats.get("count")).doubleValue() / total) * 100.0;
                                stats.put("coverage_pct", coverage);
                            }
                        }
                    } else {
                        SFIToolkit.displayln("Warning: No features found in process result");
                    }
                } finally {
                    iterator.close();
                }
            } else {
                SFIToolkit.displayln("Warning: Process result is null or empty");
            }
        } catch (Exception e) {
            SFIToolkit.displayln("Error in extractStatistics: " + e.getMessage());
            e.printStackTrace();
        }
        return stats;
    }

    // 在修复几何体的部分添加检查多边形闭合的代码

    // 添加一个检查并修复闭合的辅助方法
    private static Geometry ensurePolygonClosed(Geometry geometry) {
        if (geometry == null) return null;
        
        if (geometry instanceof Polygon) {
            return ensurePolygonRingsClosed((Polygon) geometry);
        } else if (geometry instanceof MultiPolygon) {
            MultiPolygon multiPoly = (MultiPolygon) geometry;
            Polygon[] polygons = new Polygon[multiPoly.getNumGeometries()];
            boolean modified = false;
            
            for (int i = 0; i < multiPoly.getNumGeometries(); i++) {
                Polygon poly = (Polygon) multiPoly.getGeometryN(i);
                Polygon closedPoly = ensurePolygonRingsClosed(poly);
                polygons[i] = closedPoly;
                if (closedPoly != poly) modified = true;
            }
            
            if (modified) {
                return new GeometryFactory().createMultiPolygon(polygons);
            }
        }
        
        return geometry;
    }

    // 检查并确保多边形的所有环都是闭合的
    private static Polygon ensurePolygonRingsClosed(Polygon polygon) {
        if (polygon == null) return null;
        
        GeometryFactory gf = new GeometryFactory();
        boolean modified = false;
        
        // 检查并修复外环
        LinearRing exteriorRing = (LinearRing) polygon.getExteriorRing();
        LinearRing closedExteriorRing = ensureRingClosed(exteriorRing);
        if (closedExteriorRing != exteriorRing) modified = true;
        
        // 检查并修复内环
        LinearRing[] interiorRings = new LinearRing[polygon.getNumInteriorRing()];
        for (int i = 0; i < polygon.getNumInteriorRing(); i++) {
            LinearRing interiorRing = (LinearRing) polygon.getInteriorRingN(i);
            LinearRing closedInteriorRing = ensureRingClosed(interiorRing);
            interiorRings[i] = closedInteriorRing;
            if (closedInteriorRing != interiorRing) modified = true;
        }
        
        // 如果任何环被修改，创建一个新的多边形
        if (modified) {
            return gf.createPolygon(closedExteriorRing, interiorRings);
        }
        
        return polygon;
    }

    // 确保环是闭合的
    private static LinearRing ensureRingClosed(LinearRing ring) {
        if (ring == null) return null;
        
        Coordinate[] coordinates = ring.getCoordinates();
        if (coordinates.length < 3) return ring; // 太少的点，无法形成有效环
        
        // 检查第一个和最后一个点是否相同
        Coordinate first = coordinates[0];
        Coordinate last = coordinates[coordinates.length - 1];
        
        if (first.equals(last)) {
            // 环已经闭合
            return ring;
        } else {
            SFIToolkit.displayln("Found unclosed ring, adding closing point");
            
            // 创建新的坐标数组，复制原始坐标并添加闭合点
            Coordinate[] closedCoords = new Coordinate[coordinates.length + 1];
            System.arraycopy(coordinates, 0, closedCoords, 0, coordinates.length);
            closedCoords[closedCoords.length - 1] = new Coordinate(first.x, first.y, first.z);
            
            // 创建新的闭合环
            return new GeometryFactory().createLinearRing(closedCoords);
        }
    }
}

end
