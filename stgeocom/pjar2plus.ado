/*
pjar2plus - A Stata program to copy JAR files to a specified directory in Stata PLUS directory

Description:
    This program copies JAR files from a specified source directory or file to a target directory.
    If the source is a directory, all JAR files within it will be copied.
    If the source is a single JAR file, it will be copied to the target directory.
    The target directory can be specified or defaults to the Stata PLUS/jar directory.

Syntax:
    pjar2plus anything, [to(string) replace]

Options:
    anything    - The source directory or file path. Can be a directory containing JAR files or a single JAR file.
    to(string)  - The target directory where the JAR files will be copied. Defaults to the Stata PLUS/jar directory if not specified.
    replace     - Overwrite existing files in the target directory.

Details:
    - It then checks if the target directory exists and creates it if necessary.
    - If the source is a directory, it copies all JAR files from the directory to the target directory.
    - If the source is a single file, it checks if the file is a JAR file and then copies it to the target directory.
    - Appropriate error messages are displayed if the source directory or file is not found or if the file is not a JAR file.

Examples:
    . pjar2plus "E:/source_directory", to("target_directory")
    . pjar2plus "E:/source_directory", replace
    . pjar2plus "E:/source_directory"
    . pjar2plus "E:/source_directory/singlefile.jar", to("target_directory")

Author:
    Your Name
    Your Contact Information
*/


cap program drop pjar2plus
program define pjar2plus
version 16.0
syntax anything, [to(string) replace]

removequotes, file(`"`anything'"')
local anything  `r(file)'
local anything = subinstr(`"`anything'"',"\","/",.)

local pluspath = c(sysdir_plus)
if `"`to'"' == "" {
    cap mkdir "`pluspath'jar"
    local to  `"`pluspath'jar"'
}
else{
    confirm name `to'
    mata: st_numscalar("r(exist)",direxists(`"`pluspath'`to'"'))
    if r(exist) == 0{
       mkdir `"`pluspath'`to'"'
    }
}

mata: st_numscalar("r(exist)",direxists(`"`anything'"'))
local direxist = r(exist)

local fileexist = fileexist("`anything'")

if `direxist' == 0 & `fileexist' == 0{
    di as err `"Directory {`anything'} Not found"'
    exit 198
}

if `direxist'{
    // find all jar files in the directory 
    local files : dir "`anything'" files "*.jar"
    foreach file in `files' {
        copy "`anything'/`file'" "`to'/`file'", `replace'
    }
}

if `fileexist' == 0 {
    di as err `"File {`anything'} Not found"'
    exit 198
}

if `fileexist'{
    // check if the file is a jar file
    local ext = substr("`anything'",length("`anything'")-3,.)
    if "`ext'" != ".jar" {
        di as err "Not a jar file"
        exit 198
    }

    mata: st_local("jarfile", pathbasename("`anything'"))
    copy "`anything'" "`to'/`jarfile'", replace
}



end


cap program drop removequotes
program define removequotes,rclass
version 16
syntax, file(string) 
return local file `file'
end
