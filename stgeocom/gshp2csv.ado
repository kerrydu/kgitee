cap program drop gshp2csv
program define gshp2csv

version 18.0
checkdependencies  // 使用相同的依赖检查机制

syntax using/, outfile(string) [idvar(string) namevar(string) autofields]

// 在解析idvar和namevar之前添加自动探测选项
if "`autofields'"=="autofields" | ("`idvar'"=="" & "`namevar'"=="") {
    di as text "Attempting to auto-detect ID and name fields in shapefile..."
    local idvar "AUTO"
    local namevar "AUTO"
}

// 文件路径标准化处理
local shapefile = subinstr(`"`using'"',"\","/",.)
local outfile = subinstr(`"`outfile'"',"\","/",.)

// 显示操作信息
di as text "Extracting polygon data from shapefile..."
di as text "Shapefile: `shapefile'"
di as text "Output CSV: `outfile'"

// 调用Java方法处理shapefile并导出为CSV
java: PolygonExporter.exportPolygonsToCSV("`shapefile'", "`outfile'", "`idvar'", "`namevar'")

di as text "Polygon data extraction complete."

end

// 检查依赖项
program define checkdependencies
version 18.0

local jars gt-main-32.0.jar gt-api-32.0.jar gt-shapefile-32.0.jar gt-referencing-32.0.jar gt-epsg-hsql-32.0.jar gt-metadata-32.0.jar

local rc 0
foreach jar in `jars'{
    cap findfile `jar'
    if _rc {
        local rc = 1
    }
}

if `rc'{
    path_geotoolsjar
    local path `r(path)'

    foreach jar in `jars' {
        cap findfile `jar', path(`"`path'"')
        if _rc {
            di as error "`jar' NOT found"
            di as error "Use geotools_init for re-initializing Java environment"
            di as error "Make sure `jar' exists in your specified directory"
            exit
        }
    }

    qui adopath ++ `"`path'"'
}

end

// Java implementation
java:
/cp gt-metadata-32.0.jar
/cp gt-api-32.0.jar
/cp gt-main-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-shapefile-32.0.jar

import com.stata.sfi.*;
import org.geotools.api.data.*;
import org.geotools.data.shapefile.*;
import org.geotools.data.simple.*;
import org.geotools.api.feature.*;
import org.geotools.api.feature.simple.*;
import org.geotools.api.feature.type.PropertyDescriptor;
import org.locationtech.jts.geom.*;
import org.geotools.data.shapefile.shp.ShapefileReader;
import org.geotools.data.shapefile.files.ShpFiles;
import java.io.*;
import java.util.*;

public class PolygonExporter {
    static {
        // 设置默认编码
        System.setProperty("file.encoding", "UTF-8");
        
        // 设置默认 Locale
        try {
            java.util.Locale.setDefault(java.util.Locale.CHINA);
        } catch (Exception e) {
            // 忽略设置 Locale 的错误
        }
    }

    public static void exportPolygonsToCSV(String shapefilePath, String csvFilePath, 
                                          String idField, String nameField) {
        try {
            // 尝试使用直接方法加载shapefile
            try {
                exportWithDirectMethod(shapefilePath, csvFilePath, idField, nameField);
            } catch (Exception e) {
                SFIToolkit.displayln("Error with standard loading method: " + e.getMessage());
                SFIToolkit.displayln("Attempting alternate loading method for corrupted shapefile...");
                
                try {
                    exportWithAlternateMethod(shapefilePath, csvFilePath, idField, nameField);
                } catch (Exception e2) {
                    SFIToolkit.displayln("Alternate method failed: " + e2.getMessage());
                    SFIToolkit.displayln("Attempting last resort method (geometry only)...");
                    exportWithLastResortMethod(shapefilePath, csvFilePath);
                }
            }
        } catch (Exception e) {
            SFIToolkit.errorln("Error exporting polygons: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // 标准方法 - 使用GeoTools默认的shapefile读取方式
    private static void exportWithDirectMethod(String shapefilePath, String csvFilePath, 
                                             String idField, String nameField) throws Exception {
        // 原有的加载和处理代码
        SFIToolkit.displayln("Loading shapefile: " + shapefilePath);
        ShapefileDataStore dataStore = new ShapefileDataStore(new File(shapefilePath).toURI().toURL());
        dataStore.setCharset(java.nio.charset.Charset.forName("UTF-8"));
        SimpleFeatureSource source = dataStore.getFeatureSource();
        SimpleFeatureCollection features = source.getFeatures();
        
        // 获取schema
        SimpleFeatureType schema = source.getSchema();
        
        // 自动探测字段
        if (idField.equals("AUTO") || nameField.equals("AUTO")) {
            String[] autoFields = detectFields(schema);
            
            if (idField.equals("AUTO")) {
                idField = autoFields[0];
                SFIToolkit.displayln("Auto-detected ID field: " + idField);
            }
            
            if (nameField.equals("AUTO")) {
                nameField = autoFields[1];
                SFIToolkit.displayln("Auto-detected name field: " + nameField);
            }
        }
        
        // 确保字段存在
        if (!isValidField(schema, idField)) {
            SFIToolkit.errorln("Invalid ID field: " + idField);
            return;
        }
        if (!isValidField(schema, nameField)) {
            SFIToolkit.errorln("Invalid Name field: " + nameField);
            return;
        }
        
        // 创建CSV文件
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath))) {
            // 写入CSV头部
            writer.write("ID,NAME,POLYGON_INDEX,RING_TYPE,POINT_INDEX,X,Y\n");
            
            // 处理每个要素
            SimpleFeatureIterator iterator = features.features();
            int totalFeatures = features.size();
            int processed = 0;
            
            try {
                SFIToolkit.displayln("Processing " + totalFeatures + " features...");
                
                while (iterator.hasNext()) {
                    SimpleFeature feature = iterator.next();
                    processed++;
                    
                    // 获取ID和名称
                    String id = getAttributeAsString(feature, idField, "ID_" + processed);
                    String name = getAttributeAsString(feature, nameField, "Region_" + processed);
                    
                    // 获取几何体
                    Geometry geometry = (Geometry) feature.getDefaultGeometry();
                    if (geometry == null || geometry.isEmpty()) {
                        SFIToolkit.displayln("Skipping empty geometry for ID: " + id);
                        continue;
                    }
                    
                    // 处理几何体并写入坐标
                    int polygonIndex = 0;
                    
                    if (geometry instanceof Polygon) {
                        writePolygon(writer, id, name, (Polygon)geometry, polygonIndex);
                    } else if (geometry instanceof MultiPolygon) {
                        MultiPolygon multiPoly = (MultiPolygon)geometry;
                        for (int i = 0; i < multiPoly.getNumGeometries(); i++) {
                            Geometry geom = multiPoly.getGeometryN(i);
                            if (geom instanceof Polygon) {
                                writePolygon(writer, id, name, (Polygon)geom, i);
                            }
                        }
                    }
                    
                    // 进度更新
                    if (processed % 10 == 0 || processed == totalFeatures) {
                        SFIToolkit.displayln(String.format("Progress: %d/%d (%.1f%%)", 
                            processed, totalFeatures, (processed*100.0/totalFeatures)));
                    }
                }
            } finally {
                iterator.close();
            }
            
            SFIToolkit.displayln("Successfully processed " + processed + " features.");
        }
        
        dataStore.dispose();
        
    }

    // 修改备用方法，使用更直接的方式
    private static void exportWithAlternateMethod(String shapefilePath, String csvFilePath, 
                                                String idField, String nameField) throws Exception {
        SFIToolkit.displayln("Using alternate method for shapefile: " + shapefilePath);
        
        // 1. 修复文件路径，确保使用.shp而不是.shx
        if (shapefilePath.toLowerCase().endsWith(".shx")) {
            shapefilePath = shapefilePath.substring(0, shapefilePath.length() - 4) + ".shp";
            SFIToolkit.displayln("Corrected path to: " + shapefilePath);
        }
        
        // 2. 尝试直接读取Shapefile而不是通过工厂
        try {
            // 尝试使用不同的字符集
            String[] charsets = {"UTF-8", "GBK", "GB2312", "ISO-8859-1"};
            boolean success = false;
            
            for (String charset : charsets) {
                try {
                    SFIToolkit.displayln("Trying with charset: " + charset);
                    
                    ShapefileDataStore dataStore = new ShapefileDataStore(new File(shapefilePath).toURI().toURL());
                    dataStore.setCharset(java.nio.charset.Charset.forName(charset));
                    
                    // 获取特征
                    SimpleFeatureSource source = dataStore.getFeatureSource();
                    SimpleFeatureCollection features = source.getFeatures();
                    SimpleFeatureType schema = source.getSchema();
                    
                    // 测试读取数据
                    SimpleFeatureIterator testIter = features.features();
                    if (testIter.hasNext()) {
                        testIter.next();  // 测试读取一个特征
                        testIter.close();
                        
                        // 如果成功，使用这个字符集
                        SFIToolkit.displayln("Successfully read shapefile with charset: " + charset);
                        
                        // 重新获取数据
                        processShapefile(dataStore, source, schema, csvFilePath, idField, nameField);
                        success = true;
                        break;
                    }
                    testIter.close();
                    dataStore.dispose();
                    
                } catch (Exception e) {
                    SFIToolkit.displayln("Failed with charset " + charset + ": " + e.getMessage());
                }
            }
            
            if (!success) {
                throw new IOException("Failed to read shapefile with any charset");
            }
            
        } catch (Exception e) {
            // 最后的备用方法：尝试手动读取.shp和.dbf文件
            SFIToolkit.errorln("Cannot read shapefile: " + e.getMessage());
            exportWithLastResortMethod(shapefilePath, csvFilePath);
        }
    }

    // 添加处理shapefile的方法
    private static void processShapefile(ShapefileDataStore dataStore, SimpleFeatureSource source, 
                                        SimpleFeatureType schema, String csvFilePath, 
                                        String idField, String nameField) throws Exception {
        // 自动探测字段
        if (idField.equals("AUTO") || nameField.equals("AUTO")) {
            String[] autoFields = detectFields(schema);
            
            if (idField.equals("AUTO")) {
                idField = autoFields[0];
                SFIToolkit.displayln("Auto-detected ID field: " + idField);
            }
            
            if (nameField.equals("AUTO")) {
                nameField = autoFields[1];
                SFIToolkit.displayln("Auto-detected name field: " + nameField);
            }
        }
        
        // 确保字段存在
        if (!isValidField(schema, idField)) {
            SFIToolkit.errorln("Invalid ID field: " + idField);
            return;
        }
        if (!isValidField(schema, nameField)) {
            SFIToolkit.errorln("Invalid Name field: " + nameField);
            return;
        }
        
        // 创建CSV文件
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath))) {
            writer.write("ID,NAME,POLYGON_INDEX,RING_TYPE,POINT_INDEX,X,Y\n");
            
            SimpleFeatureCollection features = source.getFeatures();
            SimpleFeatureIterator iterator = features.features();
            int processed = 0;
            int totalFeatures = features.size();
            
            try {
                SFIToolkit.displayln("Processing " + totalFeatures + " features...");
                
                while (iterator.hasNext()) {
                    try {
                        SimpleFeature feature = iterator.next();
                        processed++;
                        
                        // 获取ID和名称
                        String id = getAttributeAsString(feature, idField, "ID_" + processed);
                        String name = getAttributeAsString(feature, nameField, "Region_" + processed);
                        
                        // 获取几何体
                        Geometry geometry = (Geometry) feature.getDefaultGeometry();
                        if (geometry == null || geometry.isEmpty()) {
                            SFIToolkit.displayln("Skipping empty geometry for ID: " + id);
                            continue;
                        }
                        
                        // 处理几何体
                        if (geometry instanceof Polygon) {
                            writePolygon(writer, id, name, (Polygon)geometry, 0);
                        } else if (geometry instanceof MultiPolygon) {
                            MultiPolygon multiPoly = (MultiPolygon)geometry;
                            for (int i = 0; i < multiPoly.getNumGeometries(); i++) {
                                Geometry geom = multiPoly.getGeometryN(i);
                                if (geom instanceof Polygon) {
                                    writePolygon(writer, id, name, (Polygon)geom, i);
                                }
                            }
                        }
                    } catch (Exception e) {
                        SFIToolkit.displayln("Error processing feature: " + e.getMessage());
                    }
                    
                    if (processed % 10 == 0 || processed == totalFeatures) {
                        SFIToolkit.displayln(String.format("Progress: %d/%d (%.1f%%)", 
                            processed, totalFeatures, (processed*100.0/totalFeatures)));
                    }
                }
            } finally {
                iterator.close();
            }
            
            SFIToolkit.displayln("Successfully processed " + processed + " features.");
        }
        
        dataStore.dispose();
    }
    
    // 写入多边形数据到CSV
    private static void writePolygon(BufferedWriter writer, String id, String name, 
                                   Polygon polygon, int polygonIndex) throws IOException {
        // 转义CSV中的特殊字符
        String safeId = id.replace("\"", "\"\"");
        String safeName = name.replace("\"", "\"\"");
        
        // 写入外环
        LineString exteriorRing = polygon.getExteriorRing();
        writeLineString(writer, safeId, safeName, polygonIndex, "exterior", exteriorRing);
        
        // 写入内环
        for (int i = 0; i < polygon.getNumInteriorRing(); i++) {
            LineString interiorRing = polygon.getInteriorRingN(i);
            writeLineString(writer, safeId, safeName, polygonIndex, "interior_" + i, interiorRing);
        }
    }
    
    // 写入线串数据到CSV
    private static void writeLineString(BufferedWriter writer, String id, String name, 
                                      int polygonIndex, String ringType, LineString ring) throws IOException {
        Coordinate[] coords = ring.getCoordinates();
        for (int i = 0; i < coords.length; i++) {
            writer.write(String.format("\"%s\",\"%s\",%d,\"%s\",%d,%f,%f\n", 
                id, name, polygonIndex, ringType, i, coords[i].x, coords[i].y));
        }
    }
    
    // 自动探测字段
    private static String[] detectFields(SimpleFeatureType schema) {
        String idField = null;
        String nameField = null;
        
        List<String> possibleIdFields = Arrays.asList("ID", "FID", "OID", "GID", "OBJECTID", "CODE", "ID_", "GEOID");
        List<String> possibleNameFields = Arrays.asList("NAME", "NAME_", "NAME_CN", "CNAME", "REGION", "CITY", "LABEL", "ADMINNAME");
        
        // 获取所有属性名称
        List<String> attributeNames = new ArrayList<>();
        for (PropertyDescriptor pd : schema.getDescriptors()) {
            String name = pd.getName().toString();
            attributeNames.add(name);
        }
        
        SFIToolkit.displayln("Available fields in shapefile: " + String.join(", ", attributeNames));
        
        // 寻找可能的ID字段
        for (String field : possibleIdFields) {
            if (isValidField(schema, field)) {
                idField = field;
                break;
            }
        }
        
        // 如果没有找到匹配的ID字段，使用第一个非几何字段
        if (idField == null) {
            for (String name : attributeNames) {
                if (!name.equalsIgnoreCase("the_geom") && !name.equalsIgnoreCase("geometry")) {
                    idField = name;
                    break;
                }
            }
        }
        
        // 寻找可能的名称字段
        for (String field : possibleNameFields) {
            if (isValidField(schema, field)) {
                nameField = field;
                break;
            }
        }
        
        // 如果没有找到名称字段，使用第一个与ID不同的非几何字段
        if (nameField == null) {
            for (String name : attributeNames) {
                if (!name.equalsIgnoreCase("the_geom") && !name.equalsIgnoreCase("geometry") && !name.equals(idField)) {
                    nameField = name;
                    break;
                }
            }
        }
        
        // 如果没有找到第二个字段，使用ID字段作为名称字段
        if (nameField == null) {
            nameField = idField;
        }
        
        return new String[]{idField, nameField};
    }
    
    private static boolean isValidField(SimpleFeatureType schema, String fieldName) {
        return schema.getDescriptor(fieldName) != null;
    }
    
    private static String getAttributeAsString(SimpleFeature feature, String fieldName, String defaultValue) {
        Object value = feature.getAttribute(fieldName);
        return value != null ? value.toString() : defaultValue;
    }

    // 新增一个最终备用方法，手动读取几何数据
    private static void exportWithLastResortMethod(String shapefilePath, String csvFilePath) throws Exception {
        SFIToolkit.displayln("Attempting last resort method - reading only geometry...");
        
        // 确保路径是正确的shapefile
        if (!shapefilePath.toLowerCase().endsWith(".shp")) {
            shapefilePath = shapefilePath.substring(0, shapefilePath.lastIndexOf(".")) + ".shp";
        }
        
        // 使用ShapefileReader直接读取几何
        try (ShapefileReader reader = new ShapefileReader(new ShpFiles(shapefilePath), false, true, new GeometryFactory())) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath))) {
                // 写入CSV头部
                writer.write("ID,NAME,POLYGON_INDEX,RING_TYPE,POINT_INDEX,X,Y\n");
                
                int featureId = 0;
                while (reader.hasNext()) {
                    try {
                        Geometry geometry = (Geometry) reader.nextRecord().shape();
                        featureId++;
                        
                        // 使用索引作为ID和名称，因为我们无法访问DBF数据
                        String id = "ID_" + featureId;
                        String name = "Feature_" + featureId;
                        
                        SFIToolkit.displayln("Processing geometry " + featureId);
                        
                        // 处理几何体
                        if (geometry instanceof Polygon) {
                            writePolygon(writer, id, name, (Polygon)geometry, 0);
                        } else if (geometry instanceof MultiPolygon) {
                            MultiPolygon multiPoly = (MultiPolygon)geometry;
                            for (int i = 0; i < multiPoly.getNumGeometries(); i++) {
                                Geometry geom = multiPoly.getGeometryN(i);
                                if (geom instanceof Polygon) {
                                    writePolygon(writer, id, name, (Polygon)geom, i);
                                }
                            }
                        }
                        
                        // 显示进度
                        if (featureId % 10 == 0) {
                            SFIToolkit.displayln("Processed " + featureId + " geometries");
                        }
                        
                    } catch (Exception e) {
                        SFIToolkit.displayln("Error reading geometry: " + e.getMessage());
                    }
                }
                
                SFIToolkit.displayln("Successfully processed " + featureId + " geometries with last resort method");
            }
        } catch (Exception e) {
            SFIToolkit.errorln("Last resort method failed: " + e.getMessage());
            throw e;
        }
    }
}

end