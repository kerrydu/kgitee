import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;

public class NCtoCSVdropMissing {
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000;
    private static final int BLOCK_SIZE = 100_000;

    public static void main() {
        long startTime = System.currentTimeMillis();
        String ncFilePath = "C:/Users/kerry/Desktop/0Desktop/tas_day_NorESM2-MM_historical_r1i1p1f1_gn_hist.nc";
        String csvFilePath = "d:/temp2.csv";
        String variableName = "tas";

        try (NetcdfDataset ncDataset = NetcdfDataset.openDataset(ncFilePath);
             FileWriter csvWriter = new FileWriter(csvFilePath)) {
            
            Variable mainVar = ncDataset.findVariable(variableName);
            if (mainVar == null) {
                System.out.println("变量 " + variableName + " 未找到");
                return;
            }

            List<Variable> coordVars = new ArrayList<>();
            for (Dimension dim : mainVar.getDimensions()) {
                Variable coordVar = ncDataset.findVariable(dim.getShortName());
                if (coordVar != null) coordVars.add(coordVar);
            }

            long totalSize = calculateTotalSize(coordVars);
            if (totalSize > MAX_SAFE_ELEMENTS) {
                System.out.println("数据集过大，操作已取消");
                return;
            }

            writeHeader(csvWriter, coordVars, variableName);
            processDataOptimized(csvWriter, coordVars, mainVar, totalSize);
            
            System.out.println("数据写入完成，耗时：" + (System.currentTimeMillis() - startTime) + "ms");
        } catch (Exception e) {
            try { Files.deleteIfExists(Paths.get(csvFilePath)); } catch (IOException ex) {}
            e.printStackTrace();
        }
    }

    private static long calculateTotalSize(List<Variable> coordVars) {
        long total = 1;
        for (Variable var : coordVars) total *= var.getSize();
        return total;
    }

    private static void writeHeader(FileWriter csvWriter, List<Variable> coordVars, String varName) throws IOException {
        StringBuilder header = new StringBuilder();
        for (Variable var : coordVars) {
            header.append(var.getShortName()).append(",");
        }
        header.append(varName).append("\n");
        csvWriter.write(header.toString());
    }

    private static void processDataOptimized(FileWriter csvWriter, List<Variable> coordVars, 
                                           Variable mainVar, long totalSize) throws IOException {
        
            double fillValue = getFillValue(mainVar);
            List<double[]> coordCache = new ArrayList<>(coordVars.size());
            for (Variable var : coordVars) {
                coordCache.add((double[]) var.read().get1DJavaArray(double.class));
            }

            double[] mainValues = (double[]) mainVar.read().get1DJavaArray(double.class);
            int[] shape = coordVars.stream().mapToInt(v -> (int)v.getSize()).toArray();
            
            StringBuilder buffer = new StringBuilder(BLOCK_SIZE * 64);
            for (long i = 0; i < totalSize; i++) {
                double currentValue = mainValues[(int)i];
                if (currentValue == fillValue || Double.isNaN(currentValue)) {
                    continue;
                }

                int[] indices = calculateIndices(i, shape);
                
                for (int j = 0; j < coordVars.size(); j++) {
                    buffer.append(coordCache.get(j)[indices[j]]).append(",");
                }
                buffer.append(currentValue).append("\n");

                if ((i + 1) % BLOCK_SIZE == 0 || i == totalSize - 1) {
                    csvWriter.write(buffer.toString());
                    buffer.setLength(0);
                }
            }
        
    }

    private static double getFillValue(Variable var) {
        if (var.findAttribute("_FillValue") != null) {
            return var.findAttribute("_FillValue").getNumericValue().doubleValue();
        }
        return Double.NaN;
    }

    private static int[] calculateIndices(long index, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length-1; i >= 0; i--) {
            indices[i] = (int)(index % shape[i]);
            index /= shape[i];
        }
        return indices;
    }
}