import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Coordinate;

import java.io.*;
import java.util.*;

public class ProjConv {
    
    /**
     * 核心坐标转换方法
     * @param x 输入X坐标
     * @param y 输入Y坐标
     * @param sourceEPSG 源坐标系EPSG编号（如"EPSG:4326"）
     * @param targetEPSG 目标坐标系EPSG编号
     */
    public static double[] convertCoordinate(double x, double y, 
                                            String sourceEPSG, String targetEPSG) 
            throws Exception {
        // 验证EPSG格式
        validateEPSG(sourceEPSG);
        validateEPSG(targetEPSG);
        
        CoordinateReferenceSystem source = CRS.decode(sourceEPSG);
        CoordinateReferenceSystem target = CRS.decode(targetEPSG);
        
        MathTransform transform = CRS.findMathTransform(source, target, true);
        Coordinate coord = new Coordinate(x, y);
        Coordinate transformed = JTS.transform(coord, null, transform);
        
        return new double[]{transformed.x, transformed.y};
    }

    /**
     * CSV批量转换方法
     * @param inputPath 输入文件路径
     * @param outputPath 输出文件路径
     * @param sourceEPSG 源坐标系EPSG
     * @param targetEPSG 目标坐标系EPSG
     */
    public static void convertCsv(String inputPath, String outputPath,
                                 String sourceEPSG, String targetEPSG) 
            throws Exception {
        List<String[]> records = readCsv(inputPath);
        List<String[]> output = new ArrayList<>();
        output.add(new String[]{"newX", "newY"});

        for (String[] record : records.subList(1, records.size())) {
            double x = Double.parseDouble(record[0]);
            double y = Double.parseDouble(record[1]);
            
            double[] converted = convertCoordinate(x, y, sourceEPSG, targetEPSG);
            output.add(new String[]{
                String.format("%.6f", converted[0]),
                String.format("%.6f", converted[1])
            });
        }
        writeCsv(outputPath, output);
    }

    // EPSG格式验证
    private static void validateEPSG(String epsg) throws IllegalArgumentException {
        if (!epsg.matches("^EPSG:\\d+$")) {
            throw new IllegalArgumentException("无效的EPSG格式，正确格式示例：EPSG:4326");
        }
    }

    // CSV读写工具方法（保持不变）
    private static List<String[]> readCsv(String filePath) throws IOException {
        List<String[]> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                records.add(line.split(","));
            }
        }
        return records;
    }

    private static void writeCsv(String filePath, List<String[]> data) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
            for (String[] row : data) {
                bw.write(String.join(",", row));
                bw.newLine();
            }
        }
    }

    public static void main() {
        try {
            // 示例1：UTM 50N转WGS84
            // convertCsv("utm.csv", "wgs84.csv", 
            //           "EPSG:32650",  // UTM 50N
            //           "EPSG:4326");  // WGS84

            // 示例2：Web墨卡托转高斯投影（假设EPSG:2381为某高斯坐标系）
            double[] result = convertCoordinate(13519436.414, 4859621.225, 
                                               "EPSG:3857",   // Web墨卡托
                                               "EPSG:2381");  // 高斯投影
            System.out.println(Arrays.toString(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}