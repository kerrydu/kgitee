import ucar.array.Array;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.Group;

import java.io.IOException;
import java.util.List;

public class NCFileInfo {
    public static void main(String[] args) {
        String filePath = "C:/Users/kerry/Desktop/0Desktop/tas_day_NorESM2-MM_historical_r1i1p1f1_gn_hist.nc"; // Replace with your NC file path
        
        try (NetcdfDataset ncfile = NetcdfDataset.openDataset(filePath)) {
            System.out.println("File Name: " + ncfile.getLocation());
            System.out.println("Format: " + ncfile.getFileTypeId());
            
            printAttributes(ncfile.getGlobalAttributes(), "Global Attributes");
            printDimensions(ncfile, "维度");
            printVariables(ncfile.getVariables(), "Variables");
            
            // 只有在存在组时才打印组信息
            if (!ncfile.getRootGroup().getGroups().isEmpty()) {
                printGroups(ncfile.getRootGroup(), "");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void printAttributes(Iterable<Attribute> attributes, String title) {
        System.out.println("\n" + title + ":");
        for (Attribute att : attributes) {
            System.out.println("  " + att.getShortName() + ": " + att.getStringValue());
        }
    }
    
    private static void printDimensions(NetcdfDataset ncfile, String title) {
        System.out.println("\n" + title + ":");
        for (Dimension dim : ncfile.getDimensions()) {
            String dimName = dim.getShortName();
            Variable var = ncfile.findVariable(dimName);
            if (var != null) {
                try {
                    Array<?> data = var.readArray();
                    String dimInfo = String.format("  %s: 长度 = %d", dimName, dim.getLength());
                    if (dim.isUnlimited()) {
                        dimInfo += " (无限维度)";
                    }
                    dimInfo += String.format(", 起点 = %.4f, 终点 = %.4f", data.getDouble(0), data.getDouble(data.getSize() - 1));
                    System.out.println(dimInfo);
                } catch (IOException e) {
                    System.out.println("  " + dimName + ": 无法读取数据 - " + e.getMessage());
                }
            } else {
                System.out.println("  " + dimName + ": 找不到对应的变量");
            }
        }
    }
    
    private static void printVariables(Iterable<Variable> variables, String title) {
        System.out.println("\n" + title + ":");
        for (Variable var : variables) {
            System.out.println("  " + var.getShortName());
        }
    }
    
    private static void printGroups(Group group, String indent) {
        // Only print group information if the group is not empty
        if (!group.getGroups().isEmpty()) {
            System.out.println(indent + "组: " + group.getShortName());
            printAttributes(group.getAttributes(), indent + "  属性");
            printDimensions(group.getDimensions(), indent + "  维度");
            printVariables(group.getVariables(), indent + "  变量");
            
            for (Group subGroup : group.getGroups()) {
                printGroups(subGroup, indent + "  ");
            }
        }
    }
    
    private static void printDimensions(List<Dimension> dimensions, String indent) {
        System.out.println(indent + "维度:");
        for (Dimension dim : dimensions) {
            String dimInfo = String.format("%s  %s: 长度 = %d", indent, dim.getShortName(), dim.getLength());
            if (dim.isUnlimited()) {
                dimInfo += " (无限维度)";
            }
            System.out.println(dimInfo);
        }
    }
}
