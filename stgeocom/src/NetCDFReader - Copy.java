import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import java.io.IOException;
import com.stata.sfi.*;
import java.util.Arrays;
import java.util.stream.Collectors;

public class NetCDFReader {
    
    public static void main(String args[]) {
        if (args.length < 2) {
            System.out.println("Usage: java NetCDFReader <filePath> <variableName>");
            return;
        }
        //String filePath 设置为输入项的第一个元素
        String filePath = args[0];
        //String variableName 设置为输入项的第二个元素
        String variableName = args[1];
        
        try (NetcdfDataset dataset = NetcdfDataset.openDataset(filePath)) {
            Variable var = dataset.findVariable(variableName);
            if (var == null) {
                // System.out.println("Variable not found: " + variableName);
                SFIToolkit.errorln("Variable not found: " + variableName);
                return;
            }
            display(var);
        } catch (Exception e) {
            // System.out.println("Error reading file: " + e.getMessage());
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        }
    }

    public static void display(Variable var) {
        if (var == null) {
            //System.out.println("Variable is null");
            SFIToolkit.errorln("Variable is null");
            return;
        }

        System.out.println(var.getShortName() + " :");
        System.out.println("  Type: " + var.getDataType());
        System.out.println("  Dimensions: " + var.getDimensionsString());
        
        // 修改这一行
        System.out.println("  Shape: " + java.util.Arrays.toString(var.getShape()));
        
        // Add scale, offset, and missing value information
        String[] attributeNames = {"scale_factor", "add_offset", "missing_value", "_FillValue"};
        for (String attName : attributeNames) {
            Attribute att = var.findAttribute(attName);
            if (att == null) {
                att = var.getParentGroup().getNetcdfFile().findGlobalAttribute(attName);
            }
            if (att != null) {
               // System.out.println("  " + attName + ": " + att.getNumericValue());
               System.out.println("  " + attName + ": " + att.getValues().getDouble(0));
            }
        }
        
        System.out.println("  Attributes:");
        for (Attribute att : var.getAttributes()) {
            System.out.println("    " + att.getShortName() + ": " + att.getStringValue());
        }
    }


    public static void printVarStructure(String ncFileName, String variableName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            Variable variable = netcdfDataset.findVariable(variableName);
            if (variable == null) {
                //System.out.println("Variable " + variableName + " not found in the file.");
                SFIToolkit.errorln("Variable " + variableName + " not found in the file.");
                return;
            }

            // Print variable information
            // System.out.println("Variable Name: " + variable.getShortName() +" (dim: "+ variable.getDimensionsString()+")");
            System.out.println("Variable Name: " + variable.getShortName());
            System.out.println("Variable Attributes:");
            variable.getAttributes().forEach(attribute -> 
                System.out.println(attribute.getShortName() + ": " + attribute.getStringValue()));

            // 在打印变量信息的代码块后添加以下内容：
            Attribute unitAtt = variable.findAttribute("units");
            if (unitAtt != null) {
                System.out.println("Unit: " + unitAtt.getStringValue());
                Macro.setLocal("unit", unitAtt.getStringValue());  // 可选：存入Stata宏变量
            } else {
                System.out.println("Unit: not specified");
            }

            // Check and print coordinate axes information
            // String coordinateAxesInfo = variable.getDimensions().stream()
            //     .map(dimension -> dimension.getShortName())
            //     .reduce((a, b) -> a + " " + b)
            //     .orElse("No coordinate axes");
            String coordinateAxesInfo = variable.getDimensions().stream()
                .map(Dimension::getShortName)
                .collect(Collectors.joining(" ")); // 比 reduce 更安全的写法            

            System.out.println("Coordinate Axes:" + coordinateAxesInfo );

            // Set coordinate axes information as Stata's local macro
            Macro.setLocal("coordAxes", coordinateAxesInfo);

            // Get and print the length of each dimension
            int[] shape = variable.getShape();
            for (int i = 0; i < shape.length; i++) {
                System.out.println("Dimension " + i + " length: " + shape[i]);
            }

            // Return the lengths of dimensions to Stata's local 'dimensions'
            String dimensions = String.join(" ", Arrays.stream(shape).mapToObj(String::valueOf).toArray(String[]::new));
            Macro.setLocal("dimensions", dimensions);

            // Get variable data type
            System.out.println("Data Type: " + variable.getDataType());
            Macro.setLocal("datatype", variable.getDataType().toString());
        } catch (IOException | IllegalArgumentException e) {
            //e.printStackTrace();
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        } 
    }
    
    public static void printNetCDFStructure(String ncFileName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            // Print global attributes
            System.out.println("Global Attributes:");
            for (Attribute attr : netcdfDataset.getGlobalAttributes()) {
                System.out.println(attr.toString());
            }

            // // Print dimension information
            // System.out.println("\nDimensions:");
            // for (Dimension dim : netcdfDataset.getDimensions()) {
            //     System.out.println(dim.getShortName() + " (" + dim.getLength() + ")");
            // }

            // Print dimension information 修改后的代码块
            System.out.println("\nDimensions:");
            // 创建坐标轴维度名称集合（维度名与变量名相同且有axis属性的视为坐标轴）
            java.util.Set<String> axisDims = netcdfDataset.getVariables().stream()
                .filter(var -> {
                    Attribute axisAttr = var.findAttribute("axis");
                    return axisAttr != null || 
                        var.getShortName().equalsIgnoreCase("time") ||  // 常见时间坐标
                        var.getShortName().equalsIgnoreCase("lat") ||   // 常见纬度
                        var.getShortName().equalsIgnoreCase("lon");     // 常见经度
                })
                .map(Variable::getShortName)
                .collect(java.util.stream.Collectors.toSet());

            for (Dimension dim : netcdfDataset.getDimensions()) {
                String axisInfo = axisDims.contains(dim.getShortName()) 
                    ? " [Coordinate Axis]" 
                    : "";
                System.out.printf("%-20s %-6s %s%n",
                    dim.getShortName(),
                    "(" + dim.getLength() + ")",
                    axisInfo);
            }            

            // Print variable information
            System.out.println("\nVariables:");
            for (Variable var : netcdfDataset.getVariables()) {
                System.out.println("Variable Name: " + var.getShortName());
                System.out.println("Dimensions: " + var.getDimensionsString());
                System.out.println("Attributes:");
                for (Attribute attr : var.getAttributes()) {
                    System.out.println("  " + attr.getShortName() + ": " + attr.getStringValue());
                }
                System.out.println();
            }

            // Print group information
            System.out.println("\nGroups:");
            for (Group group : netcdfDataset.getRootGroup().getGroups()) {
                System.out.println("Group Name: " + group.getShortName());
                System.out.println("Dimensions:");
                for (Dimension dim : group.getDimensions()) {
                    System.out.println("  " + dim.getShortName() + " (" + dim.getLength() + ")");
                }
                System.out.println("Variables:");
                for (Variable var : group.getVariables()) {
                    System.out.println("  Variable Name: " + var.getShortName());
                    System.out.println("  Dimensions: " + var.getDimensionsString());
                    System.out.println("  Attributes:");
                    for (Attribute attr : var.getAttributes()) {
                        System.out.println("    " + attr.getShortName() + ": " + attr.getStringValue());
                    }
                }
                System.out.println();
            }
        } catch (IOException e) {
            // System.err.println("Error opening NetCDF file: " + e.getMessage());
            SFIToolkit.errorln("Error opening NetCDF file: " + e.getMessage());
        }
    }



}
