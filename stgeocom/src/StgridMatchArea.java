import org.locationtech.jts.geom.*;
import org.locationtech.jts.index.strtree.STRtree;
import java.util.*;
import java.util.concurrent.*;

import javax.xml.crypto.Data;

import com.stata.sfi.*;

public class StgridMatchArea {
    
    public static void run() {
        try {
            // 1. 从当前Stata数据集读取栅格点坐标
            List<GridPoint> gridPoints = readPointsFromStata();

            // 2. 从Stata frame citypoly读取城市多边形数据并构建空间索引
            Map<String, List<Coordinate>> cityCoordinates = readCityCoordinatesFromStata();
            List<City> cities = createCities(cityCoordinates);
            STRtree spatialIndex = buildSpatialIndex(cities);

            // 3. 并行匹配栅格点和城市
            List<MatchedPoint> matchedPoints = parallelMatchPointsToCities(gridPoints, spatialIndex);

            // 4. 将结果返回到Stata栅格数据集
            writeResultsToStata(matchedPoints);

            //return 0; // 成功执行
        } catch (Exception e) {
            e.printStackTrace();
            //return 1; // 执行失败
        }
    }

    // 从当前Stata数据集读取点数据
    private static List<GridPoint> readPointsFromStata() {
        List<GridPoint> points = new ArrayList<>();
        long obsCount = Data.getObsTotal();
        int latVar = 1;
        int lonVar = 2;

        for (long i = 1; i <= obsCount; i++) {
            double lat = Data.getNum(latVar, i);
            double lon = Data.getNum(lonVar, i);
            points.add(new GridPoint(lat, lon));
        }
        return points;
    }

    // 从Stata frame citypoly读取城市多边形数据
    private static Map<String, List<Coordinate>> readCityCoordinatesFromStata() {
        Map<String, List<Coordinate>> cityCoordinates = new HashMap<>();
        try {
            String citypoly = Macro.getLocal("usingframe");
            Frame framename = Frame.connect(citypoly);
            //String frameName = "citypoly";
            int idVar = 1;
            int latVar = 2;
            int lonVar = 3;
            long obsCount = framename.getObsTotal();

            String currentId = null;
            List<Coordinate> currentCoordinates = new ArrayList<>();

            for (long i = 1; i <= obsCount; i++) {
                String id =  framename.getStr(idVar, i);
                double lat = framename.getNum(latVar, i);
                double lon = framename.getNum(lonVar, i);

                if (currentId == null || !currentId.equals(id)) {
                    if (currentId != null) {
                        cityCoordinates.put(currentId, currentCoordinates);
                    }
                    currentId = id;
                    currentCoordinates = new ArrayList<>();
                }
                currentCoordinates.add(new Coordinate(lon, lat));
            }

            if (currentId != null) {
                cityCoordinates.put(currentId, currentCoordinates);
            }

            return cityCoordinates;
        } catch (FrameException e) {
            SFIToolkit.error("failed to connect to citypoly frame: " + e.getMessage());
            return cityCoordinates;
        }
    }

    // 将结果写入Stata数据集
    private static void writeResultsToStata(List<MatchedPoint> matchedPoints) {
        int idVar = 3;
        for (int i = 0; i < matchedPoints.size(); i++) {
            MatchedPoint point = matchedPoints.get(i);
            Data.storeStrfFast(idVar, i + 1, point.regionId);
        }
        Data.updateModified();
    }

    // 定义栅格点类
    private static class GridPoint {
        double lat;
        double lon;

        GridPoint(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }
    }

    // 定义城市类
    private static class City {
        String id;
        Geometry geometry;

        City(String id, Geometry geometry) {
            this.id = id;
            this.geometry = geometry;
        }

        Envelope getEnvelope() {
            return geometry.getEnvelopeInternal();
        }

        Geometry getGeometry() {
            return geometry;
        }
    }

    // 定义匹配结果类
    private static class MatchedPoint {
        String regionId;
        double lat;
        double lon;

        MatchedPoint(String regionId, double lat, double lon) {
            this.regionId = regionId;
            this.lat = lat;
            this.lon = lon;
        }
    }

    // 创建城市对象
    private static List<City> createCities(Map<String, List<Coordinate>> cityCoordinates) {
        List<City> cities = new ArrayList<>();
        GeometryFactory geometryFactory = new GeometryFactory();
        for (Map.Entry<String, List<Coordinate>> entry : cityCoordinates.entrySet()) {
            String id = entry.getKey();
            List<Coordinate> coordinates = entry.getValue();
            
            // 确保坐标列表是闭合的
            if (!coordinates.get(0).equals(coordinates.get(coordinates.size() - 1))) {
                coordinates.add(coordinates.get(0));
            }
            
            Geometry geometry = geometryFactory.createPolygon(coordinates.toArray(new Coordinate[0]));
            cities.add(new City(id, geometry));
        }
        return cities;
    }

    // 构建空间索引
    private static STRtree buildSpatialIndex(List<City> cities) {
        STRtree index = new STRtree();
        for (City city : cities) {
            index.insert(city.getEnvelope(), city);
        }
        return index;
    }

    // 并行匹配栅格点和城市
    private static List<MatchedPoint> parallelMatchPointsToCities(List<GridPoint> gridPoints, STRtree spatialIndex) {
        int processors = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(processors);
        int chunkSize = gridPoints.size() / processors;

        List<Future<List<MatchedPoint>>> futures = new ArrayList<>();
        for (int i = 0; i < processors; i++) {
            int start = i * chunkSize;
            int end = (i == processors - 1) ? gridPoints.size() : (i + 1) * chunkSize;
            futures.add(executor.submit(() -> matchPointsToCitiesChunk(gridPoints.subList(start, end), spatialIndex)));
        }

        List<MatchedPoint> matchedPoints = new ArrayList<>();
        for (Future<List<MatchedPoint>> future : futures) {
            try {
                matchedPoints.addAll(future.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
        return matchedPoints;
    }

    // 匹配一个子列表的栅格点和城市
    private static List<MatchedPoint> matchPointsToCitiesChunk(List<GridPoint> gridPoints, STRtree spatialIndex) {
        List<MatchedPoint> matchedPoints = new ArrayList<>();
        GeometryFactory geometryFactory = new GeometryFactory();

        for (GridPoint point : gridPoints) {
            Point geoPoint = geometryFactory.createPoint(new Coordinate(point.lon, point.lat));
            @SuppressWarnings("unchecked")
            List<City> candidates = spatialIndex.query(geoPoint.getEnvelopeInternal());
            
            boolean matched = false;
            for (City city : candidates) {
                if (city.getGeometry().contains(geoPoint)) {
                    matchedPoints.add(new MatchedPoint(city.id, point.lat, point.lon));
                    matched = true;
                    break;
                }
            }
            
            if (!matched) {
                matchedPoints.add(new MatchedPoint("", point.lat, point.lon));
            }
        }

        return matchedPoints;
    }
}
