* gtifftoStata.ado
cap program drop gtiffread
program define gtiffread
version 18.0

checkdependencies

syntax anything using/, [crscode(string) band(real 1) origin(numlist min=2 max=2 integer >0) size(numlist min=2 max=2 integer)  clear]
gettoken anything 0:0
if "`anything'"=="meta" { // display the metadata

    gtiffinfo `0'
    
}  
else if "`anything'"=="value" { // export pixel values with coordinates
    greadvalue `0'
    
}
else{
    di as error "Invalid subcommand"
    di as error "Valid subcommands are: meta, value"
    exit 198
}


end 
////////////////////////////////////////

program define checkdependencies

version 18 

local jars gt-main-32.0.jar gt-referencing-32.0.jar gt-epsg-hsql-32.0.jar gt-process-raster-32.0.jar
local jars `jars' gt-epsg-extension-32.0.jar gt-geotiff-32.0.jar gt-coverage-32.0.jar

local rc 0
foreach jar in `jars'{
	cap findfile `jar'
	if _rc {
		local rc = 1
	}
}

if `rc'{
	path_geotoolsjar
    local path `r(path)'

	foreach jar in `jars' {
	
	    cap findfile `jar', path(`"`path'"')
	    if _rc {
        di as error "`jar' NOT found"
        di as error "use geotools_init for re-initializing Java environment,help geotools_init"
        di as error "make sure `jar' exists in your specified directory"
        exit
      }
	
	}
	

    qui adopath ++ `"`path'"'
}



end


////////////////////////////////////////
cap program drop greadvalue
program define greadvalue
version 18.0
syntax using/, [crscode(string) band(real 1) origin(numlist min=2 max=2 integer >0) size(numlist min=2 max=2 integer) clear ]
// 参数处理逻辑
if "`crscode'"=="" local crscode None

if "`clear'"!="clear" {
    qui describe
    if r(N) > 0 | r(k) > 0 {
        di as error "Data already in memory, use the clear option to overwrite"
        exit 198
    }

}
else {
    clear
}

// 初始化Stata数据结构
qui {
    
    gen double x = .
    gen double y = .
    gen double value = .
}

if "`origin'"=="" {
    java: GeoTiffToStataExporter.exportToStata("`using'", `band', "`crscode'", 0, 0, -1, -1)
}
else {
    // 坐标转换逻辑（同原CSV版本）
    local startRow: word 1 of `origin'
    local startCol: word 2 of `origin'
    local startCol = `startCol'-1
    local startRow = `startRow'-1
    
    if "`size'"=="" {
        local endRow -1
        local endCol -1
    }
    else {
        local endRow : word 1 of `size'
        local endCol : word 2 of `size'
        local endRow = `endRow' + `startRow'
        local endCol = `endCol' + `startCol'
    }
    
    java: GeoTiffToStataExporter.exportToStata("`using'", `band', "`crscode'", `startRow', `endRow', `startCol', `endCol')
}

// 添加标签和注释
label variable x "GeoTiff X Coordinate"
label variable y "GeoTiff Y Coordinate"
label variable value "Pixel Value (Band `band')"

end

java:
// GeoTiffToStataExporter.java

/cp gt-metadata-32.0.jar       
/cp gt-api-32.0.jar
/cp gt-main-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-epsg-extension-32.0.jar
/cp gt-geotiff-32.0.jar
/cp gt-coverage-32.0.jar
/cp gt-process-raster-32.0.jar

import com.stata.sfi.*;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridGeometry2D;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.api.referencing.operation.MathTransform;
import org.geotools.referencing.CRS;
// 新增关键导入
import org.geotools.geometry.Position2D;
import org.geotools.coverage.grid.GridCoordinates2D;
import java.awt.image.Raster;
import java.io.File;

public class GeoTiffToStataExporter {
    private static final int BLOCK_SIZE = 100_000;
    private static final int MAX_OBS = 1_000_000_000;

    public static void exportToStata(String geotiffPath, int bandIndex,
                                    String targetEpsg, 
                                    int startRow, int endRow,
                                    int startCol, int endCol) {
        GeoTiffReader reader = null;
        try {
            reader = new GeoTiffReader(new File(geotiffPath));
            GridCoverage2D coverage = reader.read(null);
            Raster raster = coverage.getRenderedImage().getData();
            
            validateCoordinates(raster, startRow, endRow, startCol, endCol);
            validateBand(raster, bandIndex - 1);
            
            //int startObs =  1;
            long totalObs = calculateTotalObs(startRow, endRow, startCol, endCol);
            if (totalObs > MAX_OBS) {
                // throw new IllegalArgumentException("Reading too many observations");
                SFIToolkit.errorln("Reading too many observations");
                return;
            }
            Data.setObsTotal(totalObs);

            processBlocks(raster, bandIndex - 1, coverage.getGridGeometry(),
                         getNoDataValue(coverage, bandIndex),
                         createTransform(coverage, targetEpsg),
                         startRow, endRow, startCol, endCol);
            
            //Data.updateModified();
        } catch (Exception e) {
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        } finally {
            if (reader != null) {
                try { reader.dispose(); } 
                catch (Exception e) { 
                    SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
                 }
            }
        }
    }

    private static void processBlocks(Raster raster, int bandIndex,
                                    GridGeometry2D gridGeometry,
                                    double noData, MathTransform transform,
                                    int startRow, int endRow,
                                    int startCol, int endCol) {
        int currentObs = 1;
        try {
            CRS.reset("all");
            for (int y = startRow; y <= endRow; y++) {
                for (int x = startCol; x <= endCol; x++) {
                    double value = raster.getSampleDouble(x, y, bandIndex);
                    if (isNoData(value, noData)) continue;

                    Position2D pos = convertCoordinate(gridGeometry, x, y, transform);
                    Data.storeNum(1, currentObs, pos.getX());
                    Data.storeNum(2, currentObs, pos.getY());
                    Data.storeNum(3, currentObs, value);
                    currentObs++;
                }
            }
            Data.updateModified();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Position2D convertCoordinate(GridGeometry2D gridGeometry,
                                              int x, int y,
                                              MathTransform transform)
        throws TransformException {
        Position2D pos = new Position2D();
        pos.setLocation(gridGeometry.gridToWorld(new GridCoordinates2D(x, y)));
        
        if (transform != null) {
            double[] src = {pos.getX(), pos.getY()};
            double[] dst = new double[2];
            transform.transform(src, 0, dst, 0, 1);
            pos.setLocation(dst[0], dst[1]);
        }
        return pos;
    }

    // 必要工具方法
    // 修正后的正确写法
    private static void validateCoordinates(Raster raster, 
                                        int startRow, int endRow,
                                        int startCol, int endCol) {
        int maxRow = raster.getHeight() - 1;
        int maxCol = raster.getWidth() - 1;
        
        if (startRow < 0 || startCol < 0 || endRow < startRow || endCol < startCol) {
            throw new IllegalArgumentException("Invalid coordinate range");
        }
        
        if (endRow > maxRow || endCol > maxCol) {
            throw new IllegalArgumentException(
                String.format(
                    "Coordinates are out of range (maximum range: %dx%d)", 
                    raster.getHeight(), 
                    raster.getWidth() // 正确闭合括号
                ) // 结束String.format参数
            ); // 结束throw语句
        }
    }

    private static long calculateTotalObs(int startRow, int endRow, 
                                        int startCol, int endCol) {
        return (long)(endRow - startRow + 1) * (endCol - startCol + 1);
    }

    private static MathTransform createTransform(GridCoverage2D coverage, String epsg) 
        throws Exception{
        if ("None".equalsIgnoreCase(epsg)) return null;
        return CRS.findMathTransform(
            coverage.getCoordinateReferenceSystem(),
            CRS.decode(epsg, true)
        );
    }

    private static double getNoDataValue(GridCoverage2D coverage, int bandIndex) {
        return coverage.getSampleDimension(bandIndex-1).getNoDataValues()[0];
    }

    private static void validateBand(Raster raster, int bandIndex) {
        if (bandIndex < 0 || bandIndex >= raster.getNumBands()) {
            throw new IllegalArgumentException("Invalid band index");
        }
    }

    private static boolean isNoData(double value, double noData) {
        return (Double.isFinite(noData) && Math.abs(value - noData) < 1e-9) ||
               (Double.isNaN(noData) && Double.isNaN(value));
    }
}


end