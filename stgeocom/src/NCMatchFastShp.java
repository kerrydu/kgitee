import java.util.*;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.index.strtree.STRtree;
import org.locationtech.jts.index.ItemVisitor;
import java.io.FileReader;
import java.io.FileWriter;

public class NCMatchFastShp {
    private static final double MISSING_VALUE_THRESHOLD = 9999;

    public static void main(String[] args) {
        String cityPolygonsCsvPath = "E:/OneDrive/doing/programming/xtsfsp2024/province.csv";
        String gridDataCsvPath = "path/to/your/grid_data.csv"; // 新的栅格数据CSV文件路径
        String csvOutputPath = "d:/temp.csv";

        try {
            // 读取城市多边形数据
            Map<String, Polygon> cityPolygons = readCityPolygons(cityPolygonsCsvPath);

            // 读取栅格数据
            GridData gridData = readGridData(gridDataCsvPath);

            // 计算每个城市的平均tas值
            Map<String, List<Double>> cityAverageTas = calculateCityAverageTas(cityPolygons, gridData);

            // 输出结果到CSV
            writeToCsv(csvOutputPath, cityAverageTas, gridData.hasTimeCoordinate);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<String, Polygon> readCityPolygons(String csvPath) throws Exception {
        Map<String, Polygon> cityPolygons = new HashMap<>();
        GeometryFactory geometryFactory = new GeometryFactory();
        try (CSVReader reader = new CSVReader(new FileReader(csvPath))) {
            // 跳过第一行（标题行）
            reader.readNext();
            
            String[] nextLine;
            String currentCityId = null;
            List<Coordinate> coordinates = new ArrayList<>();
            while ((nextLine = reader.readNext()) != null) {
                String cityId = nextLine[0];
                double lat = Double.parseDouble(nextLine[1]);
                double lon = Double.parseDouble(nextLine[2]);
                
                if (currentCityId == null || !currentCityId.equals(cityId)) {
                    if (currentCityId != null) {
                        coordinates.add(coordinates.get(0)); // 闭合多边形
                        Polygon polygon = geometryFactory.createPolygon(new CoordinateArraySequence(coordinates.toArray(new Coordinate[0])));
                        cityPolygons.put(currentCityId, polygon);
                    }
                    currentCityId = cityId;
                    coordinates.clear();
                }
                coordinates.add(new Coordinate(lon, lat));
            }
            // 处理最后一个城市
            if (currentCityId != null) {
                coordinates.add(coordinates.get(0)); // 闭合多边形
                Polygon polygon = geometryFactory.createPolygon(new CoordinateArraySequence(coordinates.toArray(new Coordinate[0])));
                cityPolygons.put(currentCityId, polygon);
            }
        }
        return cityPolygons;
    }

    private static GridData readGridData(String csvPath) throws Exception {
        List<Double> lats = new ArrayList<>();
        List<Double> lons = new ArrayList<>();
        List<Double> tasValues = new ArrayList<>();
        List<Double> times = new ArrayList<>();
        boolean hasTimeCoordinate = false;

        try (CSVReader reader = new CSVReader(new FileReader(csvPath))) {
            String[] header = reader.readNext();
            hasTimeCoordinate = header.length == 4;

            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                lats.add(Double.parseDouble(nextLine[0]));
                lons.add(Double.parseDouble(nextLine[1]));
                tasValues.add(Double.parseDouble(nextLine[2]));
                if (hasTimeCoordinate) {
                    times.add(Double.parseDouble(nextLine[3]));
                }
            }
        }

        return new GridData(lats, lons, tasValues, times, hasTimeCoordinate);
    }

    private static Map<String, List<Double>> calculateCityAverageTas(Map<String, Polygon> cityPolygons, GridData gridData) {
        Map<String, List<Double>> cityAverageTas = new HashMap<>();
        Map<String, Integer> cityPointCounts = new HashMap<>();
        GeometryFactory geometryFactory = new GeometryFactory();

        STRtree spatialIndex = new STRtree();
        for (Map.Entry<String, Polygon> entry : cityPolygons.entrySet()) {
            spatialIndex.insert(entry.getValue().getEnvelopeInternal(), entry);
        }
        spatialIndex.build();

        int timeSteps = gridData.hasTimeCoordinate ? gridData.times.size() : 1;

        // 获取唯一的lat和lon值
        Set<Double> uniqueLats = new HashSet<>(gridData.lats);
        Set<Double> uniqueLons = new HashSet<>(gridData.lons);

        // 双重循环遍历唯一的lat和lon值
        for (double lat : uniqueLats) {
            for (double lon : uniqueLons) {
                final Point point = geometryFactory.createPoint(new Coordinate(lon, lat));

                spatialIndex.query(point.getEnvelopeInternal(), new ItemVisitor() {
                    @Override
                    public void visitItem(Object item) {
                        @SuppressWarnings("unchecked")
                        Map.Entry<String, Polygon> entry = (Map.Entry<String, Polygon>) item;
                        if (entry.getValue().contains(point)) {
                            String cityId = entry.getKey();
                            List<Double> cityValues = cityAverageTas.computeIfAbsent(cityId, k -> new ArrayList<>(Collections.nCopies(timeSteps, 0.0)));
                            cityPointCounts.merge(cityId, 1, Integer::sum);

                            // 找到当前lat和lon在原始数据中的索引
                            int index = -1;
                            for (int i = 0; i < gridData.lats.size(); i++) {
                                if (isApproximatelyEqual(gridData.lats.get(i), lat) && isApproximatelyEqual(gridData.lons.get(i), lon)) {
                                    index = i;
                                    break;
                                }
                            }

                            if (index != -1) {
                                if (gridData.hasTimeCoordinate) {
                                    for (int t = 0; t < timeSteps; t++) {
                                        double tasValue = gridData.tasValues.get(index + t * gridData.lats.size());
                                        if (Math.abs(tasValue) <= MISSING_VALUE_THRESHOLD) {
                                            cityValues.set(t, cityValues.get(t) + tasValue);
                                        }
                                    }
                                } else {
                                    double tasValue = gridData.tasValues.get(index);
                                    if (Math.abs(tasValue) <= MISSING_VALUE_THRESHOLD) {
                                        cityValues.set(0, cityValues.get(0) + tasValue);
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }

        // 计算平均值
        for (String cityId : cityAverageTas.keySet()) {
            List<Double> values = cityAverageTas.get(cityId);
            int count = cityPointCounts.get(cityId);
            for (int t = 0; t < timeSteps; t++) {
                if (count > 0) {
                    values.set(t, values.get(t) / count);
                } else {
                    values.set(t, Double.NaN);
                }
            }
        }

        return cityAverageTas;
    }

    private static boolean isApproximatelyEqual(double a, double b) {
        if (Double.isNaN(a) || Double.isNaN(b)) {
            return Double.isNaN(a) && Double.isNaN(b);
        }
        // 对于非常大的值，使用相对误差
        if (Math.abs(a) > MISSING_VALUE_THRESHOLD || Math.abs(b) > MISSING_VALUE_THRESHOLD) {
            return Math.abs(a - b) / Math.max(Math.abs(a), Math.abs(b)) < 1e-6;
        }
        // 对于接近于0的值使用绝对误差
        if (Math.abs(a) < Double.MIN_NORMAL && Math.abs(b) < Double.MIN_NORMAL) {
            return Math.abs(a - b) < Double.MIN_VALUE;
        }
        // 使用相对误差
        return Math.abs(a - b) / Math.max(Math.abs(a), Math.abs(b)) < 1e-6;
    }

    private static void writeToCsv(String csvPath, Map<String, List<Double>> cityAverageTas, boolean hasTimeCoordinate) throws Exception {
        try (CSVWriter writer = new CSVWriter(new FileWriter(csvPath))) {
            String[] header = hasTimeCoordinate ? new String[]{"城市ID", "时间", "平均tas"} : new String[]{"城市ID", "平均tas"};
            writer.writeNext(header);

            for (Map.Entry<String, List<Double>> entry : cityAverageTas.entrySet()) {
                String cityId = entry.getKey();
                List<Double> averages = entry.getValue();

                if (hasTimeCoordinate) {
                    for (int i = 0; i < averages.size(); i++) {
                        writer.writeNext(new String[]{cityId, String.valueOf(i), String.valueOf(averages.get(i))});
                    }
                } else {
                    writer.writeNext(new String[]{cityId, String.valueOf(averages.get(0))});
                }
            }
        }
    }

    private static class GridData {
        List<Double> lats;
        List<Double> lons;
        List<Double> tasValues;
        List<Double> times;
        boolean hasTimeCoordinate;

        GridData(List<Double> lats, List<Double> lons, List<Double> tasValues, List<Double> times, boolean hasTimeCoordinate) {
            this.lats = lats;
            this.lons = lons;
            this.tasValues = tasValues;
            this.times = times;
            this.hasTimeCoordinate = hasTimeCoordinate;
        }
    }
}
