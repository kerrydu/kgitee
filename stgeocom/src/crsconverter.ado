cap program drop crsconverter
program crsconverter
	version 14.0
	syntax varlist(min=2 max=2 numeric), gen(string) from(string) to(string) 

    java: java clear
    java: /cp Proj4j-1.3.0.jar
    java: /open StataCRSConverter.java 

    local x: word 1 of `varlist'
    local y: word 2 of `varlist'

    java: StataCRSConverter.main("`x'","`y'","`gen'_`x'","`gen'_`y'","`from'","`to'");



    end