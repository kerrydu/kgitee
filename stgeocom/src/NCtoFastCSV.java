import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.nc2.Dimension;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import de.siegmar.fastcsv.writer.CsvWriter;

public class NCtoFastCSV {
    //private static final int BUFFER_SIZE = 8192 * 1024; // 8MB 缓冲区
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000; // 10亿个元素，约4GB数据

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis(); // 记录开始时间

        String ncFilePath = "C:/Users/kerry/Desktop/0Desktop/tas_day_NorESM2-MM_historical_r1i1p1f1_gn_hist.nc";
        String csvFilePath = "d:/temp2.csv";
        String variableName = "tas";

        try (NetcdfFile ncFile = NetcdfFile.open(ncFilePath);
             CsvWriter csvWriter = CsvWriter.builder().build(new FileWriter(csvFilePath))) {
            Variable mainVar = ncFile.findVariable(variableName);
            if (mainVar == null) {
                System.out.println("变量 " + variableName + " 未找到");
                return;
            }

            List<Variable> coordVars = new ArrayList<>();
            for (Dimension dim : mainVar.getDimensions()) {
                Variable coordVar = ncFile.findVariable(dim.getShortName());
                if (coordVar != null) {
                    coordVars.add(coordVar);
                }
            }

            long totalSize = calculateTotalSize(coordVars);
            System.out.println("数据总行数：" + totalSize);

            if (totalSize > MAX_SAFE_ELEMENTS) {
                System.out.println("警告：数据集过大，可能导致内存溢出。操作已取消。");
                return;
            }

            writeHeader(csvWriter, coordVars, variableName);
            processData(csvWriter, coordVars, mainVar, totalSize);
            System.out.println("数据已成功写入CSV文件：" + csvFilePath);

            long endTime = System.currentTimeMillis(); // 记录结束时间
            long duration = endTime - startTime; // 计算运行时间
            System.out.println("程序运行时间：" + duration + " 毫秒");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static long calculateTotalSize(List<Variable> coordVars) {
        long totalSize = 1;
        for (Variable coordVar : coordVars) {
            totalSize *= coordVar.getSize();
        }
        return totalSize;
    }

    private static void writeHeader(CsvWriter csvWriter, List<Variable> coordVars, String variableName) throws IOException {
        String[] header = new String[coordVars.size() + 1];
        for (int i = 0; i < coordVars.size(); i++) {
            header[i] = coordVars.get(i).getShortName();
        }
        header[coordVars.size()] = variableName;
        csvWriter.writeRow(header);
    }

    private static void processData(CsvWriter csvWriter, List<Variable> coordVars, Variable mainVar, long totalSize) throws IOException {
        int[] shape = coordVars.stream().mapToInt(var -> (int) var.getSize()).toArray();

        for (long i = 0; i < totalSize; i++) {
            int[] indices = calculateIndices(i, shape);
            String[] row = new String[coordVars.size() + 1];
            
            for (int j = 0; j < coordVars.size(); j++) {
                Variable coordVar = coordVars.get(j);
                try {
                    Array coordData = coordVar.read(new int[]{indices[j]}, new int[]{1});
                    row[j] = coordData.getObject(0).toString();
                } catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
            }

            try {
                int[] origin = new int[mainVar.getRank()];
                int[] size = new int[mainVar.getRank()];
                for (int j = 0; j < mainVar.getRank(); j++) {
                    origin[j] = (j < indices.length) ? indices[j] : 0;
                    size[j] = 1;
                }
                Array mainData = mainVar.read(origin, size);
                row[coordVars.size()] = String.valueOf(mainData.getDouble(0));
            } catch (InvalidRangeException e) {
                e.printStackTrace();
            }

            csvWriter.writeRow(row);
        }
    }

    private static int[] calculateIndices(long flatIndex, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length - 1; i >= 0; i--) {
            indices[i] = (int) (flatIndex % shape[i]);
            flatIndex /= shape[i];
        }
        return indices;
    }
}
