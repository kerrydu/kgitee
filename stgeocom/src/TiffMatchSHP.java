import java.io.File;
import java.io.IOException;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.crs.CoordinateReferenceSystem;
import org.locationtech.jts.geom.Geometry;
import org.geotools.api.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.geotools.api.referencing.operation.MathTransform;

import org.geotools.coverage.grid.GridCoordinates2D;
import org.geotools.coverage.grid.GridGeometry2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;
import org.opengis.referencing.operation.TransformException;

import java.awt.image.Raster;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class TiffMatchSHP {
    public static void main(String[] args) throws Exception {
        // 读取TIFF文件
        File tiffFile = new File("path/to/your/tiff/file.tif");
        GeoTiffReader reader = new GeoTiffReader(tiffFile);
        GridCoverage2D coverage = reader.read(null);

        // 读取中国城市SHP文件
        File shpFile = new File("path/to/your/china/cities.shp");
        ShapefileDataStore dataStore = new ShapefileDataStore(shpFile.toURI().toURL());
        SimpleFeatureCollection featureCollection = dataStore.getFeatureSource().getFeatures();

        // 获取坐标参考系统
        CoordinateReferenceSystem dataCRS = coverage.getCoordinateReferenceSystem();
        CoordinateReferenceSystem featureCRS = featureCollection.getSchema().getCoordinateReferenceSystem();
        MathTransform transform = CRS.findMathTransform(featureCRS, dataCRS, true);

        // 遍历每个城市特征
        SimpleFeatureIterator iterator = featureCollection.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();
            
            // 转换几何体到TIFF的坐标系
            Geometry transformedGeometry = JTS.transform(geometry, transform);
            
            // 获取平均值
            double average = getPixelValuesInGeometry(coverage, transformedGeometry)[0];
            
            // 输出结果
            String cityName = feature.getAttribute("NAME").toString();
            System.out.println("城市: " + cityName + ", 平均值: " + average);
        }

        // 清理资源
        iterator.close();
        dataStore.dispose();
        reader.dispose();
    }

    // 这个方法需要根据实际的TIFF数据结构来实现
    private static double[] getPixelValuesInGeometry(GridCoverage2D coverage, Geometry geometry) {
        GridGeometry2D gridGeometry = coverage.getGridGeometry();
        Raster raster = coverage.getRenderedImage().getData();
        GeometryFactory geometryFactory = new GeometryFactory();

        // 获取几何体的边界框
        ReferencedEnvelope geomEnvelope = new ReferencedEnvelope(geometry.getEnvelopeInternal(), coverage.getCoordinateReferenceSystem());
        Envelope intersection = geomEnvelope.intersection(gridGeometry.getEnvelope2D());

        // 将边界框转换为栅格坐标
        GridCoordinates2D minGrid, maxGrid;
        try {
            minGrid = gridGeometry.worldToGrid(new Coordinate(intersection.getMinX(), intersection.getMinY()));
            maxGrid = gridGeometry.worldToGrid(new Coordinate(intersection.getMaxX(), intersection.getMaxY()));
        } catch (TransformException e) {
            e.printStackTrace();
            return new double[0];
        }

        // 确保坐标在栅格范围内
        int minX = Math.max(0, minGrid.x);
        int minY = Math.max(0, minGrid.y);
        int maxX = Math.min(raster.getWidth() - 1, maxGrid.x);
        int maxY = Math.min(raster.getHeight() - 1, maxGrid.y);

        AtomicReference<Double> sum = new AtomicReference<>(0.0);
        AtomicInteger count = new AtomicInteger(0);

        // 并行处理像素
        java.util.stream.IntStream.range(minY, maxY + 1).parallel().forEach(y -> {
            for (int x = minX; x <= maxX; x++) {
                try {
                    Coordinate coord = gridGeometry.gridToWorld(new GridCoordinates2D(x, y)).getCoordinate();
                    if (geometry.contains(geometryFactory.createPoint(coord))) {
                        double[] pixelValue = new double[1];
                        raster.getPixel(x, y, pixelValue);
                        sum.updateAndGet(v -> v + pixelValue[0]);
                        count.incrementAndGet();
                    }
                } catch (TransformException e) {
                    e.printStackTrace();
                }
            }
        });

        // 返回平均值而不是所有像素值
        return new double[]{sum.get() / count.get()};
    }
}
