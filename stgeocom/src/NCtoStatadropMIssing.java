import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.nc2.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ucar.ma2.InvalidRangeException;
import com.stata.sfi.*;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;

public class NCtoStatadropMissing {
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000;
    private static final int BLOCK_SIZE = 100_000;

    public static void main(String ncFilePath, String variableName) {
        try (NetcdfDataset ncFile = NetcdfDataset.openDataset(ncFilePath)) {
            Variable mainVar = ncFile.findVariable(variableName);
            if (mainVar == null) {
                SFIToolkit.errorln("Variable " + variableName + " not found");
                return;
            }

            List<Variable> coordVars = new ArrayList<>();
            Map<Variable, Integer> dimOrderMap = new HashMap<>();
            List<Dimension> mainDimensions = mainVar.getDimensions();
            for (int dimIndex = 0; dimIndex < mainDimensions.size(); dimIndex++) {
                Dimension dim = mainDimensions.get(dimIndex);
                Variable coordVar = ncFile.findVariable(dim.getShortName());
                if (coordVar != null) {
                    coordVars.add(coordVar);
                    dimOrderMap.put(coordVar, dimIndex);
                }
            }

            long totalSize = calculateTotalSize(coordVars);
            if (totalSize > MAX_SAFE_ELEMENTS) {
                SFIToolkit.errorln("Dataset exceeds size limit");
                return;
            }

            createStataVariables(coordVars, variableName, totalSize);
            processData(coordVars, mainVar, totalSize, dimOrderMap);
            Data.updateModified();
        } catch (Exception e) {
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        }
    }

    private static long calculateTotalSize(List<Variable> coordVars) {
        long total = 1;
        for (Variable var : coordVars) total *= var.getSize();
        return total;
    }

    // private static void createStataVariables(List<Variable> coordVars, String varName, long totalSize) {
    //     if (totalSize > Integer.MAX_VALUE) {
    //         SFIToolkit.errorln("Observation limit exceeded");
    //         return;
    //     }
    //     Data.setObsTotal((int) totalSize);
    //     for (Variable var : coordVars) Data.addVarDouble(var.getShortName());
    //     Data.addVarDouble(varName);
    // }

    private static void processData(List<Variable> coordVars, Variable mainVar, 
                                   long totalSize, Map<Variable, Integer> dimOrderMap) throws IOException {
    
            // 预提取缺失值属性
            Attribute missingAttr = null;
            String[] missingAttrs = {"_FillValue", "missing_value", "FillValue"};
            for (String attrName : missingAttrs) {
                missingAttr = mainVar.findAttribute(attrName);
                //打印missingAttr
                //SFIToolkit.displayln("missingAttr: " + missingAttr);
                if (missingAttr != null) break;
            }

            Double explicitMissing = null;
            if (missingAttr != null && missingAttr.getDataType().isNumeric()) {
                try {
                    explicitMissing = missingAttr.getNumericValue().doubleValue();
                } catch (Exception e) {
                    SFIToolkit.errorln("缺失值解析失败: " + e.getMessage());
                }
            }
            //打印explictMissing
            //SFIToolkit.displayln("explictMissing: " + explicitMissing);

            DataType dataType = mainVar.getDataType();
            // 打印datatype
            SFIToolkit.displayln("dataType: " + dataType);

            // 在processData方法中修改显式缺失值解析
            Object typedMissing = null;
            if (missingAttr != null) {
                try {
                    // 根据数据类型转换缺失值
                    switch(dataType) {
                        case DOUBLE:
                            typedMissing = missingAttr.getNumericValue().doubleValue();
                            //打印typedMissing
                            //System.out.println("typedMissing: " + typedMissing);
                            break;
                        case FLOAT:
                            typedMissing = missingAttr.getNumericValue().floatValue();
                             //打印typedMissing
                             System.out.println("typedMissing: " + typedMissing);
                            break;
                        case INT:
                            typedMissing = missingAttr.getNumericValue().intValue();
                             //打印typedMissing
                             //System.out.println("typedMissing: " + typedMissing);
                            break;
                        case LONG:
                            typedMissing = missingAttr.getNumericValue().longValue();
                             //打印typedMissing
                             System.out.println("typedMissing: " + typedMissing);
                            break;
                        case SHORT:
                            typedMissing = missingAttr.getNumericValue().shortValue();
                             //打印typedMissing
                             //System.out.println("typedMissing: " + typedMissing);
                            break;
                        case BYTE:
                            typedMissing = missingAttr.getNumericValue().byteValue();
                            break;
                        default:
                            typedMissing = missingAttr.getNumericValue().doubleValue();
                             //打印typedMissing
                             //System.out.println("typedMissing: " + typedMissing);
                    }
                } catch (Exception e) {
                    SFIToolkit.errorln("类型化缺失值解析失败: " + e.getMessage());
                }
            }
            
            List<double[]> coordCache = new ArrayList<>();
            for (Variable var : coordVars) {
                Array data = var.read();
                coordCache.add((double[]) data.get1DJavaArray(double.class));
            }

            // 修改mainValues获取方式
            Array mainArray = mainVar.read();
            double[] mainValues;
            mainValues = (double[]) mainArray.get1DJavaArray(double.class);

            int[] shape = coordVars.stream()
                .mapToInt(v -> (int)v.getSize())
                .toArray();

            int[] dimIndexes = new int[coordVars.size()];
            for (int i = 0; i < coordVars.size(); i++) {
                dimIndexes[i] = dimOrderMap.get(coordVars.get(i));
            }

            double[] buffer = new double[coordVars.size() + 1];
            
            for (int blockStart = 0; blockStart < totalSize; blockStart += BLOCK_SIZE) {
                int blockEnd = (int) Math.min(blockStart + BLOCK_SIZE, totalSize);
                
                for (int obs = blockStart; obs < blockEnd; obs++) {
                    double currentValue = mainValues[obs];
                    
                    if (isMissingValue(currentValue, explicitMissing, dataType)) {
                        continue;
                    }

                    int[] indices = calculateIndices(obs, shape);
                    int row = obs + 1;

                    for (int varIdx = 0; varIdx < coordVars.size(); varIdx++) {
                        buffer[varIdx] = coordCache.get(varIdx)[indices[dimIndexes[varIdx]]];
                    }
                    buffer[coordVars.size()] = mainValues[obs];

                    for (int col = 0; col < buffer.length; col++) {
                        Data.storeNumFast(col + 1, row, buffer[col]);
                    }
                }
            }
            Data.updateModified();
    }

    private static int[] calculateIndices(long index, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length-1; i >= 0; i--) {
            indices[i] = (int)(index % shape[i]);
            index /= shape[i];
        }
        return indices;
    }

        private static boolean isMissingValue(double currentValue, 
                                        Double explicitMissing,
                                        DataType dataType) {
        
            // 处理显式缺失值为NaN的情况
            if (Missing.isMissing(currentValue)) {
                return true;
            }
        // 显式缺失值判断
        if (explicitMissing != null) {
            try {
                // 根据数据类型转换比较值
                switch(dataType) {
                    case DOUBLE:
                    case FLOAT:
                        // 浮点型使用容差比较
                        return Math.abs(currentValue - explicitMissing) < 
                        (dataType == DataType.FLOAT ? 1e-6 : 1e-9);
                    case INT:
                        // 整型严格匹配
                        return (int)currentValue == explicitMissing.intValue();
                    case LONG:
                        return (long)currentValue == explicitMissing.longValue();
                    case SHORT:
                        return (short)currentValue == explicitMissing.shortValue();
                    case BYTE:
                        return (byte)currentValue == explicitMissing.byteValue();
                    default:
                        // 其他类型使用默认容差
                        return currentValue == explicitMissing;
                }
            } catch (Exception e) {
                SFIToolkit.errorln("缺失值比较异常: " + e.getMessage());
            }
        }
        
        // 隐式NaN判断（仅限浮点型）
        return dataType.isFloatingPoint() && Double.isNaN(currentValue);
        //return  Double.isNaN(currentValue);
    }


// 修改后的createStataVariables方法
private static void createStataVariables(List<Variable> coordVars, String varName, long totalSize) {
    if (totalSize > Integer.MAX_VALUE) {
        SFIToolkit.errorln("Observation limit exceeded");
        return;
    }
    Data.setObsTotal((int) totalSize);
    
    // 创建坐标变量
    for (Variable var : coordVars) {
        createStataVariable(var);
    }
    
    // 创建主变量（保持double类型保证精度）
    Data.addVarDouble(varName);
}



// 添加缺失值获取方法
private static Double getFillValue(Variable var) {
    try {
        Attribute fillAttr = var.findAttribute("_FillValue");
        if (fillAttr != null && fillAttr.getDataType().isNumeric()) {
            return fillAttr.getNumericValue().doubleValue();
        }
    } catch (Exception e) {
        SFIToolkit.errorln("获取填充值失败: " + var.getShortName());
    }
    return null;
}

// 值范围检查方法（优化版）
private static boolean needDoubleForStorage(Variable var) {
    try {
        Array data = var.read();
        int sampleSize = Math.min(1000, (int) var.getSize());
        Double fillValue = getFillValue(var);

        // 抽样检查数据范围
        for (int i = 0; i < sampleSize; i++) {
            Number val = (Number) data.getObject(i);
            long num = val.longValue();
            if (num > Integer.MAX_VALUE || num < Integer.MIN_VALUE) {
                return true;
            }
        }

        // 检查填充值范围
        if (fillValue != null) {
            long fill = fillValue.longValue();
            return fill > Integer.MAX_VALUE || fill < Integer.MIN_VALUE;
        }

        return false;
    } catch (Exception e) {
        SFIToolkit.errorln("值范围检查失败: " + var.getShortName() + " - " + e.getMessage());
        return true;
    }
}

// 增强的变量创建方法
private static void createStataVariable(Variable ncVar) {
    DataType dataType = ncVar.getDataType();
    String varName = ncVar.getShortName();
    
    try {
        switch(dataType) {
            case DOUBLE:
                Data.addVarDouble(varName);
                break;
            case FLOAT:
                Data.addVarFloat(varName);
                break;
            case LONG:
            case INT:
                if (needDoubleForStorage(ncVar)) {
                    Data.addVarDouble(varName);
                    SFIToolkit.displayln(varName + " 使用double存储（超出Stata long范围）");
                } else {
                    Data.addVarLong(varName);
                }
                break;
            case SHORT:
            case BYTE:
                Data.addVarInt(varName); // Stata int类型（4字节）
                break;
            default:
                Data.addVarDouble(varName);
                SFIToolkit.displayln(varName + " 使用默认double类型（类型码：" + dataType + ")");
        }
        
        // 添加变量标签
        String desc = ncVar.getDescription();
        if (desc != null && !desc.isEmpty()) {
            Data.setVarLabel(Data.getVarIndex(varName), desc);
        }
    } catch (Exception e) {
        SFIToolkit.errorln("创建变量失败: " + varName + " - " + e.getMessage());
        Data.addVarDouble(varName); // 保底方案
    }
}


}