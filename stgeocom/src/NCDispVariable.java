import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import java.io.IOException;
import com.stata.sfi.*;
import java.util.Arrays;

public class NCDispVariable {
    
    public static void main(String args[]) {
        //String filePath ="D:/wechat/WeChat Files/wxid_vu6eu977ndi121/FileStorage/File/2024-07/tas_day_IITM-ESM_historical_r1i1p1f1_gn_2014.nc";
        //String filePath = "/Users/sigma/Library/Containers/com.tencent.xinWeChat/Data/Library/Application Support/com.tencent.xinWeChat/2.0b4.0.9/1f3455493fb15f52851b8efcc279850a/Message/MessageTemp/3a658e84c887e24b3555b3c59aa6268b/OpenData/tas_day_IITM-ESM_historical_r1i1p1f1_gn_2014.nc";
        //String variableName = "tas";

        //String filePath 设置为输入项的第一个元素
        String filePath = args[0];
        //String variableName 设置为输入项的第二个元素
        String variableName = args[1];
        
        try (NetcdfDataset dataset = NetcdfDataset.openDataset(filePath)) {
            Variable var = dataset.findVariable(variableName);
            if (var == null) {
                System.out.println("Variable not found: " + variableName);
                return;
            }
            display(var);
        } catch (Exception e) {
            System.out.println("Error reading file: " + e.getMessage());
        }
    }

    public static void display(Variable var) {
        if (var == null) {
            System.out.println("Variable is null");
            return;
        }

        System.out.println(var.getShortName() + " :");
        System.out.println("  Type: " + var.getDataType());
        System.out.println("  Dimensions: " + var.getDimensionsString());
        
        // 修改这一行
        System.out.println("  Shape: " + java.util.Arrays.toString(var.getShape()));
        
        // Add scale, offset, and missing value information
        String[] attributeNames = {"scale_factor", "add_offset", "missing_value", "_FillValue"};
        for (String attName : attributeNames) {
            Attribute att = var.findAttribute(attName);
            if (att == null) {
                att = var.getParentGroup().getNetcdfFile().findGlobalAttribute(attName);
            }
            if (att != null) {
                System.out.println("  " + attName + ": " + att.getNumericValue());
            }
        }
        
        System.out.println("  Attributes:");
        for (Attribute att : var.getAttributes()) {
            System.out.println("    " + att.getShortName() + ": " + att.getStringValue());
        }
    }


    public static void printVarStructure(String ncFileName, String variableName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            Variable variable = netcdfDataset.findVariable(variableName);
            if (variable == null) {
                System.out.println("Variable " + variableName + " not found in the file.");
                return;
            }

            // Print variable information
            System.out.println("Variable Name: " + variable.getShortName() + variable.getDimensionsString());
            System.out.println("Variable Attributes:");
            variable.getAttributes().forEach(attribute -> 
                System.out.println(attribute.getShortName() + ": " + attribute.getStringValue()));

            // Check and print coordinate axes information
            String coordinateAxesInfo = variable.getDimensions().stream()
                .map(dimension -> dimension.getShortName())
                .reduce((a, b) -> a + " " + b)
                .orElse("No coordinate axes");

            System.out.println("Coordinate Axes: " + coordinateAxesInfo);

            // Set coordinate axes information as Stata's local macro
            Macro.setLocal("coordAxes", coordinateAxesInfo);

            // Get and print the length of each dimension
            int[] shape = variable.getShape();
            for (int i = 0; i < shape.length; i++) {
                System.out.println("Dimension " + i + " length: " + shape[i]);
            }

            // Return the lengths of dimensions to Stata's local 'dimensions'
            String dimensions = String.join(" ", Arrays.stream(shape).mapToObj(String::valueOf).toArray(String[]::new));
            Macro.setLocal("dimensions", dimensions);

            // Get variable data type
            System.out.println("Data Type: " + variable.getDataType());
            Macro.setLocal("datatype", variable.getDataType().toString());
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        } 
    }
    
    public static void printNetCDFStructure(String ncFileName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            // Print global attributes
            System.out.println("Global Attributes:");
            for (Attribute attr : netcdfDataset.getGlobalAttributes()) {
                System.out.println(attr.toString());
            }

            // Print dimension information
            System.out.println("\nDimensions:");
            for (Dimension dim : netcdfDataset.getDimensions()) {
                System.out.println(dim.getShortName() + " (" + dim.getLength() + ")");
            }

            // Print variable information
            System.out.println("\nVariables:");
            for (Variable var : netcdfDataset.getVariables()) {
                System.out.println("Variable Name: " + var.getShortName());
                System.out.println("Dimensions: " + var.getDimensionsString());
                System.out.println("Attributes:");
                for (Attribute attr : var.getAttributes()) {
                    System.out.println("  " + attr.getShortName() + ": " + attr.getStringValue());
                }
                System.out.println();
            }

            // Print group information
            System.out.println("\nGroups:");
            for (Group group : netcdfDataset.getRootGroup().getGroups()) {
                System.out.println("Group Name: " + group.getShortName());
                System.out.println("Dimensions:");
                for (Dimension dim : group.getDimensions()) {
                    System.out.println("  " + dim.getShortName() + " (" + dim.getLength() + ")");
                }
                System.out.println("Variables:");
                for (Variable var : group.getVariables()) {
                    System.out.println("  Variable Name: " + var.getShortName());
                    System.out.println("  Dimensions: " + var.getDimensionsString());
                    System.out.println("  Attributes:");
                    for (Attribute attr : var.getAttributes()) {
                        System.out.println("    " + attr.getShortName() + ": " + attr.getStringValue());
                    }
                }
                System.out.println();
            }
        } catch (IOException e) {
            System.err.println("Error opening NetCDF file: " + e.getMessage());
        }
    }



}
