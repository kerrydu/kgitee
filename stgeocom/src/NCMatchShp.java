import java.io.*;
import java.util.*;
import ucar.nc2.*;
import ucar.ma2.*;
import ucar.nc2.dataset.NetcdfDataset;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.index.strtree.STRtree;
import org.locationtech.jts.index.ItemVisitor;
import java.io.FileReader;
import java.io.FileWriter;

public class NCMatchShp {
    public static void main(String[] args) {
        String csvInputPath = "E:/OneDrive/doing/programming/xtsfsp2024/province.csv";
        String ncFilePath = "D:/wechat/WeChat Files/wxid_vu6eu977ndi121/FileStorage/File/2024-07/tas_day_IITM-ESM_historical_r1i1p1f1_gn_2014.nc";
        String csvOutputPath = "d:/temp.csv";
        String variableName = "tas";

        try {
            // 读取城市多边形数据
            Map<String, Polygon> cityPolygons = readCityPolygons(csvInputPath);

            // 读取NC文件数据
            NetcdfDataset dataset = NetcdfDataset.openDataset(ncFilePath);
            Variable tasVar = dataset.findVariable(variableName);
            Variable latVar = dataset.findVariable("lat");
            Variable lonVar = dataset.findVariable("lon");
            Variable timeVar = dataset.findVariable("time");

            // 获取坐标数据
            double[] lats = (double[]) latVar.read().copyTo1DJavaArray();
            double[] lons = (double[]) lonVar.read().copyTo1DJavaArray();

            // 计算每个城市的平均tas值
            Map<String, List<Double>> cityAverageTas = calculateCityAverageTas(cityPolygons, tasVar, lats, lons, timeVar, (timeVar != null));

            // 输出结果到CSV
            writeToCsv(csvOutputPath, cityAverageTas, timeVar, (timeVar != null));

            dataset.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<String, Polygon> readCityPolygons(String csvPath) throws Exception {
        Map<String, Polygon> cityPolygons = new HashMap<>();
        GeometryFactory geometryFactory = new GeometryFactory();
        try (CSVReader reader = new CSVReader(new FileReader(csvPath))) {
            // 跳过第一行（标题行）
            reader.readNext();
            
            String[] nextLine;
            String currentCityId = null;
            List<Coordinate> coordinates = new ArrayList<>();
            while ((nextLine = reader.readNext()) != null) {
                String cityId = nextLine[0];
                double lat = Double.parseDouble(nextLine[1]);
                double lon = Double.parseDouble(nextLine[2]);
                
                if (currentCityId == null || !currentCityId.equals(cityId)) {
                    if (currentCityId != null) {
                        coordinates.add(coordinates.get(0)); // 闭合多边形
                        Polygon polygon = geometryFactory.createPolygon(new CoordinateArraySequence(coordinates.toArray(new Coordinate[0])));
                        cityPolygons.put(currentCityId, polygon);
                    }
                    currentCityId = cityId;
                    coordinates.clear();
                }
                coordinates.add(new Coordinate(lon, lat));
            }
            // 处理最后一个城市
            if (currentCityId != null) {
                coordinates.add(coordinates.get(0)); // 闭合多边形
                Polygon polygon = geometryFactory.createPolygon(new CoordinateArraySequence(coordinates.toArray(new Coordinate[0])));
                cityPolygons.put(currentCityId, polygon);
            }
        }
        return cityPolygons;
    }

    private static Map<String, List<Double>> calculateCityAverageTas(Map<String, Polygon> cityPolygons,
                                                                     Variable tasVar, double[] lats, double[] lons,
                                                                     Variable timeVar, boolean hasTimeCoordinate) throws Exception {
        Map<String, List<Double>> cityAverageTas = new HashMap<>();
        Map<String, Integer> cityPointCounts = new HashMap<>(); // 修改为记录每个城市的总点数
        int[] shape = tasVar.getShape();
        int timeSteps = hasTimeCoordinate ? shape[0] : 1;
        GeometryFactory geometryFactory = new GeometryFactory();

        // 创建空间索引
        STRtree spatialIndex = new STRtree();
        for (Map.Entry<String, Polygon> entry : cityPolygons.entrySet()) {
            spatialIndex.insert(entry.getValue().getEnvelopeInternal(), entry);
        }
        spatialIndex.build();

        // 获取缺失值
        Number missingValue = tasVar.findAttribute("_FillValue").getNumericValue();
        double fillValue = missingValue.doubleValue();

        // 读取所有时间步的数据
        Array tasData = hasTimeCoordinate ? tasVar.read() : tasVar.read().reshape(new int[]{1, shape[1], shape[2]});

        for (int i = 0; i < lats.length; i++) {
            for (int j = 0; j < lons.length; j++) {
                final Point point = geometryFactory.createPoint(new Coordinate(lons[j], lats[i]));
                final int[] finalI = {i};
                final int[] finalJ = {j};

                spatialIndex.query(point.getEnvelopeInternal(), new ItemVisitor() {
                    @Override
                    public void visitItem(Object item) {
                        @SuppressWarnings("unchecked")
                        Map.Entry<String, Polygon> entry = (Map.Entry<String, Polygon>) item;
                        if (entry.getValue().contains(point)) {
                            String cityId = entry.getKey();
                            List<Double> cityValues = cityAverageTas.computeIfAbsent(cityId, k -> new ArrayList<>(Collections.nCopies(timeSteps, 0.0)));
                            cityPointCounts.merge(cityId, 1, Integer::sum); // 增加城市点数计数

                            for (int t = 0; t < timeSteps; t++) {
                                double tasValue = tasData.getDouble(t * lats.length * lons.length + finalI[0] * lons.length + finalJ[0]);
                                if (Double.compare(tasValue, fillValue) != 0) {
                                    cityValues.set(t, cityValues.get(t) + tasValue);
                                }
                            }
                        }
                    }
                });
            }
        }

        // 计算平均值
        for (String cityId : cityAverageTas.keySet()) {
            List<Double> values = cityAverageTas.get(cityId);
            int count = cityPointCounts.get(cityId);
            for (int t = 0; t < timeSteps; t++) {
                if (count > 0) {
                    values.set(t, values.get(t) / count);
                } else {
                    values.set(t, Double.NaN); // 如果该时间步没有有效数据，设置为NaN
                }
            }
        }

        // 输出每个城市ID包含的栅格点数量
        System.out.println("每个城市ID包含的栅格点数量:");
        for (Map.Entry<String, Integer> entry : cityPointCounts.entrySet()) {
            System.out.println("城市ID: " + entry.getKey() + ", 栅格点数量: " + entry.getValue());
        }

        return cityAverageTas;
    }

    private static void writeToCsv(String csvPath, Map<String, List<Double>> cityAverageTas, Variable timeVar, boolean hasTimeCoordinate) throws Exception {
        try (CSVWriter writer = new CSVWriter(new FileWriter(csvPath))) {
            String[] header = hasTimeCoordinate ? new String[]{"城市ID", "时间", "平均tas"} : new String[]{"城市ID", "平均tas"};
            writer.writeNext(header);

            for (Map.Entry<String, List<Double>> entry : cityAverageTas.entrySet()) {
                String cityId = entry.getKey();
                List<Double> averages = entry.getValue();

                if (hasTimeCoordinate) {
                    Array timeData = timeVar.read();
                    for (int i = 0; i < averages.size(); i++) {
                        writer.writeNext(new String[]{cityId, String.valueOf(timeData.getDouble(i)), String.valueOf(averages.get(i))});
                    }
                } else {
                    writer.writeNext(new String[]{cityId, String.valueOf(averages.get(0))});
                }
            }
        }
    }
}
