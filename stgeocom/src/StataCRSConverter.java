import org.locationtech.proj4j.*;
import com.stata.sfi.Data;
import com.stata.sfi.SFIToolkit;

public class StataCRSConverter {
    
    // Proj4J核心对象
    private static final CRSFactory crsFactory = new CRSFactory();
    private static final CoordinateTransformFactory ctFactory = new CoordinateTransformFactory();
    
    /**
     * 核心坐标转换方法
     * @param x 输入X坐标
     * @param y 输入Y坐标
     * @param sourceCRS 源坐标系定义（如"EPSG:4326"）
     * @param targetCRS 目标坐标系定义
     */
    public static double[] convertCoordinate(double x, double y, 
                                           String sourceCRS, 
                                           String targetCRS) throws Proj4jException {
        CoordinateReferenceSystem src = crsFactory.createFromName(sourceCRS);
        CoordinateReferenceSystem tgt = crsFactory.createFromName(targetCRS);
        CoordinateTransform transform = ctFactory.createTransform(src, tgt);
        
        ProjCoordinate result = new ProjCoordinate();
        transform.transform(new ProjCoordinate(x, y), result);
        return new double[]{result.x, result.y};
    }

    /**
     * 主处理方法
     */
    public static void main(String x, String y, String newx, String newy, 
                          String sourceCRS, String targetCRS) {
        try {
            // 获取变量索引
            int xIndex = Data.getVarIndex(x);
            int yIndex = Data.getVarIndex(y);
            
            // 创建新变量
            int newXIndex = createVariable(newx);
            int newYIndex = createVariable(newy);
            
            // 批量处理数据
            processObservations(xIndex, yIndex, newXIndex, newYIndex, sourceCRS, targetCRS);
            
            Data.updateModified();
        } catch (Exception e) {
            handleFatalError(e);
        }
    }

    private static void processObservations(int xIndex, int yIndex, 
                                          int newXIndex, int newYIndex,
                                          String sourceCRS, 
                                          String targetCRS) {
        long totalObs = Data.getObsTotal();
        for (long obs = 1; obs <= totalObs; obs++) {
            try {
                double xVal = Data.getNum(xIndex, obs);
                double yVal = Data.getNum(yIndex, obs);
                
                double[] converted = convertCoordinate(xVal, yVal, sourceCRS, targetCRS);
                
                Data.storeNumFast(newXIndex, obs, converted[0]);
                Data.storeNumFast(newYIndex, obs, converted[1]);
            } catch (Exception e) {
                handleObservationError(newXIndex, newYIndex, obs, e);
            }
        }
    }

    // 以下工具方法与原实现保持兼容
    private static void handleObservationError(int xIndex, int yIndex, 
                                             long obs, Exception e) {
        try {
            Data.storeNum(xIndex, obs, Data.getMissingValue());
            Data.storeNum(yIndex, obs, Data.getMissingValue());
            SFIToolkit.errorln("Observation " + obs + " failed: " + getRootCause(e).getMessage());
        } catch (Exception ex) {
            SFIToolkit.errorln("Error handling failed: " + ex.getMessage());
        }
    }

    private static int createVariable(String varName) throws Exception {
        // if (Data.getVarIndex(varName) != -1) {
        //     throw new Exception("Variable exists: " + varName);
        // }
        return Data.addVarDouble(varName);
    }

    private static void handleFatalError(Exception e) {
        try {
            SFIToolkit.errorln("Fatal error: " + getRootCause(e).getMessage());
        } catch (Exception ex) {
            System.err.println("Critical failure: " + ex.getMessage());
        }
    }

    private static Throwable getRootCause(Throwable e) {
        while (e.getCause() != null) {
            e = e.getCause();
        }
        return e;
    }
}