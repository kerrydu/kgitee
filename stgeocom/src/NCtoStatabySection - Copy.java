
import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.nc2.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import com.stata.sfi.*;
import ucar.nc2.dataset.NetcdfDataset;

public class NCtoStatabySection {
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000; // 1 billion elements, about 4GB of data

    public static void main(String ncFilePath, String variableName, String strorigin, String strsize) {

        String[] originStr = strorigin.split(" ");
        String[] sizeStr =   strsize.split(" ");
        int[] origin = new int[originStr.length];
        int[] size = new int[sizeStr.length];
        
        for (int i = 0; i < originStr.length; i++) {
            origin[i] = Integer.parseInt(originStr[i]);
        }
        
        for (int i = 0; i < sizeStr.length; i++) {
            size[i] = Integer.parseInt(sizeStr[i]);
        } 

        try (NetcdfDataset ncFile = NetcdfDataset.openDataset(ncFilePath)) {
            Variable mainVar = ncFile.findVariable(variableName);
            if (mainVar == null) {
                System.out.println("Variable " + variableName + " not found");
                return;
            }

            List<Variable> coordVars = new ArrayList<>();
            for (Dimension dim : mainVar.getDimensions()) {
                Variable coordVar = ncFile.findVariable(dim.getShortName());
                if (coordVar != null) {
                    coordVars.add(coordVar);
                }
            }

            long totalSize = calculateTotalSize(size);
            System.out.println("Number of data rows to be read: " + totalSize);

            if (totalSize > MAX_SAFE_ELEMENTS) {
                System.out.println("Warning: Dataset is too large and may cause memory overflow. Operation cancelled.");
                return;
            }

            createStataVariables(coordVars, variableName, totalSize);
            processData(coordVars, mainVar, totalSize, origin, size);
            System.out.println("Data has been successfully written to Stata dataset");
        } catch (IOException e) {
            System.err.println("IO error occurred while processing file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static long calculateTotalSize(int[] size) {
        long totalSize = 1;
        for (int s : size) {
            totalSize *= s;
        }
        return totalSize;
    }

    private static void createStataVariables(List<Variable> coordVars, String variableName, long totalSize) {
        Data.setObsTotal((int) totalSize);
        for (Variable coordVar : coordVars) {
            Data.addVarDouble(coordVar.getShortName());
        }
        Data.addVarDouble(variableName);
    }

    private static void processData(List<Variable> coordVars, Variable mainVar, long totalSize, int[] origin, int[] size) throws IOException {
        for (long i = 0; i < totalSize; i++) {
            int[] indices = calculateIndices(i, size);
            
            int col = 1;
            for (Variable coordVar : coordVars) {
                try {
                    int coordIndex = coordVars.indexOf(coordVar);
                    Array coordData = coordVar.read(new int[]{origin[coordIndex] + indices[coordIndex]}, new int[]{1});
                    Data.storeNumFast(col++, (int)i + 1, ((Number) coordData.getObject(0)).doubleValue());
                } catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
            }

            try {
                int[] dataOrigin = new int[mainVar.getRank()];
                int[] dataSize = new int[mainVar.getRank()];
                for (int j = 0; j < mainVar.getRank(); j++) {
                    dataOrigin[j] = (j < indices.length) ? origin[j] + indices[j] : 0;
                    dataSize[j] = 1;
                }
                Array mainData = mainVar.read(dataOrigin, dataSize);
                Data.storeNumFast(col, (int)i + 1, mainData.getDouble(0));
            } catch (InvalidRangeException e) {
                e.printStackTrace();
            }
        }
        Data.updateModified();
    }

    private static int[] calculateIndices(long flatIndex, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length - 1; i >= 0; i--) {
            indices[i] = (int) (flatIndex % shape[i]);
            flatIndex /= shape[i];
        }
        return indices;
    }
}
