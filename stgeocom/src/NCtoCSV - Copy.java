import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.nc2.Dimension;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import com.stata.sfi.*;

public class NCtoCSV {
    private static final int BUFFER_SIZE = 8192 * 1024; // 8MB buffer
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000; // 1 billion elements (~4GB data)

    public static void main(String[] args) {
        String ncFilePath = args[0];
        String csvFilePath = args[1];
        String variableName = args[2];

        try (NetcdfDataset ncDataset = NetcdfDataset.openDataset(ncFilePath)) {
            Variable mainVar = ncDataset.findVariable(variableName);
            if (mainVar == null) {
                // System.out.println("Variable " + variableName + " not found");
                SFIToolkit.errorln("Variable " + variableName + " not found");
                return;
            }

            List<Variable> coordVars = new ArrayList<>();
            for (Dimension dim : mainVar.getDimensions()) {
                Variable coordVar = ncDataset.findVariable(dim.getShortName());
                if (coordVar != null) {
                    coordVars.add(coordVar);
                }
            }

            long totalSize = calculateTotalSize(coordVars);
            System.out.println("Total rows: " + totalSize);

            if (totalSize > MAX_SAFE_ELEMENTS) {
                //System.out.println("Warning: Dataset too large, operation cancelled to prevent memory overflow.");
                SFIToolkit.errorln("Warning: Dataset too large, operation cancelled to prevent memory overflow.");
                return;
            }

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath), BUFFER_SIZE)) {
                writeHeader(writer, coordVars, variableName);
                processData(writer, coordVars, mainVar, totalSize);
                System.out.println("Data successfully written to CSV file: " + csvFilePath);
            }
        } catch (IOException e) {
            //e.printStackTrace();
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        }
    }

    private static long calculateTotalSize(List<Variable> coordVars) {
        long totalSize = 1;
        for (Variable coordVar : coordVars) {
            totalSize *= coordVar.getSize();
        }
        return totalSize;
    }

    private static void writeHeader(BufferedWriter writer, List<Variable> coordVars, String variableName) throws IOException {
        for (Variable coordVar : coordVars) {
            writer.write(coordVar.getShortName() + ",");
        }
        writer.write(variableName);
        writer.newLine();
    }

    private static void processData(BufferedWriter writer, List<Variable> coordVars, Variable mainVar, long totalSize) throws IOException {
        StringBuilder sb = new StringBuilder();
        int[] shape = coordVars.stream().mapToInt(var -> (int) var.getSize()).toArray();

        for (long i = 0; i < totalSize; i++) {
            int[] indices = calculateIndices(i, shape);
            
            for (Variable coordVar : coordVars) {
                try {
                    Array coordData = coordVar.read(new int[]{indices[coordVars.indexOf(coordVar)]}, new int[]{1});
                    sb.append(coordData.getObject(0)).append(",");
                } catch (InvalidRangeException e) {
                    //e.printStackTrace();
                    SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
                }
            }

            try {
                int[] origin = new int[mainVar.getRank()];
                int[] size = new int[mainVar.getRank()];
                for (int j = 0; j < mainVar.getRank(); j++) {
                    origin[j] = (j < indices.length) ? indices[j] : 0;
                    size[j] = 1;
                }
                Array mainData = mainVar.read(origin, size);
                sb.append(mainData.getDouble(0)).append("\n");
            } catch (InvalidRangeException e) {
                //e.printStackTrace();
                SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
            }

            if (sb.length() > BUFFER_SIZE) {
                writer.write(sb.toString());
                sb.setLength(0);
            }
        }

        if (sb.length() > 0) {
            writer.write(sb.toString());
        }
    }

    private static int[] calculateIndices(long flatIndex, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length - 1; i >= 0; i--) {
            indices[i] = (int) (flatIndex % shape[i]);
            flatIndex /= shape[i];
        }
        return indices;
    }
}