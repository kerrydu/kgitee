import ucar.nc2.Variable;
import ucar.ma2.Array;
import ucar.nc2.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.InvalidRangeException;
import com.stata.sfi.*;
import ucar.nc2.dataset.NetcdfDataset;

public class NCtoStata {
    private static final long MAX_SAFE_ELEMENTS = 1_000_000_000; // 1 billion elements, about 4GB of data

    public static void main(String ncFilePath, String variableName) {
        try (NetcdfDataset ncFile = NetcdfDataset.openDataset(ncFilePath)) {
            Variable mainVar = ncFile.findVariable(variableName);
            if (mainVar == null) {
                //System.out.println("Variable " + variableName + " not found");
                SFIToolkit.errorln("Variable " + variableName + " not found");
                return;
            }

            // =========== 修改部分开始 ===========
            List<Variable> coordVars = new ArrayList<>();
            Map<Variable, Integer> dimOrderMap = new HashMap<>();

            // 按主变量的维度原始顺序遍历
            List<Dimension> mainDimensions = mainVar.getDimensions();
            for (int dimIndex = 0; dimIndex < mainDimensions.size(); dimIndex++) {
                Dimension dim = mainDimensions.get(dimIndex);
                Variable coordVar = ncFile.findVariable(dim.getShortName());
                if (coordVar != null) {
                    coordVars.add(coordVar);
                    // 记录维度原始位置
                    dimOrderMap.put(coordVar, dimIndex); 
                }
            }
            // =========== 修改部分结束 ===========

            long totalSize = calculateTotalSize(coordVars);
            System.out.println("Total number of data rows: " + totalSize);

            if (totalSize > MAX_SAFE_ELEMENTS) {
                System.out.println("Warning: Dataset is too large and may cause memory overflow. Operation cancelled.");
                return;
            }

            createStataVariables(coordVars, variableName, totalSize);
            processData(coordVars, mainVar, totalSize, dimOrderMap); // 传递映射表
            System.out.println("Data has been successfully written to Stata dataset");
        } catch (IOException e) {
            //System.err.println("IO error occurred while processing file: " + e.getMessage());
            SFIToolkit.errorln("IO error occurred while processing file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static long calculateTotalSize(List<Variable> coordVars) {
        long totalSize = 1;
        for (Variable coordVar : coordVars) {
            totalSize *= coordVar.getSize();
        }
        return totalSize;
    }

    private static void createStataVariables(List<Variable> coordVars, String variableName, long totalSize) {
        if (totalSize > Integer.MAX_VALUE) {
            // System.out.println("Dataset exceeds Stata's maximum observation limit");
            SFIToolkit.error("Dataset exceeds Stata's maximum observation limit");
            return;
        }
        Data.setObsTotal((int) totalSize); // 添加校验后强制转换
        for (Variable coordVar : coordVars) {
            Data.addVarDouble(coordVar.getShortName());
        }
        Data.addVarDouble(variableName);
    }

    private static void processData(List<Variable> coordVars, Variable mainVar, long totalSize) throws IOException {
        int[] shape = coordVars.stream().mapToInt(var -> (int) var.getSize()).toArray();

        for (long i = 0; i < totalSize; i++) {
            int[] indices = calculateIndices(i, shape);
            
            int col = 1;
            // 修改坐标变量读取部分
            for (Variable coordVar : coordVars) {
                try {
                    // 使用映射表获取正确的维度索引
                    int correctIndex = dimOrderMap.get(coordVar);
                    Array coordData = coordVar.read(
                        new int[]{indices[correctIndex]}, 
                        new int[]{1}
                    );
                    Data.storeNumFast(col++, (int)i + 1, 
                        ((Number) coordData.getObject(0)).doubleValue());
                } catch (InvalidRangeException e) {
                        // System.err.println("Invalid index at i=" + i);
                        Data.close();
                        SFIToolkit.errorln("Invalid index at i=" + i);
                        SFIToolkit.errorln(SFIToolkit.stackTraceToString(e))
                        throw new RuntimeException(e); // 终止处理
                }
            }

            try {
                int[] origin = new int[mainVar.getRank()];
                int[] size = new int[mainVar.getRank()];
                for (int j = 0; j < mainVar.getRank(); j++) {
                    origin[j] = (j < indices.length) ? indices[j] : 0;
                    size[j] = 1;
                }
                Array mainData = mainVar.read(origin, size);
                Data.storeNumFast(col, (int)i + 1, mainData.getDouble(0));
            }catch (InvalidRangeException e) {
                        // System.err.println("Invalid index at i=" + i);
                        Data.close();
                        SFIToolkit.errorln(SFIToolkit.stackTraceToString(e))
                        throw new RuntimeException(e); // 终止处理
            }
        }
        Data.updateModified();
    }

    private static int[] calculateIndices(long flatIndex, int[] shape) {
        int[] indices = new int[shape.length];
        for (int i = shape.length - 1; i >= 0; i--) {
            indices[i] = (int) (flatIndex % shape[i]);
            flatIndex /= shape[i];
        }
        return indices;
    }
}
