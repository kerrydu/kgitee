import org.locationtech.jts.geom.*;
import org.locationtech.jts.index.strtree.STRtree;
import java.io.*;
import java.util.*;

public class gridMatchAera {
    
    public static void main(String[] args) throws Exception {
        String input_grid_points = "D:/wechat/WeChat Files/wxid_vu6eu977ndi121/FileStorage/File/2024-07/grid_points.csv";
        String input_cities = "D:/wechat/WeChat Files/wxid_vu6eu977ndi121/FileStorage/File/2024-07/cities.csv";
        String output_matched_points = "D:/wechat/WeChat Files/wxid_vu6eu977ndi121/FileStorage/File/2024-07/matched_points.csv";
        // 1. 读取栅格点坐标
        List<GridPoint> gridPoints = readPointsFromCSV(input_grid_points);

        // 2. 读取城市多边形数据并构建空间索引
        Map<String, List<Coordinate>> cityCoordinates = readCityCoordinatesFromCSV(input_cities);
        List<City> cities = createCities(cityCoordinates);
        STRtree spatialIndex = buildSpatialIndex(cities);

        // 3. 匹配栅格点和城市
        List<MatchedPoint> matchedPoints = matchPointsToCities(gridPoints, spatialIndex);

        // 4. 输出结果到CSV文件
        writeResultsToCSV(matchedPoints, output_matched_points);
    }

    // 从CSV读取点数据
    private static List<GridPoint> readPointsFromCSV(String filename) throws IOException {
        List<GridPoint> points = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            br.readLine(); // 跳过标题行
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                double lat = Double.parseDouble(values[0]);
                double lon = Double.parseDouble(values[1]);
                points.add(new GridPoint(lat, lon));
            }
        }
        return points;
    }

    // 从CSV读取城市多边形数据
    private static Map<String, List<Coordinate>> readCityCoordinatesFromCSV(String filename) throws IOException {
        Map<String, List<Coordinate>> cityCoordinates = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            br.readLine(); // 跳过标题行
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                String id = values[0];
                String[] coordinates = values[1].split(" ");
                List<Coordinate> coordinatesList = new ArrayList<>();
                for (String coord : coordinates) {
                    String[] parts = coord.split(",");
                    coordinatesList.add(new Coordinate(Double.parseDouble(parts[0]), Double.parseDouble(parts[1])));
                }
                cityCoordinates.put(id, coordinatesList);
            }
        }
        return cityCoordinates;
    }

    // 构建空间索引
    private static STRtree buildSpatialIndex(List<City> cities) {
        STRtree index = new STRtree();
        for (City city : cities) {
            index.insert(city.getGeometry().getEnvelopeInternal(), city);
        }
        index.build();
        return index;
    }

    // 匹配点和城市
    private static List<MatchedPoint> matchPointsToCities(List<GridPoint> points, STRtree spatialIndex) {
        List<MatchedPoint> matchedPoints = new ArrayList<>();
        GeometryFactory geometryFactory = new GeometryFactory();

        for (GridPoint point : points) {
            Point jtsPoint = geometryFactory.createPoint(new Coordinate(point.lon, point.lat));
            @SuppressWarnings("unchecked")
            List<City> candidateCities = (List<City>) spatialIndex.query(jtsPoint.getEnvelopeInternal());
            
            City matchedCity = null;
            double minDistance = Double.MAX_VALUE;

            for (City city : candidateCities) {
                double distance = jtsPoint.distance(city.getGeometry().getCentroid());
                if (distance < minDistance) {
                    minDistance = distance;
                    matchedCity = city;
                }
            }

            if (matchedCity != null) {
                matchedPoints.add(new MatchedPoint(matchedCity.id, point.lat, point.lon));
            }
        }

        return matchedPoints;
    }

    // 将结果写入CSV
    private static void writeResultsToCSV(List<MatchedPoint> matchedPoints, String filename) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            bw.write("RegionID,Latitude,Longitude");
            bw.newLine();
            for (MatchedPoint point : matchedPoints) {
                bw.write(String.format("%s,%.6f,%.6f", point.regionId, point.lat, point.lon));
                bw.newLine();
            }
        }
    }

    // 定义栅格点类
    private static class GridPoint {
        double lat;
        double lon;

        GridPoint(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }
    }

    // 定义城市类
    private static class City {
        String id;
        Geometry geometry;

        City(String id, Geometry geometry) {
            this.id = id;
            this.geometry = geometry;
        }

        Envelope getEnvelope() {
            return geometry.getEnvelopeInternal();
        }

        Geometry getGeometry() {
            return geometry;
        }
    }

    // 定义匹配结果类
    private static class MatchedPoint {
        String regionId;
        double lat;
        double lon;

        MatchedPoint(String regionId, double lat, double lon) {
            this.regionId = regionId;
            this.lat = lat;
            this.lon = lon;
        }
    }

    // 创建城市对象
    private static List<City> createCities(Map<String, List<Coordinate>> cityCoordinates) {
        List<City> cities = new ArrayList<>();
        GeometryFactory geometryFactory = new GeometryFactory();
        for (Map.Entry<String, List<Coordinate>> entry : cityCoordinates.entrySet()) {
            String id = entry.getKey();
            List<Coordinate> coordinates = entry.getValue();
            Geometry geometry = geometryFactory.createPolygon(coordinates.toArray(new Coordinate[coordinates.size()]));
            cities.add(new City(id, geometry));
        }
        return cities;
    }
}
