/****************************************************************************
 * ncdisp_core - Display metadata for a variable in NetCDF file
 *
 * Description:
 *   Displays detailed structural information and metadata about a variable 
 *   in a NetCDF file, including dimensions, attributes, and scale parameters.
 *   Uses Java backend via netcdfAll-5.6.0.jar to read NetCDF files.
 * 
 * Syntax:
 *   ncdisp_core varname using filename [, display]
 * 
 * Parameters:
 *   varname    - Name of the variable in the NetCDF file to display info for
 *   filename   - Path to the NetCDF file (.nc)
 *   display    - Optional display parameter (currently not used)
 * 
 * Requirements:
 *   - netcdfAll-5.6.0.jar must be installed
 *   - Java environment must be properly configured
 *   - Execute ncread_init before first use to set up the jar path
 * 
 * Returns:
 *   r(varname)     - Name of the displayed variable
 *   r(dimensions)  - Space-separated list of dimension sizes
 *   r(coordinates) - Coordinate axes information
 *   r(datatype)    - Data type of the variable
 * 
 * Output information:
 *   - Variable structure (name and type)
 *   - Dimensions (name, length, coordinate status)
 *   - Scale/offset parameters (scale_factor, add_offset, missing values)
 *   - Variable attributes
 *   - Units and other metadata
 * 
 * Examples:
 *   ncdisp_core temperature using "climate_data.nc"
 * 
 * Notes:
 *   This program is part of the stgeocom package for handling geospatial data.
 *   Paths with backslashes are automatically converted to forward slashes.
 * 
 * Version: 18.0+
 * Last modified: 2024-07-28
 ****************************************************************************/

*! 2024-07-28
cap program drop ncdisp_core
program define ncdisp_core,rclass
version 18


cap findfile netcdfAll-5.6.0.jar

if _rc{
    cap findfile path_ncreadjar.ado 
    if _rc {
        di as error "jar path NOT specified, use ncread_init for setting up"
        exit
        
    }

    path_ncreadjar
    local path `r(path)'

    cap findfile netcdfAll-5.6.0.jar, path(`"`path'"')
    if _rc {
        di as error "netcdfAll-5.6.0.jar NOT found"
        di as error "use netcdf_init for re-initializing Java environment"
        di as error "make sure netcdfAll-5.6.0.jar exists in your specified directory"
        exit
    }

    qui adopath ++ `"`path'"'

}


//stgeocominit
syntax anything using/ ,[display]
removequotes,file(`anything')
local varname `r(file)'
removequotes,file(`using')
local file `r(file)'
local file = subinstr(`"`file'"',"\","/",.)

java: NetCDFReader.printVarStructure("`file'","`varname'");

return local varname `varname'
return local dimensions `dimensions' 
return local coordinates `coordAxes' 
return local datatype `datatype'
end


////////////////////////////////////////
cap program drop removequotes
program define removequotes,rclass
version 16
syntax, file(string) 
return local file `file'
end


java:
/cp "netcdfAll-5.6.0.jar"
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import java.io.IOException;
import com.stata.sfi.*;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class NetCDFReader {
    public static void printVarStructure(String ncFileName, String variableName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            Variable variable = netcdfDataset.findVariable(variableName);
            if (variable == null) {
                SFIToolkit.errorln("Variable " + variableName + " not found");
                return;
            }
    
            System.out.println("\n=== Variable Structure ===");
            System.out.printf("Name: %-25s Type: %-15s%n", 
                variable.getShortName(), variable.getDataType());
            
            System.out.println("\n=== Dimensions ===");
            System.out.printf("%-15s %-8s %-15s%n", "Dimension", "Length", "Coordinate");
            variable.getDimensions().forEach(dim -> 
                System.out.printf("%-15s %-8d %-15s%n",
                    dim.getShortName(),
                    dim.getLength(),
                    isCoordinateAxis(dim) ? "[Yes]" : "")
            );
    
            // 新增标度参数输出区块
            System.out.println("\n=== Scale/Offset Parameters ===");
            String[] scaleAtts = {"scale_factor", "add_offset", "missing_value", "_FillValue"};
            Arrays.stream(scaleAtts).forEach(attName -> {
                Attribute att = findAttributeRecursive(variable, attName);
                if (att != null && att.getDataType().isNumeric()) {
                    double value = att.getNumericValue().doubleValue();
                    System.out.printf("%-15s: %-12.6f (Type: %s)%n",
                        attName.replace("_", " "), 
                        value,
                        att.getDataType());
                }
            });
    
            System.out.println("\n=== Attributes ===");
            variable.getAttributes().forEach(attr -> {
                String value = attr.getDataType().isString() ? 
                    attr.getStringValue() : attr.getNumericValue().toString();
                System.out.printf("%-20s: %s%n", attr.getShortName(), value);
            });
    
            System.out.println("\n=== Metadata ===");
            Attribute unitAtt = variable.findAttribute("units");
            if (unitAtt != null) {
                String unit = unitAtt.getStringValue()
                    .replace("degrees_", "°")
                    .replace("meters", "m");
                System.out.printf("%-15s: %s (original: %s)%n", 
                    "Units", unit, unitAtt.getStringValue());
                Macro.setLocal("unit", unitAtt.getStringValue());
            }
    
            int[] shape = variable.getShape();
            System.out.printf("%n%-15s: %s%n", "Shape", Arrays.toString(shape));
            System.out.printf("%-15s: %s%n", "Data Type", variable.getDataType());
            
            Macro.setLocal("dimensions", Arrays.stream(shape)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" ")));
                
        } catch (IOException | IllegalArgumentException e) {
            SFIToolkit.errorln(SFIToolkit.stackTraceToString(e));
        }
    }
    
    // 添加属性查找方法
    private static Attribute findAttributeRecursive(Variable var, String attName) {
        Attribute att = var.findAttribute(attName);
        if (att == null) {
            att = var.getParentGroup().getNetcdfFile().findGlobalAttribute(attName);
        }
        return att;
    }

    private static String truncate(String value, int maxLength) {
        return value.length() > maxLength ? 
            value.substring(0, maxLength-3) + "..." : value;
    }

    private static boolean isCoordinateAxis(Dimension dim) {
        return dim.getShortName().matches("(?i)lat|lon|time|height|depth") 
            || dim.getGroup().findVariable(dim.getShortName()) != null;
    }



}




end
