
*! 2024-07-28
cap program drop ncdisp
program define ncdisp,rclass
version 18

cap findfile netcdfAll-5.6.0.jar

if _rc{
    cap findfile path_ncreadjar.ado 
    if _rc {
        di as error "jar path NOT specified, use netcdf_init for setting up"
        exit
        
    }

    path_ncreadjar
    local path `r(path)'

    cap findfile netcdfAll-5.6.0.jar, path(`"`path'"')
    if _rc {
        di as error "netcdfAll-5.6.0.jar NOT found"
        di as error "use netcdf_init for re-initializing Java environment"
        di as error "make sure netcdfAll-5.6.0.jar in your specified directory"
        exit
    }

    qui adopath ++ `"`path'"'

}

ncdisp_core `0'

end






///////////////////////////////////////////////////////////////////////
// /*
//     Program: ncdisp
//     Author: Unknown
//     Date: 2024-07-28

//     Description:
//     This program is used to display the structure of a NetCDF file and retrieve information about a specific variable.

//     Syntax:
//     ncdisp anything using file(string)

//     Arguments:
//     - anything: Any valid Stata expression.
//     - file: The path to the NetCDF file.

//     Returns:
//     - varname: The name of the variable.
//     - dimensions: The dimensions of the variable.
//     - coordinates: The coordinate axes of the variable.
//     - datatype: The data type of the variable.

//     Example:
//     ncdisp varname using "path/to/file.nc"
// */
// *! 2024-07-28
// cap program drop ncdisp
// program define ncdisp,rclass
// version 18


// cap findfile netcdfAll-5.6.0.jar

// if _rc{
//     cap findfile path_ncreadjar.ado 
//     if _rc {
//         di as error "jar path NOT specified, use ncread_init for setting up"
//         exit
        
//     }

//     path_ncreadjar
//     local path `r(path)'

//     cap findfile netcdfAll-5.6.0.jar, path(`"`path'"')
//     if _rc {
//         di as error "netcdfAll-5.6.0.jar NOT found"
//         di as error "use netcdf_init for re-initializing Java environment"
//         di as error "make sure netcdfAll-5.6.0.jar exists in your specified directory"
//         exit
//     }

//     qui adopath ++ `"`path'"'

// }


// //stgeocominit
// syntax anything using/ ,[display]
// removequotes,file(`anything')
// local varname `r(file)'
// removequotes,file(`using')
// local file `r(file)'
// local file = subinstr(`"`file'"',"\","/",.)
// di _n 
// java clear
// java: /cp "netcdfAll-5.6.0.jar"
// java: /open "NetCDFReader.java"
// java: NetCDFReader.printVarStructure("`file'","`varname'");

// return local varname `varname'
// return local dimensions `dimensions' 
// return local coordinates `coordAxes' 
// return local datatype `datatype'
// end

// cap program drop removequotes
// program define removequotes,rclass
// version 16
// syntax, file(string) 
// return local file `file'
// end

