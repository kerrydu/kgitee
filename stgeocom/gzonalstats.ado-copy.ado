cap program drop gzonalstats
program define gzonalstats
version 18.0
syntax using/, SHPfile(string) [STATs(string) band(integer 0)]

// Default value for stats if not provided
if missing("`stats'") {
    local stats "count avg min max"
}

// Use the arguments passed to the program
local tifffile `"`using'"'

java: zonalstatics.main("`shpfile'", "`tifffile'", `band', "`stats'")

end

java:

// Core GeoTools libraries
/cp gt-main-32.0.jar
/cp gt-coverage-32.0.jar
/cp gt-shapefile-32.0.jar
/cp gt-geotiff-32.0.jar
/cp gt-process-raster-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-epsg-extension-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-api-32.0.jar
/cp gt-metadata-32.0.jar

// External dependencies
/cp json-simple-1.1.1.jar
/cp commons-lang3-3.15.0.jar
/cp commons-io-2.16.1.jar
/cp jts-core-1.20.0.jar

// Java imports
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// GeoTools imports
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.api.feature.simple.SimpleFeature;
import org.geotools.api.referencing.crs.CoordinateReferenceSystem;
import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ReprojectingFeatureCollection;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.process.raster.RasterZonalStatistics;
import org.geotools.referencing.CRS;

public class zonalstatics {

    static {
        // Disable the JSON-related service loading at startup
        System.setProperty("org.geotools.referencing.forceXY", "true");
        System.setProperty("org.geotools.factory.hideLegacyServiceImplementations", "true");
        
        // Suppress specific service loader errors
        Logger logger = Logger.getLogger("org.geotools.util.factory");
        logger.setLevel(Level.SEVERE);
    }

    public static void main(String shpPath, String tiffPath, int bandIndex, String statsParam) throws Exception {
        // Declare resources outside the try block so we can close them in finally
        ShapefileDataStore shapefileDataStore = null;
        AbstractGridCoverage2DReader reader = null;
        SimpleFeatureIterator featureIterator = null;
        
        try {
            // Disable excessive logging
            Logger.getGlobal().setLevel(Level.SEVERE);
            
            // Parse requested statistics
            String[] requestedStats = statsParam.toLowerCase().split("\\s+");
            boolean showCount = false;
            boolean showAvg = false;
            boolean showMin = false;
            boolean showMax = false;
            boolean showStd = false;
            
            for (String stat : requestedStats) {
                switch(stat.trim()) {
                    case "count": showCount = true; break; 
                    case "avg": showAvg = true; break;
                    case "min": showMin = true; break;
                    case "max": showMax = true; break;
                    case "std": showStd = true; break;
                }
            }
            
            // Check if vector data file exists
            File shpFile = new File(shpPath);
            if (!shpFile.exists()) {
                System.out.println("Shapefile does not exist: " + shpPath);
                return;
            }

            // Load vector data (shapefile)
            ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();
            Map<String, Object> shpParams = new HashMap<>();
            shpParams.put("url", shpFile.toURI().toURL());
            shapefileDataStore = (ShapefileDataStore) dataStoreFactory.createDataStore(shpParams);

            // Set UTF-8 encoding explicitly
            shapefileDataStore.setCharset(java.nio.charset.Charset.forName("UTF-8"));

            // Get shapefile's FeatureCollection
            SimpleFeatureCollection featureCollection = shapefileDataStore.getFeatureSource().getFeatures();

            // Check if raster data file exists
            File tiffFile = new File(tiffPath);
            if (!tiffFile.exists()) {
                System.out.println("GeoTIFF file does not exist: " + tiffPath);
                return;
            }

            // Load raster data (GeoTIFF)
            reader = new GeoTiffReader(tiffFile);
            GridCoverage2D coverage = reader.read(null);

            // Check coordinate system consistency and convert vector data to raster data coordinate system
            CoordinateReferenceSystem rasterCRS = coverage.getCoordinateReferenceSystem2D();
            CoordinateReferenceSystem vectorCRS = shapefileDataStore.getSchema().getCoordinateReferenceSystem();
            System.out.println("Vector CRS before conversion: " + vectorCRS);
            System.out.println("Raster CRS: " + rasterCRS);
            
            if (!CRS.equalsIgnoreMetadata(rasterCRS, vectorCRS)) {
                System.out.println("Coordinate systems do not match, converting vector data to raster coordinate system...");
                featureCollection = new ReprojectingFeatureCollection(featureCollection, rasterCRS);
                CoordinateReferenceSystem newVectorCRS = featureCollection.getSchema().getCoordinateReferenceSystem();
                System.out.println("Vector CRS after conversion: " + newVectorCRS);
            }

            // Check the number of bands in the GeoTIFF file
            int numBands = coverage.getNumSampleDimensions();
            System.out.println("Number of bands in GeoTIFF: " + numBands);
            
            // Ensure band index is in valid range
            if (bandIndex >= numBands || bandIndex < 0) {
                System.out.println("Specified band index is out of range, current index: " + bandIndex + ", total bands: " + numBands);
                return;
            }

            RasterZonalStatistics process = new RasterZonalStatistics();
            SimpleFeatureCollection resultFeatures = process.execute(
                    coverage,      // raster data
                    bandIndex,     // use specified band
                    featureCollection,  // vector regions
                    null           // classification image (optional, not needed here)
            );
            
            System.out.println("\n=== ZONAL STATISTICS RESULTS ===\n");
            
            // Process results - safely with proper resource cleanup
            featureIterator = resultFeatures.features();
            try {
                int regionCount = 1;
                while (featureIterator.hasNext()) {
                    SimpleFeature feature = featureIterator.next();
                    
                    System.out.println("Region #" + regionCount + ":");
                    
                    // Print identification attributes
                    for (int i = 0; i < feature.getType().getAttributeCount(); i++) {
                        String attributeName = feature.getType().getDescriptor(i).getLocalName();
                        if (!attributeName.startsWith("count") && 
                            !attributeName.startsWith("min") && 
                            !attributeName.startsWith("max") && 
                            !attributeName.startsWith("sum") && 
                            !attributeName.startsWith("avg") && 
                            !attributeName.startsWith("std") &&
                            !attributeName.equals("z_Shape_Area") && 
                            !attributeName.equals("z_Shape_Leng") &&
                            !attributeName.equals("z_the_geom")) {
                            
                            Object value = feature.getAttribute(attributeName);
                            if (value != null) {
                                System.out.println("  " + attributeName + ": " + value);
                            }
                        }
                    }
                    
                    // Print requested statistics
                    if (showCount) {
                        Object countValue = feature.getAttribute("count");
                        if (countValue != null) {
                            System.out.println("  count: " + countValue);
                        } else {
                            System.out.println("  count: [not available]");
                        }
                    }
                    
                    if (showAvg) {
                        Object meanValue = feature.getAttribute("avg");
                        if (meanValue != null) {
                            System.out.println("  avg: " + meanValue);
                        } else {
                            System.out.println("  avg: [not available]");
                        }
                    }
                    
                    if (showMin) {
                        Object minValue = feature.getAttribute("min");
                        if (minValue != null) {
                            System.out.println("  min: " + minValue);
                        } else {
                            System.out.println("  min: [not available]");
                        }
                    }
                    
                    if (showMax) {
                        Object maxValue = feature.getAttribute("max");
                        if (maxValue != null) {
                            System.out.println("  max: " + maxValue);
                        } else {
                            System.out.println("  max: [not available]");
                        }
                    }
                    
                    if (showStd) {
                        // Try all possible names for standard deviation
                        Object stdValue = null;
                        String foundName = null;
                        //String[] stdNames = {"std", "stdev", "stddev", "standard_deviation"};
                        String[] stdNames = {"stddev"};
                        for (String stdName : stdNames) {
                            stdValue = feature.getAttribute(stdName);
                            if (stdValue != null) {
                                foundName = stdName;
                                break;
                            }
                        }
                        
                        if (stdValue != null) {
                            System.out.println("  stddev: " + stdValue);
                        } else {
                            // Try to calculate it if we have the necessary values
                            Object countVal = feature.getAttribute("count");
                            Object sumVal = feature.getAttribute("sum");
                            Object sum2Val = feature.getAttribute("sum_2"); 
                            
                            if (sum2Val == null) {
                                sum2Val = feature.getAttribute("sum2");
                            }
                            
                            if (countVal != null && sumVal != null && sum2Val != null) {
                                try {
                                    double count = ((Number) countVal).doubleValue();
                                    double sum = ((Number) sumVal).doubleValue();
                                    double sumOfSquares = ((Number) sum2Val).doubleValue();
                                    
                                    if (count > 1) {
                                        double mean = sum / count;
                                        double variance = (sumOfSquares / count) - (mean * mean);
                                        // Prevent negative variance due to floating-point errors
                                        if (variance >= 0) {
                                            double std = Math.sqrt(variance);
                                            System.out.println("  stddev: " + std + " (calculated)");
                                        } else {
                                            System.out.println("  stddev: [calculation resulted in negative variance]");
                                        }
                                    } else {
                                        System.out.println("  stddev: [not calculable with count <= 1]");
                                    }
                                } catch (Exception e) {
                                    System.out.println("  stddev: [calculation failed: " + e.getMessage() + "]");
                                }
                            } else {
                                System.out.println("  stddev: [not available and not calculable]");
                            }
                        }
                    }
                    
                    // Print attributes with their data types for debugging
                    // System.out.println("\n  Debug: All attributes with types:");
                    // for (int i = 0; i < feature.getType().getAttributeCount(); i++) {
                    //     String attributeName = feature.getType().getDescriptor(i).getLocalName();
                    //     Object attributeValue = feature.getAttribute(i);
                    //     String typeName = attributeValue != null ? attributeValue.getClass().getName() : "null";
                    //     System.out.println("    " + attributeName + " (" + typeName + "): " + attributeValue);
                    // }
                    
                    System.out.println("\n---------------------------\n");
                    regionCount++;
                }
            } finally {
                // Always close the feature iterator
                if (featureIterator != null) {
                    featureIterator.close();
                }
            }
            
            System.out.println("=== END OF ZONAL STATISTICS ===");

        } catch (Exception e) {
            System.out.println("Error in zonalstatics: " + e.getMessage());
            e.printStackTrace();
        } finally {
            // Clean up all resources even if an exception occurs
            try {
                if (featureIterator != null) {
                    featureIterator.close();
                }
                if (reader != null) {
                    reader.dispose();
                }
                if (shapefileDataStore != null) {
                    shapefileDataStore.dispose();
                }
                // Force JVM garbage collection to help release file locks
                System.gc();
            } catch (Exception e) {
                System.out.println("Error closing resources: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
}

end