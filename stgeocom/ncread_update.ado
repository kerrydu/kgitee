program ncread_update
	
version 18
syntax, [gitee]

if "`gitee'" == "" {
	local urls ""
}
else {
    local urls "https://gitee.com/kerrydu/kgitee/raw/master/stgeocom"
}

net install ncread, from(`urls') replace

end