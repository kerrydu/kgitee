cap program drop ncread
program define ncread

cap findfile netcdfAll-5.6.0.jar

if _rc{
    cap findfile path_ncreadjar.ado 
    if _rc {
        di as error "jar path NOT specified, use netcdf_init for setting up"
        exit
        
    }

    path_ncreadjar
    local path `r(path)'

    cap findfile netcdfAll-5.6.0.jar, path(`"`path'"')
    if _rc {
        di as error "netcdfAll-5.6.0.jar NOT found"
        di as error "use netcdf_init for re-initializing Java environment"
        di as error "make sure netcdfAll-5.6.0.jar in your specified directory"
        exit
    }

    qui adopath ++ `"`path'"'

}

ncread_core `0'


end

