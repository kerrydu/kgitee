
cap program drop gtifftocsv 
program define gtifftocsv 

version 18.0

checkdependencies

syntax using/, csv(string) [crscode(string) band(real 1) origin(numlist min=2 max=2 interger >0) size(numlist min=2 max=2 interger)]

if "`crscode'"=="" local crscode None

if "`origin'"==""{
	java: GeoTiffToCsvExporter.main("`using'",`band',"`csvfile'","`crscode'", 0, 0, -1, -1);
	exit
}
else{
	local startRow: word 1 of `origin'
	local startCol: word 2 of `origin'
	local startCol = `startCol'-1
	local startRow = `startRow'-1
	if "`size'"==""{
		local endRow -1
		local endCol -1
	}
	else{
		local endRow : word 1 of `size'
		local endCol : word 2 of `size'
		local endRow = `endRow' + `startRow'
		local endCol = `endCol' + `startCol'
	}
	
	java: GeoTiffToCsvExporter.main("`using'",`band',"`csvfile'","`crscode'",`startRow',`endRow',`startCol',`endCol');
	
}




end

//
//
// java:
//
// /cp gt-metadata-32.0.jar       
// /cp gt-api-32.0.jar
// /cp gt-main-32.0.jar
// /cp gt-referencing-32.0.jar
// /cp gt-epsg-hsql-32.0.jar
// /cp gt-epsg-extension-32.0.jar
// /cp gt-geotiff-32.0.jar
// /cp gt-coverage-32.0.jar
// /cp gt-process-raster-32.0.jar
//
//
//
// import org.geotools.api.referencing.crs.CoordinateReferenceSystem;
// import org.geotools.api.referencing.operation.MathTransform;
// import org.geotools.api.referencing.operation.TransformException;
// import org.geotools.coverage.grid.GridCoverage2D;
// import org.geotools.coverage.grid.GridGeometry2D;
// import org.geotools.coverage.grid.io.GridCoverage2DReader;
// import org.geotools.gce.geotiff.GeoTiffReader;
// import org.geotools.geometry.Position2D;
// import org.geotools.referencing.CRS;
// import org.geotools.util.factory.Hints;
// import org.geotools.coverage.grid.GridCoordinates2D;
// import java.awt.image.Raster;
// import java.io.BufferedWriter;
// import java.io.File;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.nio.file.Files;
// import org.geotools.coverage.GridSampleDimension;
//
//
// public class GeoTiffToCsvExporter {
//     private static final int BUFFER_SIZE = 256 * 1024;
//     private static final int BATCH_SIZE = 8192;
//     private static final double COORDINATE_EPSILON = 1e-8;
//     private static final long MAX_RECORDS = 50_000_000;
//     private static final String DEFAULT_EPSG = "None";
//
//     public static void exportBandToCSV(String geotiffPath, int bandIndex, 
//                                      String outputPath, String targetEpsg) 
//             throws IOException, TransformException {
//         File file = new File(geotiffPath);
//         validateFileExistence(file);
//        
//         GridCoverage2DReader reader = null;
//         try {
//             reader = new GeoTiffReader(file, createReaderHints());
//             GridCoverage2D coverage = (GridCoverage2D) reader.read(null);
//             Raster raster = coverage.getRenderedImage().getData();
//            
//             validateBandIndex(raster, bandIndex);
//             validateFileSize(raster.getWidth(), raster.getHeight());
//            
//             processRasterData(
//                 raster, 
//                 bandIndex - 1, 
//                 coverage.getGridGeometry(),
//                 getNoDataValue(coverage, bandIndex),
//                 outputPath,
//                 createTransform(coverage, targetEpsg)
//             );
//         }finally {
//         if (reader != null) {
//             reader.dispose();
//         }
//        }
// 	}
//
//     private static void validateEpsgCode(String epsgCode) throws TransformException {
//         if (DEFAULT_EPSG.equals(epsgCode)) return;
//         if (!epsgCode.startsWith("EPSG:")) {
//             throw new TransformException("Invalid EPSG format: " + epsgCode);
//         }
//         try {
//             CRS.decode(epsgCode, true);
//         } catch (Exception e) {
//             throw new TransformException("Unsupported EPSG code: " + epsgCode, e);
//         }
//     }
//
//     private static MathTransform createTransform(GridCoverage2D coverage, String targetEpsg) 
//             throws TransformException {
//         if (DEFAULT_EPSG.equals(targetEpsg)) return null;
//        
//         validateEpsgCode(targetEpsg);
//         try {
//             CoordinateReferenceSystem sourceCRS = coverage.getCoordinateReferenceSystem();
//             CoordinateReferenceSystem targetCRS = CRS.decode(targetEpsg, true);
//            
//             if (CRS.equalsIgnoreMetadata(sourceCRS, targetCRS)) {
//                 return null;
//             }
//             return CRS.findMathTransform(sourceCRS, targetCRS, true);
//         } catch (Exception e) {
//             throw new TransformException("坐标转换失败: " + e.getMessage(), e);
//         }
//     }
//
//     private static void validateFileExistence(File file) throws IOException {
//         if (!Files.exists(file.toPath())) {
//             throw new IOException("Input file not found: " + file.getAbsolutePath());
//         }
//     }
//
//     private static Hints createReaderHints() {
//         return new Hints(
//             Hints.FORCE_LONGITUDE_FIRST_AXIS_ORDER, Boolean.TRUE
//         );
//     }
//
//     private static void validateBandIndex(Raster raster, int bandIndex) {
//         if (bandIndex < 1 || bandIndex > raster.getNumBands()) {
//             throw new IllegalArgumentException(String.format(
//                 "Invalid band index: %d (Valid range: 1-%d)",
//                 bandIndex, raster.getNumBands()
//             ));
//         }
//     }
//
//     private static void validateFileSize(int width, int height) throws IOException {
//         long totalPixels = (long) width * height;
//         if (totalPixels > MAX_RECORDS) {
//             throw new IOException(String.format(
//                 "File too large: %,d pixels exceeds maximum limit of %,d records",
//                 totalPixels, MAX_RECORDS
//             ));
//         }
//     }
//
//     private static double getNoDataValue(GridCoverage2D coverage, int bandIndex) {
//         GridSampleDimension band = coverage.getSampleDimensions()[bandIndex - 1];
//         // ... 原有代码不变 ...
//         double[] noDataValues = band.getNoDataValues();
//         return (noDataValues != null && noDataValues.length > 0) ? 
//                noDataValues[0] : Double.NaN;
//     }
//
//     private static void processRasterData(Raster raster, int bandIndex,
//                                         GridGeometry2D gridGeometry,
//                                         double noData, String outputPath,
//                                         MathTransform transform) throws IOException, TransformException {
//         try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath), BUFFER_SIZE)) {
//             writer.write("x,y,value\n");
//             StringBuilder buffer = new StringBuilder(BATCH_SIZE * 20);
//             int recordCount = 0;
//             CRS.reset("all");
//            
//             Position2D reusablePosition = new Position2D();
//             double[] transformSrc = new double[2];
//             double[] transformDst = new double[2];
//
//             for (int y = 0; y < raster.getHeight(); y++) {
//                 for (int x = 0; x < raster.getWidth(); x++) {
//                     double value = raster.getSampleDouble(x, y, bandIndex);
//                     if (isNoData(value, noData)) continue;
//
//                     convertCoordinate(gridGeometry, x, y, transform, 
//                                     reusablePosition, transformSrc, transformDst);
//                     appendCoordinate(buffer, reusablePosition, value);
//                    
//                     if (++recordCount % BATCH_SIZE == 0) {
//                         flushBuffer(writer, buffer);
//                     }
//                 }
//             }
//             flushBuffer(writer, buffer);
//         }
//     }
//
//  
//     // 修改坐标转换方法
//     private static void convertCoordinate(GridGeometry2D gridGeometry, 
//         int x, int y, 
//         MathTransform transform,
//         Position2D reusablePos,
//         double[] transformSrc,
//         double[] transformDst) 
//     throws TransformException {
//         // 修改方法调用方式
//         reusablePos.setLocation(gridGeometry.gridToWorld(new GridCoordinates2D(x, y)));
//     // ... 后续代码不变 ...
//         if (transform != null) {
//             applyCoordinateTransform(reusablePos, transform, 
//                                    reusablePos, transformSrc, transformDst);
//         }
//     }
//
//     private static void applyCoordinateTransform(Position2D srcPos,
//                                                MathTransform transform,
//                                                Position2D destPos,
//                                                double[] transformSrc,
//                                                double[] transformDst) 
//             throws TransformException {
//         transformSrc[0] = srcPos.getX();
//         transformSrc[1] = srcPos.getY();
//         transform.transform(transformSrc, 0, transformDst, 0, 1);
//         destPos.setLocation(transformDst[0], transformDst[1]);
//     }
//
//     private static void appendCoordinate(StringBuilder buffer, 
//                                         Position2D position, 
//                                         double value) {
//         buffer.append(String.format("%.8f,%.8f,%.4f%n",
//             position.getX(),
//             position.getY(),
//             value
//         ));
//     }
//
//     private static void flushBuffer(BufferedWriter writer, StringBuilder buffer) 
//             throws IOException {
//         writer.write(buffer.toString());
//         buffer.setLength(0);
//     }
//
//     private static boolean isNoData(double value, double noData) {
//         return (Double.isFinite(noData) && Math.abs(value - noData) < 1e-9) ||
//                (Double.isNaN(noData) && Double.isNaN(value));
//     }
//
//     public static void main(String inputFile, String outputFile,String targetEpsg,int band) {
//         try {
// //             String inputFile = "E:/qwc_working/SynologyDrive/气候变化_电力消费/灯光数据处理/data/light/DMSP-like2019.tif";
// //             String outputFile = "d:/temp.csv";
// //             String targetEpsg = "None";
//            
//             long startTime = System.nanoTime();
//             exportBandToCSV(inputFile, band, outputFile, targetEpsg);
//             double duration = (System.nanoTime() - startTime) / 1e6;
//            
//             System.out.printf("Processing completed in %.2f ms%n", duration);
//         } catch (IOException e) {
//             handleFileError(e);
//         } catch (TransformException e) {
//             System.err.println("CRS Error: " + e.getMessage());
//         } catch (Exception e) {
//             System.err.println("Processing failed: " + e.getMessage());
//         }
//     }
//
//     private static void handleFileError(IOException e) {
//         if (e.getMessage().contains("exceeds maximum limit")) {
//             System.err.println("Error: " + e.getMessage());
//             System.err.println("Please use chunk processing for large files");
//         } else {
//             System.err.println("File error: " + e.getMessage());
//         }
//     }
// }
//
//
// end



// java: GeoTiffToCsvExporter.main()

////////////////////////////////////////////////////////////////////////


java:

/cp gt-metadata-32.0.jar       
/cp gt-api-32.0.jar
/cp gt-main-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-epsg-extension-32.0.jar
/cp gt-geotiff-32.0.jar
/cp gt-coverage-32.0.jar
/cp gt-process-raster-32.0.jar



import org.geotools.api.referencing.crs.CoordinateReferenceSystem;
import org.geotools.api.referencing.operation.MathTransform;
import org.geotools.api.referencing.operation.TransformException;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridGeometry2D;
import org.geotools.coverage.grid.io.GridCoverage2DReader;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.Position2D;
import org.geotools.referencing.CRS;
import org.geotools.util.factory.Hints;
import org.geotools.coverage.grid.GridCoordinates2D;
import java.awt.image.Raster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import org.geotools.coverage.GridSampleDimension;


public class GeoTiffToCsvExporter {
    private static final int BUFFER_SIZE = 256 * 1024;
    private static final int BATCH_SIZE = 8192;
    private static final double COORDINATE_EPSILON = 1e-8;
    private static final long MAX_RECORDS = 50_000_000;
    private static final String DEFAULT_EPSG = "None";

    public static void exportBandToCSV(String geotiffPath, int bandIndex, 
                                     String outputPath, String targetEpsg,
                                     int startRow,
                                     int endRow,
                                     int startCol,
                                     int endCol) throws IOException, TransformException {
        File file = new File(geotiffPath);
        validateFileExistence(file);
        
        GridCoverage2DReader reader = null;
        try {
            reader = new GeoTiffReader(file, createReaderHints());
            GridCoverage2D coverage = (GridCoverage2D) reader.read(null);
            Raster raster = coverage.getRenderedImage().getData();
            
            validateBandIndex(raster, bandIndex);
            // validateFileSize(raster.getWidth(), raster.getHeight());
            validateCoordinates(startRow, endRow, startCol, endCol, raster.getHeight(), raster.getWidth());
            validateProcessingArea(startRow, endRow, startCol, endCol);
            
            processRasterData(
                raster, 
                bandIndex - 1, 
                coverage.getGridGeometry(),
                getNoDataValue(coverage, bandIndex),
                outputPath,
                createTransform(coverage, targetEpsg),
                startRow,
                endRow,
                startCol,
                endCol
            );
        }finally {
        if (reader != null) {
            reader.dispose();
        }
       }
	}

    private static void validateEpsgCode(String epsgCode) throws TransformException {
        if (DEFAULT_EPSG.equals(epsgCode)) return;
        if (!epsgCode.startsWith("EPSG:")) {
            throw new TransformException("Invalid EPSG format: " + epsgCode);
        }
        try {
            CRS.decode(epsgCode, true);
        } catch (Exception e) {
            throw new TransformException("Unsupported EPSG code: " + epsgCode, e);
        }
    }

    private static MathTransform createTransform(GridCoverage2D coverage, String targetEpsg) 
            throws TransformException {
        if (DEFAULT_EPSG.equals(targetEpsg)) return null;
        
        validateEpsgCode(targetEpsg);
        try {
            CoordinateReferenceSystem sourceCRS = coverage.getCoordinateReferenceSystem();
            CoordinateReferenceSystem targetCRS = CRS.decode(targetEpsg, true);
            
            if (CRS.equalsIgnoreMetadata(sourceCRS, targetCRS)) {
                return null;
            }
            return CRS.findMathTransform(sourceCRS, targetCRS, true);
        } catch (Exception e) {
            throw new TransformException("Coordinate transformation failed: " + e.getMessage(), e);
        }
    }

    private static void validateFileExistence(File file) throws IOException {
        if (!Files.exists(file.toPath())) {
            throw new IOException("Input file not found: " + file.getAbsolutePath());
        }
    }

    private static Hints createReaderHints() {
        return new Hints(
            Hints.FORCE_LONGITUDE_FIRST_AXIS_ORDER, Boolean.TRUE
        );
    }

    private static void validateBandIndex(Raster raster, int bandIndex) {
        if (bandIndex < 1 || bandIndex > raster.getNumBands()) {
            throw new IllegalArgumentException(String.format(
                "Invalid band index: %d (Valid range: 1-%d)",
                bandIndex, raster.getNumBands()
            ));
        }
    }

    private static void validateFileSize(int width, int height) throws IOException {
        long totalPixels = (long) width * height;
        if (totalPixels > MAX_RECORDS) {
            throw new IOException(String.format(
                "File too large: %,d pixels exceeds maximum limit of %,d records",
                totalPixels, MAX_RECORDS
            ));
        }
    }

    private static double getNoDataValue(GridCoverage2D coverage, int bandIndex) {
        GridSampleDimension band = coverage.getSampleDimensions()[bandIndex - 1];
        // ... 原有代码不变 ...
        double[] noDataValues = band.getNoDataValues();
        return (noDataValues != null && noDataValues.length > 0) ? 
               noDataValues[0] : Double.NaN;
    }

    private static void processRasterData(Raster raster, int bandIndex,
                                        GridGeometry2D gridGeometry,
                                        double noData, String outputPath,
                                        MathTransform transform, int startRow, int endRow, int startCol, int endCol) throws IOException, TransformException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath), BUFFER_SIZE)) {
            writer.write("x,y,value\n");
            StringBuilder buffer = new StringBuilder(BATCH_SIZE * 20);
            int recordCount = 0;
            CRS.reset("all");
            // 新增：获取CRS并计算精度
            //CoordinateReferenceSystem crs = gridGeometry.getCoordinateReferenceSystem();
            int precision = getCoordinatePrecision(gridGeometry.getCoordinateReferenceSystem());            
            
            Position2D reusablePosition = new Position2D();
            double[] transformSrc = new double[2];
            double[] transformDst = new double[2];
            if (endCol == -1) endCol = raster.getWidth()-1;
            if (endRow == -1) endRow = raster.getHeight()-1;
            for (int y = startRow; y <= endRow; y++) {
                for (int x = startCol; x <= endCol; x++) {
                    double value = raster.getSampleDouble(x, y, bandIndex);
                    if (isNoData(value, noData)) continue;

                    convertCoordinate(gridGeometry, x, y, transform, 
                                    reusablePosition, transformSrc, transformDst);
                    appendCoordinate(buffer, reusablePosition, value,precision);
                    
                    if (++recordCount % BATCH_SIZE == 0) {
                        flushBuffer(writer, buffer);
                    }
                }
            }
            flushBuffer(writer, buffer);
        }
    }

  
    // 修改坐标转换方法
    private static void convertCoordinate(GridGeometry2D gridGeometry, 
        int x, int y, 
        MathTransform transform,
        Position2D reusablePos,
        double[] transformSrc,
        double[] transformDst) 
    throws TransformException {
        // 修改方法调用方式
        reusablePos.setLocation(gridGeometry.gridToWorld(new GridCoordinates2D(x, y)));
    // ... 后续代码不变 ...
        if (transform != null) {
            applyCoordinateTransform(reusablePos, transform, 
                                   reusablePos, transformSrc, transformDst);
        }
    }

    private static void applyCoordinateTransform(Position2D srcPos,
                                               MathTransform transform,
                                               Position2D destPos,
                                               double[] transformSrc,
                                               double[] transformDst) 
            throws TransformException {
        transformSrc[0] = srcPos.getX();
        transformSrc[1] = srcPos.getY();
        transform.transform(transformSrc, 0, transformDst, 0, 1);
        destPos.setLocation(transformDst[0], transformDst[1]);
    }

    private static void appendCoordinate(StringBuilder buffer, 
                                    Position2D position, 
                                    double value,
                                    int Precision) {
    buffer.append(String.format("%."+Precision+"f, %."+Precision+"f, %.4f%n",
        position.getX(),
        position.getY(),
        value
    ));
}

    private static void flushBuffer(BufferedWriter writer, StringBuilder buffer) 
            throws IOException {
        writer.write(buffer.toString());
        buffer.setLength(0);
    }

    private static boolean isNoData(double value, double noData) {
        return (Double.isFinite(noData) && Math.abs(value - noData) < 1e-9) ||
               (Double.isNaN(noData) && Double.isNaN(value));
    }

    public static void main(String inputFile, 
	                        int band,
							String outputFile,
							String targetEpsg,
							int startRow,
							int endrow,
							int startCol,
							int endCol            ) {
        try {
//             String inputFile = "E:/qwc_working/SynologyDrive/气候变化_电力消费/灯光数据处理/data/light/DMSP-like2019.tif";
//             String outputFile = "d:/temp.csv";
//             String targetEpsg = "None";
            
            long startTime = System.nanoTime();
            exportBandToCSV(inputFile, band, outputFile, targetEpsg,startRow,endRow,startCol,endCol);
            double duration = (System.nanoTime() - startTime) / 1e6;
            
            System.out.printf("Processing completed in %.2f ms%n", duration);
        } catch (IOException e) {
            handleFileError(e);
        } catch (TransformException e) {
            System.err.println("CRS Error: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Processing failed: " + e.getMessage());
        }
    }

    private static void handleFileError(IOException e) {
        if (e.getMessage().contains("exceeds maximum limit")) {
            System.err.println("Error: " + e.getMessage());
            System.err.println("Please use chunk processing for large files");
        } else {
            System.err.println("File error: " + e.getMessage());
        }
    }

    // 在类中添加以下方法
    private static int getCoordinatePrecision(CoordinateReferenceSystem crs) {
        // 默认精度：角度单位用8位，线性单位用3位
        try {
            if (CRS.isGeographic(crs)) {
                return 4;  // 适用于经纬度（WGS84等）
            } else {
                return 2;  // 适用于投影坐标系（UTM等）
            }
        } catch (Exception e) {
            return 3;  // 异常时使用折中精度
        }
    }

    // 修正后：验证实际处理区域
    private static void validateProcessingArea(int startRow, int endRow, 
        int startCol, int endCol) throws IOException {
    long totalPixels = (long)(endRow - startRow + 1) * (endCol - startCol + 1);
        if (totalPixels > MAX_RECORDS) {
        throw new IOException(String.format(
        "Processing area too large: %,d pixels exceeds limit of %,d",
        totalPixels, MAX_RECORDS
        ));
        }
    }

    private static void validateCoordinates(int startRow, int endRow,
                                       int startCol, int endCol,
                                       int maxRow, int maxCol) {
    if (startRow < 0 || startCol < 0) {
        throw new IllegalArgumentException("Start coordinates cannot be negative");
    }
    if (endRow > maxRow || endCol > maxCol) {
        throw new IllegalArgumentException(
            String.format("End coordinates exceed raster dimensions (%d x %d)", 
                         maxRow+1, maxCol+1)
        );
    }
    if (startRow > endRow || startCol > endCol) {
        throw new IllegalArgumentException("Start coordinates exceed end coordinates");
    }
}


}





end


