mata:
mata set matastrict on

real matrix geo_project(real matrix XY, | string scalar s, real scalar rad,
    real vector opts)
{
    pointer (function) scalar f
    
    if (args()<3) rad = 0
    if (cols(XY)!=2) {
         errprintf("{it:XY} must have two columns\n")
         exit(3200)
    }
    (void) _geo_project_find(s, f=NULL)
    return((*f)(XY, rad, opts))
}

string scalar _geo_project_find(string scalar s0, | pointer (function) scalar f)
{
    string scalar    s
    string rowvector list
    
    list = "webmercator", "mercator", "emercator", "miller", "gallstereo",
           "lambert", "behrmann", "hobodyer", "gallpeters", "peters",
           "equirectangular",
           "robinson", "equalearth", "naturalearth",
           "winkeltripel", "hammer",
           "conic", "albers", "lambertconic",
           "orthographic"
    (void) _mm_strexpand(s=s0, strlower(strtrim(s0)), list, "webmercator")
    f = findexternal("geo_project_"+s+"()")
    if (f==NULL) {
        errprintf("projection {bf:%s} not found\n", s0)
        exit(3499)
    }
    return(s) // return expanded name
}

real scalar _geo_project_opt(real vector opts, real scalar j, real scalar def)
{   
    return(length(opts)>=j ? opts[j] : def)
}

real matrix _geo_project_rad(real scalar rad, real matrix X)
{
    if (rad) return(X)
    return(X * (pi()/180))
}

real colvector _geo_project_clip(real colvector x, string scalar lbl,
    real scalar limit, | real scalar offset)
{
    real scalar    min, max, clipped
    real rowvector minmax
    real colvector X, p
    
    // setup
    if (args()<4) offset = 0
    if (limit>=.)  return(x) // nothing to do
    if (offset>=.) return(x) // nothing to do
    max = offset + abs(limit) 
    min = offset - abs(limit)
    // clip data
    minmax = minmax(x)
    clipped = 0
    if (minmax[1]<min) {
        p = selectindex(x:<min)
        X = x
        X[p] = J(length(p), 1, min)
        clipped = 1
    }
    if (minmax[2]>max & minmax[2]<.) {
        p = selectindex(x:>max :& x:<.)
        if (clipped) X[p] = J(length(p), 1, max)
        else {
            X = x
            X[p] = J(length(p), 1, max)
            clipped = 1
        }
    }
    if (clipped) {
        printf("{txt}(using clipped values for %s coordinates" +/*
            */ " outside [%g,%g])\n", lbl, min, max)
        return(X)
    }
    return(x)
}

real matrix geo_project_webmercator(real matrix XY, real scalar rad,
     | real vector opts)
{   // Web Mercator projection based on
    // https://en.wikipedia.org/wiki/Web_Mercator_projection
    // Y clipped at +/- 85.051129
    real scalar    r, x0, yc
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 360/pi() * atan(exp(pi())) - 90) // Y limit
    r  = 256 * 2^_geo_project_opt(opts, 1, 0) / (2*pi()) // zoom
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    return(r * (
        x :+ pi(),
        ln(tan(pi()/4 :+ y/2)) :- pi()
        ))
}

real matrix geo_project_mercator(real matrix XY, real scalar rad,
     | real vector opts)
{   // Mercator projection (spherical earth model) based on
    // https://en.wikipedia.org/wiki/Mercator_projection
    real scalar    r, x0, yc
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    return(r * ( 
        x,
        sign(y) :* ln(tan(pi()/4 :+ abs(y)/2))
        )) // using abs(y) and multiply by sign to prevent missing at -90
}

real matrix geo_project_emercator(real matrix XY, real scalar rad,
     | real vector opts)
{   // Mercator projection (ellipsoid earth model) based on
    // https://en.wikipedia.org/wiki/Mercator_projection
    // inverse flattening parameter according to WGS 84 from
    // https://en.wikipedia.org/wiki/World_Geodetic_System
    // note:
    // - the squared eccentricity is defined as e2 = 1 - (b/a)^2, where a is
    //   the semi-major axis (a = 6378137) and b is the semi-minor axis of the
    //   ellipsoid
    // - the flattening (ellipticity) is defined as f = (a - b) / a, the
    //   inverse flattening as m = 1/f
    // - this implies: e2 = 2*f - f^2 = 2/m - (1/m)^2
    //                 b = a - a*f = a - a/m
    real scalar    r, x0, yc, m, e
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    m  = _geo_project_opt(opts, 3, 298.257223563) // inverse flattening
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    e  = sqrt(2/m - (1/m)^2)
    return(r * ( 
        x,
        sign(y) :* ln(tan(pi()/4 :+ abs(y)/2) :* ((1 :- e * sin(abs(y)))
            :/  (1 :+ e * sin(abs(y)))):^(e/2))
        )) // using abs(y) and multiply by sign to prevent missing at -90
}

real matrix geo_project_miller(real matrix XY, real scalar rad,
     | real vector opts)
{   // Miller cylindrical projection using the formula given at
    // https://en.wikipedia.org/wiki/Miller_cylindrical_projection
    real scalar    r, x0, yc
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    return(r * (
        x,
        (5/4) * ln(tan(pi()/4 :+ (2/5)*y))
        ))
}

real matrix geo_project_gallstereo(real matrix XY, real scalar rad,
     | real vector opts)
{   // Gall stereographic projection using the formula given at
    // https://en.wikipedia.org/wiki/Gall_stereographic_projection
    real scalar    r, x0, yc
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    return(r * (
        x / sqrt(2),
        (1 + sqrt(2)/2) * tan(y/2)
        ))
}

real matrix geo_project_lambert(real matrix XY, real scalar rad,
     | real vector opts)
{   // Lambert cylindrical equal-area projection using the formula given at
    // https://en.wikipedia.org/wiki/Cylindrical_equal-area_projection
    real scalar r, x0, s
    
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    s  = _geo_project_opt(opts, 3, 1) // stretch factor
    return(_geo_project_lambert(XY, rad, r, x0, s))
}

real matrix _geo_project_lambert(real matrix XY, real scalar rad,
     real scalar r, real scalar x0, real scalar s)
{
    real scalar    yc
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90) // Y limit
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    return(r * (sqrt(s) * x, sin(y) / sqrt(s)))
}

real matrix geo_project_behrmann(real matrix XY, real scalar rad,
     | real vector opts)
{   // Behrmann projection using the formula given at
    // https://en.wikipedia.org/wiki/Cylindrical_equal-area_projection
    real scalar r, x0
    
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    return(_geo_project_lambert(XY, rad, r, x0, .75))
}

real matrix geo_project_hobodyer(real matrix XY, real scalar rad,
     | real vector opts)
{   // Hobo–Dyer projection using the formula given at
    // https://en.wikipedia.org/wiki/Cylindrical_equal-area_projection
    real scalar r, x0
    
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    return(_geo_project_lambert(XY, rad, r, x0, cos(37.5 * pi() / 180)^2))
}

real matrix geo_project_gallpeters(real matrix XY, real scalar rad,
     | real vector opts)
{   // Gall–Peters projection using the formula given at
    // https://en.wikipedia.org/wiki/Cylindrical_equal-area_projection
    real scalar r, x0
    
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    return(_geo_project_lambert(XY, rad, r, x0, .5))
}

real matrix geo_project_peters(real matrix XY, real scalar rad,
     | real vector opts)
{
    return(geo_project_gallpeters(XY, rad, opts))
}

real matrix geo_project_equirectangular(real matrix XY, real scalar rad,
     | real vector opts)
{   // Equirectangular projection based on
    // https://en.wikipedia.org/wiki/Equirectangular_projection
    real scalar    y1, r, x0, y0
    real colvector x, y
    
    y1 = _geo_project_rad(rad, _geo_project_opt(opts, 1, 0)) // std parallel
    r  = _geo_project_opt(opts, 2, 1) // radius of earth
    x0 = _geo_project_opt(opts, 3, 0) // central meridian
    y0 = _geo_project_opt(opts, 4, 0) // central parallel
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, XY[,2] :- y0)
    return(r * (cos(y1) * x, y))
}
real matrix geo_project_robinson(real matrix XY, real scalar rad,
     | real vector opts)
{   // robinson projection using linear interpolation; the length of the central
    // meridian will be 1.3523/.8487/pi() = .5071880041 times the equator length;
    // values taken from https://en.wikipedia.org/wiki/Robinson_projection; also
    // see: Ipbuker, C. (2005). A Computational Approach to the Robinson 
    // Projection. Survey Review 38(297): 204–217. DOI:10.1179/sre.2005.38.297.204
    real scalar    r, x0, yc, i, j
    real colvector X, Y, x, y, d
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    X  = r *
         (1,.9986,.9954,.99,.9822,.973,.96,.9427,.9216,.8962,.8679,
          .835,.7986,.7597,.7186,.6732,.6213,.5722,.5322,.5322)'
    Y  = r * 1.3523/.8487 *
         (0,.062,.124,.186,.248,.31,.372,.434,.4958,.5571,.6176,
          .6769,.7346,.7903,.8435,.8936,.9394,.9761,1,1)'
    d  = abs(y) * (180/(5*pi())) // 5 degree steps
    i  = trunc(d)
    d  = d - i
    i  = editmissing(i :+ 1, 1) // (set to arbitrary index if y is missing)
    j  = i :+ 1
    return(((X[i] + d :* (X[j]-X[i])) :* x,
            (Y[i] + d :* (Y[j]-Y[i])) :* sign(y)))
}

real matrix geo_project_equalearth(real matrix XY, real scalar rad,
    | real vector opts)
{   // equal earth projection using the formula given at
    // https://en.wikipedia.org/wiki/Equal_Earth_projection
    real scalar    r, x0, yc, A1, A2, A3, A4
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x = _geo_project_rad(rad, XY[,1] :- x0)
    y = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    A1 = 1.340264; A2 = -0.081106; A3 = 0.000893; A4 = 0.003796
    y = asin(sqrt(3)/2 * sin(y))
    return(r * (
        2/3*sqrt(3)*(x :* cos(y) :/ (9*A4*y:^8 + 7*A3*y:^6 + 3*A2*y:^2 :+ A1)),
        A4*y:^9 +   A3*y:^7 +   A2*y:^3  + A1*y
        ))
}

real matrix geo_project_naturalearth(real matrix XY, real scalar rad,
    | real vector opts)
{   // natural earth projection using the formula in source code at
    // http://www.shadedrelief.com/NE_proj/natural_earth_proj.zip
    // (formula at https://en.wikipedia.org/wiki/Natural_Earth_projection
    // 27may2024, appears wrong)
    real scalar    r, x0, yc, A0, A1, A2, A3, A4, B0, B1, B2, B3, B4
    real colvector x, y, p2, p4
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    A0 =  .8707;   A1 = -.131979; A2 = -.013791; A3 = .003971; A4 = -.001529
    B0 = 1.007226; B1 =  .015085; B2 = -.044475; B3 = .028874; B4 = -.005916
    p2 = y:^2; p4 = p2:^2
    return(r * (
        x :* (A0 :+ p2 :* (A1 :+ p2 :* (A2 :+ p4 :* p2 :* (A3 :+ p2 :* A4)))),
        y :* (B0 :+ p2 :* (B1 :+ p4 :* (B2 :+ B3 :* p2 :+ B4 :* p4)))
        ))
}

real matrix geo_project_winkeltripel(real matrix XY, real scalar rad,
    | real vector opts)
{   // Winkel tripel projection using the formula given at
    // https://en.wikipedia.org/wiki/Winkel_tripel_projection
    real scalar    y1, r, x0, yc
    real colvector x, y, a
    
    yc = _geo_project_rad(!rad, 90)     // Y limit
    y1 = _geo_project_rad(!rad, 180/pi()) * acos(2/pi()) // default for y1
    y1 = _geo_project_opt(opts, 1, y1)  // std parallel
    r  = _geo_project_opt(opts, 2, 1)   // radius of earth
    x0 = _geo_project_opt(opts, 3, 0)   // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    y1 = _geo_project_rad(rad, y1)
    a  = acos(cos(y) :* cos(x/2)) // alpha
    a  = editmissing(sin(a) :/ a, 1) // sinc(alpha)
    return((r/2) * (
        x * cos(y1) + 2 * (cos(y) :* sin(x/2) :/ a),
        y + sin(y) :/ a
        ))
}

real matrix geo_project_hammer(real matrix XY, real scalar rad,
    | real vector opts)
{   // Winkel tripel projection using the formula given at
    // https://en.wikipedia.org/wiki/Winkel_tripel_projection
    real scalar    r, x0, yc
    real colvector x, y, d
    
    yc = _geo_project_rad(!rad, 90)   // Y limit
    r  = _geo_project_opt(opts, 1, 1) // radius of earth
    x0 = _geo_project_opt(opts, 2, 0) // central meridian
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    d  = sqrt(1 :+ cos(y) :* cos(x/2))
    return(r * (
        (2 * sqrt(2)) * (cos(y) :* sin(x/2) :/ d),
        sqrt(2) * (sin(y) :/ d)
        ))
}

real matrix geo_project_conic(real matrix XY, real scalar rad,
     | real vector opts)
{   // Equidistant conic projection using the formula given at
    // https://en.wikipedia.org/wiki/Equidistant_conic_projection
    real scalar    y1, y2, r, x0, y0, yc, n, G, p
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)     // Y limit
    y2 = _geo_project_rad(!rad, 60)     // default for y2
    y1 = _geo_project_opt(opts, 1,  0)  // 1st standard parallel
    y2 = _geo_project_opt(opts, 2, y2)  // 2nd standard parallel
    r  = _geo_project_opt(opts, 3,  1)  // radius of earth
    x0 = _geo_project_opt(opts, 4,  0)  // central meridian
    y0 = _geo_project_opt(opts, 5,  0)  // central parallel
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    y1 = _geo_project_rad(rad, y1)
    y2 = _geo_project_rad(rad, y2)
    y0 = _geo_project_rad(rad, y0)
    n  = y1==y2 ? sin(y1) : (cos(y1) - cos(y2)) / (y2 - y1)
    G  = cos(y1)/n + y1
    p  = G - y0 // rho0
    y  = G :- y // rho
    return(r * (y :* sin(n*x), p :- y :* cos(n*x)))
}

real matrix geo_project_albers(real matrix XY, real scalar rad,
     | real vector opts)
{   // Albers equal-area conic projection using the formula given at
    // https://en.wikipedia.org/wiki/Albers_projection
    real scalar    y1, y2, r, x0, y0, yc, n, C, p
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)     // Y limit
    y2 = _geo_project_rad(!rad, 60)     // default for y2
    y1 = _geo_project_opt(opts, 1,  0)  // 1st standard parallel
    y2 = _geo_project_opt(opts, 2, y2)  // 2nd standard parallel
    r  = _geo_project_opt(opts, 3,  1)  // radius of earth
    x0 = _geo_project_opt(opts, 4,  0)  // central meridian
    y0 = _geo_project_opt(opts, 5,  0)  // central parallel
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    y1 = _geo_project_rad(rad, y1)
    y2 = _geo_project_rad(rad, y2)
    y0 = _geo_project_rad(rad, y0)
    n  = (sin(y1) + sin(y2)) / 2
    C  = cos(y1)^2 + 2*n*sin(y1)
    p  = r/n * sqrt(C - 2*n*sin(y0)) // rho0
    y  = r/n * sqrt(C :- 2*n*sin(y)) // rho
    return((y :* sin(n*x), p :- y :* cos(n*x)))
}

real matrix geo_project_lambertconic(real matrix XY, real scalar rad,
    | real vector opts)
{   // Lambert conformal conic projection using the formula given at
    // https://en.wikipedia.org/wiki/Lambert_conformal_conic_projection
    real scalar    y1, y2, r, x0, y0, yc, n, F, p
    real colvector x, y
    
    yc = _geo_project_rad(!rad, 90)     // Y limit
    y2 = _geo_project_rad(!rad, 60)     // default for y2
    y1 = _geo_project_opt(opts, 1,  0)  // 1st standard parallel
    y2 = _geo_project_opt(opts, 2, y2)  // 2nd standard parallel
    r  = _geo_project_opt(opts, 3,  1)  // radius of earth
    x0 = _geo_project_opt(opts, 4,  0)  // central meridian
    y0 = _geo_project_opt(opts, 5,  0)  // central parallel
    x  = _geo_project_rad(rad, XY[,1] :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc))
    y1 = _geo_project_rad(rad, y1)
    y2 = _geo_project_rad(rad, y2)
    y0 = _geo_project_rad(rad, y0)
    n  = y1==y2 ? sin(y1) :
         ln(cos(y1)/cos(y2)) / ln(tan(pi()/4 + y2/2) / tan(pi()/4 + y1/2))
    F  = cos(y1) * tan(pi()/4 + y1/2)^n / n
    p  = r*F * (1 / tan(pi()/4 + y0/2)):^n  // rho0
    y  = r*F * (1 :/ tan(pi()/4 :+ y/2)):^n // rho
    return((y :* sin(n*x), p :- y :* cos(n*x)))
}

real matrix geo_project_orthographic(real matrix XY, real scalar rad,
    | real vector opts)
{   // Orthographic projection using the formula given at
    // https://en.wikipedia.org/wiki/Orthographic_map_projection
    real scalar    r, x0, y0, xc, yc
    real colvector x, y
    
    xc = _geo_project_rad(!rad, 90)     // X limit
    yc = _geo_project_rad(!rad, 90)     // Y limit
    r  = _geo_project_opt(opts, 1, 1)   // radius of earth
    x0 = _geo_project_opt(opts, 2, 0)   // central meridian
    y0 = _geo_project_opt(opts, 3, 0)   // central parallel
    x  = _geo_project_rad(rad, _geo_project_clip(XY[,1], "X", xc, x0) :- x0)
    y  = _geo_project_rad(rad, _geo_project_clip(XY[,2], "Y", yc, y0))
    y0 = _geo_project_rad(rad, y0)
    return(r * (
        cos(y) :* sin(x),
        cos(y0) :* sin(y) - sin(y0)*cos(y):*cos(x)
        ))
}

end
