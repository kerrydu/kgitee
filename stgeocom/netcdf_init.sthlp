{smcl}
{* 14July2016}{...}
{hline}
help for {hi:netcdf_init}
{hline}

{title:Initializes the environment for ncread/ncdisp}

{cmd:netcdf_init } initializes the environment for reading NetCDF files using the netcdfAll-5.6.0.java library. 
{marker syntax}{...}
{title:Syntax}

{p 4 10 2}
{cmd:readnc_init }[{it:pathofjar}] 
[,{it:options}]

{synoptset 24 tabbed}{...}
{synopthdr}
{synoptline}

{synopt :{opt download}}specify downloading netcdfALL-5.6.0.jar{p_end}
{synopt :{opt dir(string)}}specify the target directory for downloading netcdfALL-5.6.0.jar{p_end}
{synopt :{opt plus(string)}}copy netcdfALL-5.6.0.jar to the specified folder in sysdir_plus{p_end}

{synoptline}
 

{marker examples}{...}
{title:Examples}

{phang}
1. Manually downloading netcdfALL-5.6.0.jar and add the current directory into Stata adopath:

{p 12 16 2}
{cmd:.copy https://downloads.unidata.ucar.edu/netcdf-java/5.6.0/netcdfAll-5.6.0.jar .}{break}

{p 12 16 2}
{cmd:.netcdf_init}{break}

{phang}
2. Manually downloading netcdfALL-5.6.0.jar into D:/jars and add D:/jars into Stata adopath:

{p 12 16 2}
{cmd:.mkdir D:/jars}{break}

{p 12 16 2}
{cmd:.copy https://downloads.unidata.ucar.edu/netcdf-java/5.6.0/netcdfAll-5.6.0.jar D:/jars/netcdfAll-5.6.0.jar}{break}

{p 12 16 2}
{cmd:.netcdf_init D:/jars}{break}

{phang}
3. Automatically downloading netcdfALL-5.6.0.jar into sysdir_plus/jar:

{p 12 16 2}
{cmd:.netcdf_init, download plus(jar)}{break}


{hline}


{title:Authors}
{phang}
{cmd:Kerry Du}, Center for Economic Research, Shandong University, China.{break}
 E-mail: {browse "mailto:kerrydu@sdu.edu.cn":kerrydu@sdu.edu.cn}. {break}


{title:Also see}
{p 7 14 2}Help:  {help ncread}, {helpb ncdisp} (if installed){p_end}
