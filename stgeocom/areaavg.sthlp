{smcl}
{* *! version 1.0  {current_date} }{...}
{vieweralsosee "" "--"}{...}
{vieweralsosee "Install command2" "ssc install command2"}{...}
{viewerjumpto "Syntax" "areaavg##syntax"}{...}
{viewerjumpto "Description" "areaavg##description"}{...}
{viewerjumpto "Options" "areaavg##options"}{...}
{viewerjumpto "Examples" "areaavg##examples"}{...}
{title:Title}

{phang}
{bf:areaavg} {hline 2} Calculate area average of a variable

{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:areaavg} {newvar} = {varname} [{cmd:,} {it:options}]

{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:Main}
{synopt:{opt by(varlist)}}Specify grouping variables{p_end}
{synopt:{opt adjlat(varname)}}Specify latitude variable for adjustment{p_end}
{synoptline}
{p2colreset}{...}

{marker description}{...}
{title:Description}

{pstd}
The {cmd:areaavg} command calculates the area average of a specified variable. It can optionally perform calculations by group and adjust for Earth's curvature based on latitude.

{marker options}{...}
{title:Options}

{dlgtab:Main}

{phang}
{opt by(varlist)} specifies a list of variables to group by. If not specified, one average will be calculated for the entire dataset.

{phang}
{opt adjlat(varname)} specifies a variable containing latitude values to adjust calculations for Earth's curvature. Latitude should be in degrees.

{marker examples}{...}
{title:Examples}

{pstd}Calculate a simple area average of the variable temp:{p_end}
{phang2}{cmd:. areaavg avgtemp = temp}

{pstd}Calculate area average of temp by country:{p_end}
{phang2}{cmd:. areaavg avgtemp = temp, by(country)}

{pstd}Calculate area average of temp, adjusting for latitude:{p_end}
{phang2}{cmd:. areaavg avgtemp = temp, adjlat(latitude)}

{title:Author}

{pstd}Your Name{p_end}
{pstd}Your affiliation{p_end}
{pstd}Your email{p_end}

{title:Also see}

{psee}
Online: {helpb mean}, {helpb egen} (if installed)
{p_end}