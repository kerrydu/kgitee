cap program drop gcsvtoshp
program define gcsvtoshp

version 18.0
checkdependencies  // 使用相同的依赖检查机制

syntax using/, outfile(string) [crscode(string) id(string) autodetect]

// 检查必要参数
if "`crscode'"=="" {
    local crscode "EPSG:4326"  // 默认使用WGS84
    di as text "Note: No CRS specified, using WGS84 (EPSG:4326) as default."
}

// 文件路径标准化处理
local csvfile = subinstr(`"`using'"',"\","/",.)
local outfile = subinstr(`"`outfile'"',"\","/",.)

// 显示操作信息
di as text "Converting CSV coordinates to shapefile..."
di as text "CSV file: `csvfile'"
di as text "Output shapefile: `outfile'"
di as text "Coordinate Reference System: `crscode'"
if "`id'" != "" di as text "Filtering for ID: `id'"

// 调用Java方法处理CSV并导出为shapefile
java: ShapefileCreator.createShapefileFromCSV("`csvfile'", "`outfile'", "`crscode'", "`id'", "`autodetect'"=="autodetect")

di as text "Shapefile creation complete."

end

// 检查依赖项
program define checkdependencies
version 18.0

local jars gt-main-32.0.jar gt-api-32.0.jar gt-shapefile-32.0.jar gt-referencing-32.0.jar gt-epsg-hsql-32.0.jar gt-metadata-32.0.jar

local rc 0
foreach jar in `jars'{
    cap findfile `jar'
    if _rc {
        local rc = 1
    }
}

if `rc'{
    path_geotoolsjar
    local path `r(path)'

    foreach jar in `jars' {
        cap findfile `jar', path(`"`path'"')
        if _rc {
            di as error "`jar' NOT found"
            di as error "Use geotools_init for re-initializing Java environment"
            di as error "Make sure `jar' exists in your specified directory"
            exit
        }
    }

    qui adopath ++ `"`path'"'
}

end

// Java implementation
java:
/cp gt-metadata-32.0.jar
/cp gt-api-32.0.jar
/cp gt-main-32.0.jar
/cp gt-referencing-32.0.jar
/cp gt-epsg-hsql-32.0.jar
/cp gt-shapefile-32.0.jar

import com.stata.sfi.*;
import org.geotools.api.data.*;
import org.geotools.data.shapefile.*;
import org.geotools.data.simple.*;
import org.geotools.feature.simple.*;
import org.geotools.api.feature.*;
import org.geotools.api.feature.simple.*;
import org.locationtech.jts.geom.*;
import org.geotools.api.referencing.crs.*;
import org.geotools.referencing.*;
import java.io.*;
import java.util.*;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.api.feature.type.AttributeDescriptor;

public class ShapefileCreator {
    static {
        // 设置默认编码
        System.setProperty("file.encoding", "UTF-8");
        
        // 设置默认 Locale
        try {
            java.util.Locale.setDefault(java.util.Locale.CHINA);
        } catch (Exception e) {
            // 忽略设置 Locale 的错误
        }
    }
    
    // 主方法：从CSV创建Shapefile
    public static void createShapefileFromCSV(String csvFilePath, String shapefilePath, String crsCode, String idFilter, boolean autoDetect) {
        try {
            SFIToolkit.displayln("Reading CSV file: " + csvFilePath);
            
            // 解析CSV并构建多边形
            Map<String, Map<String, Object>> featureData = parseCSVFile(csvFilePath, idFilter, autoDetect);
            
            if (featureData.isEmpty()) {
                SFIToolkit.errorln("No valid features found in CSV file.");
                return;
            }
            
            SFIToolkit.displayln("Found " + featureData.size() + " unique features to create.");
            
            // 获取坐标参考系统
            CoordinateReferenceSystem crs = null;
            try {
                crs = CRS.decode(crsCode);
                SFIToolkit.displayln("Using CRS: " + crs.getName());
            } catch (Exception e) {
                SFIToolkit.errorln("Invalid CRS code: " + crsCode + ". Using default WGS84.");
                crs = CRS.decode("EPSG:4326");
            }
            
            // 创建Shapefile并写入数据
            createShapefileWithFeatures(shapefilePath, featureData, crs);
            
            SFIToolkit.displayln("Shapefile created successfully: " + shapefilePath);
            
        } catch (Exception e) {
            SFIToolkit.errorln("Error creating shapefile: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    // 解析CSV文件
    private static Map<String, Map<String, Object>> parseCSVFile(String csvFilePath, String idFilter, boolean autoDetect) throws Exception {
        Map<String, Map<String, Object>> features = new HashMap<>();
        Map<String, List<List<Coordinate>>> polygons = new HashMap<>();
        Map<String, String> idToName = new HashMap<>();
        
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFilePath))) {
            String line;
            String[] headers = null;
            int idIndex = -1;
            int nameIndex = -1;
            int polygonIndexIndex = -1;
            int ringTypeIndex = -1;
            int pointIndexIndex = -1;
            int xIndex = -1;
            int yIndex = -1;
            
            // 读取并解析头部
            if ((line = reader.readLine()) != null) {
                headers = parseCsvLine(line);
                for (int i = 0; i < headers.length; i++) {
                    String header = headers[i].trim();
                    if (header.equalsIgnoreCase("ID")) idIndex = i;
                    if (header.equalsIgnoreCase("NAME")) nameIndex = i;
                    if (header.equalsIgnoreCase("POLYGON_INDEX")) polygonIndexIndex = i;
                    if (header.equalsIgnoreCase("RING_TYPE")) ringTypeIndex = i;
                    if (header.equalsIgnoreCase("POINT_INDEX")) pointIndexIndex = i;
                    if (header.equalsIgnoreCase("X")) xIndex = i;
                    if (header.equalsIgnoreCase("Y")) yIndex = i;
                }
            }
            
            // 验证必要的列是否存在
            if (idIndex == -1 || xIndex == -1 || yIndex == -1) {
                throw new Exception("Required columns (ID, X, Y) not found in CSV.");
            }
            
            // 自动检测模式
            if (autoDetect && polygonIndexIndex == -1) {
                SFIToolkit.displayln("Auto-detecting polygon structure...");
                return parseUnstructuredCSV(csvFilePath, idIndex, nameIndex, xIndex, yIndex, idFilter);
            }
            
            // 读取数据行
            while ((line = reader.readLine()) != null) {
                String[] values = parseCsvLine(line);
                
                // 确保有足够的列
                if (values.length <= Math.max(idIndex, Math.max(xIndex, yIndex))) continue;
                
                String id = values[idIndex].trim().replace("\"", "");
                
                // 如果指定了ID过滤器，则跳过不匹配的记录
                if (idFilter != null && !idFilter.isEmpty() && !id.equals(idFilter)) continue;
                
                // 获取名称
                String name = nameIndex >= 0 && values.length > nameIndex ? values[nameIndex].trim().replace("\"", "") : id;
                idToName.put(id, name);
                
                // 获取点坐标
                double x = Double.parseDouble(values[xIndex]);
                double y = Double.parseDouble(values[yIndex]);
                
                // 获取多边形索引
                int polygonIndex = polygonIndexIndex >= 0 && values.length > polygonIndexIndex ? 
                    Integer.parseInt(values[polygonIndexIndex]) : 0;
                
                // 获取环类型
                String ringType = ringTypeIndex >= 0 && values.length > ringTypeIndex ? 
                    values[ringTypeIndex].trim().replace("\"", "") : "exterior";
                
                // 创建or获取多边形列表
                if (!polygons.containsKey(id)) {
                    polygons.put(id, new ArrayList<>());
                }
                
                List<List<Coordinate>> featurePolygons = polygons.get(id);
                
                // 确保多边形列表足够长
                while (featurePolygons.size() <= polygonIndex) {
                    featurePolygons.add(new ArrayList<>());
                }
                
                List<Coordinate> coords = featurePolygons.get(polygonIndex);
                coords.add(new Coordinate(x, y));
            }
            
            // 为每个ID创建几何体和属性
            GeometryFactory factory = new GeometryFactory();
            
            for (String id : polygons.keySet()) {
                List<List<Coordinate>> featurePolygons = polygons.get(id);
                List<Polygon> polyList = new ArrayList<>();
                
                for (List<Coordinate> coords : featurePolygons) {
                    // 确保环是闭合的
                    if (coords.size() > 2) {
                        Coordinate first = coords.get(0);
                        Coordinate last = coords.get(coords.size() - 1);
                        if (!first.equals(last)) {
                            coords.add(new Coordinate(first.x, first.y));
                        }
                        
                        // 创建环
                        LinearRing ring = factory.createLinearRing(coords.toArray(new Coordinate[0]));
                        Polygon poly = factory.createPolygon(ring);
                        polyList.add(poly);
                    }
                }
                
                // 创建几何体
                Geometry geom;
                if (polyList.size() == 1) {
                    geom = polyList.get(0);
                } else if (polyList.size() > 1) {
                    geom = factory.createMultiPolygon(polyList.toArray(new Polygon[0]));
                } else {
                    continue; // 跳过无效几何体
                }
                
                // 创建属性映射
                Map<String, Object> attributes = new HashMap<>();
                attributes.put("geometry", geom);
                attributes.put("ID", id);
                attributes.put("NAME", idToName.get(id));
                
                // 添加到结果集合
                features.put(id, attributes);
            }
        }
        
        return features;
    }
    
    // 解析非结构化CSV (只有ID, NAME, X, Y列)
    private static Map<String, Map<String, Object>> parseUnstructuredCSV(String csvFilePath, int idIndex, int nameIndex, 
                                                                      int xIndex, int yIndex, String idFilter) throws Exception {
        Map<String, Map<String, Object>> features = new HashMap<>();
        Map<String, List<Coordinate>> idToCoordinates = new HashMap<>();
        Map<String, String> idToName = new HashMap<>();
        
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFilePath))) {
            // 跳过头行
            reader.readLine();
            
            String line;
            while ((line = reader.readLine()) != null) {
                String[] values = parseCsvLine(line);
                
                // 确保有足够的列
                if (values.length <= Math.max(idIndex, Math.max(xIndex, yIndex))) continue;
                
                String id = values[idIndex].trim().replace("\"", "");
                
                // 如果指定了ID过滤器，则跳过不匹配的记录
                if (idFilter != null && !idFilter.isEmpty() && !id.equals(idFilter)) continue;
                
                // 获取名称
                String name = nameIndex >= 0 && values.length > nameIndex ? values[nameIndex].trim().replace("\"", "") : id;
                idToName.put(id, name);
                
                // 获取点坐标
                double x = Double.parseDouble(values[xIndex]);
                double y = Double.parseDouble(values[yIndex]);
                
                // 添加到坐标列表
                if (!idToCoordinates.containsKey(id)) {
                    idToCoordinates.put(id, new ArrayList<>());
                }
                idToCoordinates.get(id).add(new Coordinate(x, y));
            }
        }
        
        // 检测并生成多边形
        GeometryFactory factory = new GeometryFactory();
        for (String id : idToCoordinates.keySet()) {
            List<Coordinate> coords = idToCoordinates.get(id);
            
            // 尝试创建有效的多边形
            try {
                // 确保环是闭合的
                if (coords.size() > 2) {
                    Coordinate first = coords.get(0);
                    Coordinate last = coords.get(coords.size() - 1);
                    if (!first.equals(last)) {
                        coords.add(new Coordinate(first.x, first.y));
                    }
                    
                    // 创建多边形
                    LinearRing ring = factory.createLinearRing(coords.toArray(new Coordinate[0]));
                    Polygon poly = factory.createPolygon(ring);
                    
                    // 创建属性映射
                    Map<String, Object> attributes = new HashMap<>();
                    attributes.put("geometry", poly);
                    attributes.put("ID", id);
                    attributes.put("NAME", idToName.get(id));
                    
                    // 添加到结果集合
                    features.put(id, attributes);
                }
            } catch (Exception e) {
                SFIToolkit.displayln("Error creating polygon for ID " + id + ": " + e.getMessage());
            }
        }
        
        return features;
    }
    
    // 创建带有特征的Shapefile
    private static void createShapefileWithFeatures(String shapefilePath, 
                                                 Map<String, Map<String, Object>> features, 
                                                 CoordinateReferenceSystem crs) throws Exception {
        File outputFile = new File(shapefilePath);
        
        // 创建特征类型
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        typeBuilder.setName("region");
        typeBuilder.setCRS(crs);
        typeBuilder.add("the_geom", Polygon.class);
        typeBuilder.add("ID", String.class);
        typeBuilder.add("NAME", String.class);
        
        SimpleFeatureType featureType = typeBuilder.buildFeatureType();
        
        // 创建特征集合
        DefaultFeatureCollection collection = new DefaultFeatureCollection();
        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);
        
        for (String id : features.keySet()) {
            Map<String, Object> attributes = features.get(id);
            
            Geometry geom = (Geometry) attributes.get("geometry");
            
            // 设置属性
            featureBuilder.add(geom);
            featureBuilder.add(attributes.get("ID"));
            featureBuilder.add(attributes.get("NAME"));
            
            // 创建特征
            SimpleFeature feature = featureBuilder.buildFeature(null);
            collection.add(feature);
        }
        
        // 创建数据存储并写入特征
        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();
        Map<String, Object> params = new HashMap<>();
        params.put("url", outputFile.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);
        
        ShapefileDataStore dataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
        dataStore.createSchema(featureType);
        dataStore.setCharset(java.nio.charset.Charset.forName("UTF-8"));
        
        String typeName = dataStore.getTypeNames()[0];
        SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);
        
        if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
            Transaction transaction = new DefaultTransaction("create");
            featureStore.setTransaction(transaction);
            
            try {
                featureStore.addFeatures(collection);
                transaction.commit();
                SFIToolkit.displayln("Successfully wrote " + collection.size() + " features");
            } catch (Exception e) {
                transaction.rollback();
                throw new Exception("Error writing features: " + e.getMessage(), e);
            } finally {
                transaction.close();
            }
        } else {
            throw new Exception("Feature source is not writable");
        }
        
        dataStore.dispose();
    }
    
    // 解析CSV行，处理引号包围的字段
    private static String[] parseCsvLine(String line) {
        List<String> tokens = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        boolean inQuotes = false;
        
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            
            if (c == ',' && !inQuotes) {
                tokens.add(sb.toString());
                sb = new StringBuilder();
            } else if (c == '"') {
                // 处理转义引号 (两个连续引号)
                if (i + 1 < line.length() && line.charAt(i + 1) == '"') {
                    sb.append('"');
                    i++;
                } else {
                    inQuotes = !inQuotes;
                }
            } else {
                sb.append(c);
            }
        }
        
        // 添加最后一个字段
        tokens.add(sb.toString());
        
        return tokens.toArray(new String[0]);
    }
}

end