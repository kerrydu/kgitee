
program define stgeocominit
version 18.0
syntax, [replace]
local path `c(sysdir_plus)' 

if "`replace'"==""{
cap findfile NCtoStata.java,path(`"`path'"')
if _rc ==0 {
    cap findfile NCtoStatabySection.java,path(`"`path'"')
    if _rc ==0 {
        cap findfile NetCDFReader.java,path(`"`path'"')
        if _rc ==0 {
            cap findfile StgridMatchArea.java,path(`"`path'"')
            if _rc ==0 {
                //di "Java classes are already initialized."
                exit
            }
        }
    }
}
}

di "initilizing Java classes..."


findfile NCtoStata.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'NCtoStata.java"',replace
findfile NCtoStatabySection.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'NCtoStatabySection.java"',replace
findfile NetCDFReader.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'NetCDFReader.java"',replace
findfile StgridMatchAera.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'StgridMatchArea.java"',replace


end
