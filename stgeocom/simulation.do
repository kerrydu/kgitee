* Simulation codes for xtplfc
* 3June2020
* To run the simulation codes, two user-written packages outtable and graph2tex should be installed in advance.
* Christopher F Baum & Joao Pedro Azevedo, 2001. "OUTTABLE: Stata module to write matrix to LaTeX table," Statistical Software Components S419501.
* UCLA Statistical Consulting Group. graph2tex, downloadable from https://stats.idre.ucla.edu/stat/stata/ado/analysis

capture log close
clear all
sjlog using xtplfc1, replace
set obs 50
set seed 123456
matrix C = (1,0,0.42\0,1,0.85\0.42,0.85,1)
drawnorm x2f x3f d, corr(C)
generate id = _n
expand 40
bysort id: generate t = _n
xtset id t
generate y = 0
matrix D = (1,0.2,0.8\0.2,1,0\0.8,0,1)
drawnorm x1 x2e x3e, corr(D)
generate x2 = (x2f+x2e)/sqrt(2)
generate x3 = (x3f+x3e)/sqrt(2)
generate z=rnormal()
generate gf=1*x3+ 2*x3^2 - 0.25*(x3)^3
forvalues j=1/500 {
    cap drop e
    cap drop *_sd
    quietly drawnorm e
    quietly replace y = x1 -x2 + z*gf+ d + e
    quietly xtplfc y x1 x2, z(z) u(x3) maxnk(20) generate(fit`j') fast brep(500)
    matrix B=e(b)
    matrix B=B[1,1..2]
    matrix B1=(nullmat(B1)\B)
    matrix V=e(Vs)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V1=(nullmat(V1)\V)
    quietly xtreg y x1 x2 x3 z, fe
    matrix B=e(b)
    matrix B=B[1,1..2]
    matrix B2=(nullmat(B2)\B)
    matrix V=e(V)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V2=(nullmat(V2)\V)
    quietly xtreg y x1 x2 x3 z c.x3#c.z, fe
    matrix B=e(b)
    matrix B=B[1,1..2]
    matrix B3=(nullmat(B3)\B)
    matrix V=e(V)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V3=(nullmat(V3)\V) 
}
* Figure 1
egen av_fit = rowmean(fit*)
egen sd_fit = rowsd(fit*)
generate c = invnormal(1 - (100 - 95) / 200)
generate low = av_fit - c * sd_fit
generate up = av_fit + c * sd_fit
twoway (rarea low up x3, sort(x3) color(gs7)) ///
   (line av_fit x3, sort(x3) color(black) lpattern(solid)) ///
   (line gf x3, color(gs10) sort lpattern(longdash)), ///
   legend(label(1 confidence interval at 95%) label(3 DGP) label(2 average fit) ///
   cols(3) order(3 1 2)) xtitle("X3", height(5)) ytitle("g(X3)", height(5)) scheme(sj) 
graph2tex, epsfile(fig1) caption(Average fit of g(X3) across replications) label(fig1) 
* Table 1
clear
set obs 500
matrix res1=J(3,8,.)
forvalues k=1/3 {
    quietly svmat B`k'
    summarize B`k'1, meanonly
    matrix res1[`k',1]=r(mean)-1
    summarize B`k'2, meanonly
    matrix res1[`k',2]=r(mean)+1
    quietly svmat V`k'
    quietly replace V`k'1=sqrt(V`k'1)
    quietly generate B`k'1_lb=B`k'1-invnormal(0.975)*V`k'1
    quietly generate B`k'1_ub=B`k'1+invnormal(0.975)*V`k'1
    quietly generate CIlen`k'1=B`k'1_ub-B`k'1_lb
    quietly summarize CIlen`k'1, detail
    matrix res1[`k',5]=r(p50)    
    quietly replace V`k'2=sqrt(V`k'2)
    quietly generate B`k'2_lb=B`k'2-invnormal(0.975)*V`k'2
    quietly generate B`k'2_ub=B`k'2+invnormal(0.975)*V`k'2
    quietly generate CIlen`k'2=B`k'2_ub-B`k'2_lb
    quietly summarize CIlen`k'2, detail
    matrix res1[`k',6]=r(p50)    
    quietly generate cov`k'1=(B`k'1_lb<=1 & B`k'1_ub>=1) 
    summarize cov`k'1, meanonly
    matrix res1[`k',7]=r(mean)    
    quietly generate cov`k'2=(B`k'2_lb<=-1 & B`k'2_ub>=-1) 
    summarize cov`k'2, meanonly
    matrix res1[`k',8]=r(mean)    
    quietly replace B`k'1=(B`k'1-1)^2
    summarize B`k'1, meanonly
    matrix res1[`k',3]=r(mean)
    quietly replace B`k'2=(B`k'2+1)^2
    summarize B`k'2, meanonly
    matrix res1[`k',4]=r(mean)
}
outtable using res1, mat(res1) format(%9.4f) replace
sjlog close, replace


///////////////////////////////////////////////////////////////////////////

sjlog using xtplfc2, replace
clear all
set obs 50
set seed 789
generate a=rnormal()
generate id=_n
expand 80
bysort id: generate year=_n
generate x=10*runiform()+0.5*a
generate u=-9+18*runiform()
generate gf=sin(_pi/3*u)
generate y=0
xtset id year
mata: gfmat=J(2000,500,.)
forvalues j=1/500 {
    preserve
    quietly replace y=a+0.3*x+0.3*L2.y+L1.y*gf+sqrt(2)*rnormal() if year>2
    quietly drop if year<41
    quietly generate L_y=L1.y
    quietly generate L2_y=L2.y
    quietly ivxtplfc y L2_y x, zvars(L_y) uvar(u) generate(g) endozflag(1) ///
        maxnknots(20) ivz(L2_y,uflag(1)) fast brep(500)
    quietly putmata gfhat=g_1, replace
    mata: gfmat[.,`j']=gfhat
    matrix B=e(b)
    matrix B=B[1,1..2]
    matrix B1=(nullmat(B1)\B)
    matrix V=e(Vs)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V1=(nullmat(V1)\V)
    quietly ivregress 2sls D.y D.L2.y D.x (D.L.y=L2.y), noconstant
    matrix B=e(b)
    matrix B=B[1,2..3]
    matrix B2=(nullmat(B2)\B)
    matrix V=e(V)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V2=(nullmat(V2)\V)
    quietly ivregress 2sls D.y D.L2.y D.x D.u (D.L.y=L2.y), noconstant
    matrix B=e(b)
    matrix B=B[1,2..3]
    matrix B3=(nullmat(B3)\B)
    matrix V=e(V)
    matrix V=vecdiag(V)
    matrix V=V[1,1..2]
    matrix V3=(nullmat(V3)\V)
    restore
}
* Figure 2
quietly drop if year<41
quietly getmata (gfs*)=gfmat
egen av_fit = rowmean(gfs*)
egen sd_fit = rowsd(gfs*)
generate c = invnormal(1 - (100 - 95) / 200)
generate low = av_fit - c * sd_fit
generate up = av_fit + c * sd_fit
twoway (rarea low up u, sort(u) color(gs7)) ///
   (line av_fit u, sort(u) color(black) lpattern(solid)) ///
   (line gf u, color(gs10) sort lpattern(longdash)), ///
   legend(label(1 confidence interval at 95%) label(3 DGP) label(2 average fit) ///
   cols(3) order(3 1 2)) xtitle("U", height(5)) ytitle("g(U)", height(5)) scheme(sj) 
graph2tex, epsfile(fig2) caption(Average fit of g(U)) label(fig2) 
* Table 2
clear
set obs 500
matrix res2=J(3,8,.)
forvalues k=1/3 {
    quietly svmat B`k'
    summarize B`k'1, meanonly
    matrix res2[`k',1]=r(mean)-0.3
    summarize B`k'2, meanonly
    matrix res2[`k',2]=r(mean)-0.3
    svmat V`k'
    quietly replace V`k'1=sqrt(V`k'1)
    quietly generate B`k'1_lb=B`k'1-invnormal(0.975)*V`k'1
    quietly generate B`k'1_ub=B`k'1+invnormal(0.975)*V`k'1
    quietly generate CIlen`k'1=B`k'1_ub-B`k'1_lb
    quietly summarize CIlen`k'1, detail
    matrix res2[`k',5]=r(p50)
    quietly replace V`k'2=sqrt(V`k'2)
    quietly generate B`k'2_lb=B`k'2-invnormal(0.975)*V`k'2
    quietly generate B`k'2_ub=B`k'2+invnormal(0.975)*V`k'2
    quietly generate CIlen`k'2=B`k'2_ub-B`k'2_lb
    quietly summarize CIlen`k'2, detail
    matrix res2[`k',6]=r(p50)
    quietly generate cov`k'1=(B`k'1_lb<=0.3 & B`k'1_ub>=0.3) 
    summarize cov`k'1, meanonly
    matrix res2[`k',7]=r(mean)
    quietly generate cov`k'2=(B`k'2_lb<=0.3 & B`k'2_ub>=0.3) 
    summarize cov`k'2, meanonly
    matrix res2[`k',8]=r(mean)
    quietly replace B`k'1=(B`k'1-0.3)^2
    summarize B`k'1, meanonly
    matrix res2[`k',3]=r(mean)
    quietly replace B`k'2=(B`k'2-0.3)^2
    summarize B`k'2, meanonly
    matrix res2[`k',4]=r(mean)
}
outtable using res2, mat(res2) format(%9.4f) replace
sjlog close, replace




