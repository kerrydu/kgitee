*! version 1.0 by Kerrydu
* 计算逆距离加权平均
* syntax: invdisavg temperatture, d(distance)  i(city year) gen(citytemperature)
cap program drop invdisavg
program define invdisavg

syntax varname(numeric), Distance(varname) i(varlist) gen(name)

marksample touse
markout `touse' `distance'

tempvar invd w wv

qui gen double `invd' = 1/`distance' if `touse'

qui bys `i': egen double `w' =total(`invd') if `touse'

qui replace `w' = `invd'/`w'  if `touse'

qui gen double `wv' = `varlist'*`w'
qui bys `i': egen double `gen' = total(`wv') if `touse'

end
