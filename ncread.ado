/*
    Program: ncread
    Author: Unknown
    Date: 2024-07-28

    Description:
    This program reads variables from a NetCDF file and imports them into Stata. It supports reading variables by sections using the `origin()` and `count()` options.

    Syntax:
    ncread anything using/ [, NOCheck Count(numlist integer) Origin(numlist integer >0)]

    Arguments:
    - anything: The name of the variable to be created in Stata.
    - using: The path to the NetCDF file.
    - NOCheck: (Optional) Specifies whether to perform checks on the size of the variable. If specified, the program will not check the size and will import the variable as a single observation.
    - Count: (Optional) Specifies the number of elements to read along each dimension. Must be used with the `Origin` option.
    - Origin: (Optional) Specifies the starting index along each dimension. Must be used with the `Count` option.

    Example:
    ncread myvar using "path/to/file.nc", NOCheck Count(10 20) Origin(1 1)

    Note:
    - This program requires the "netcdfAll-5.6.0.jar" file to be in the Stata classpath.
    - The variable size limit is set to 50000. If the size exceeds this limit, it is recommended to read variables by sections using the `origin()` and `count()` options.
    - The `NOCheck` option allows ignoring the size limit, but it is not recommended and should be used with caution.
*/
*! 2024-07-28
cap program drop ncread
program define ncread,rclass
version 18
local path `c(sysdir_plus)' 
cap findfile netcdfAll-5.6.0.jar,path(`"`path'"')
if _rc {
    ncreadinit
}

syntax anything using/ , [NOCheck Count(numlist integer) Origin(numlist integer >0 ADDaxes]
removequotes,file(`anything')
local varname `r(file)'
confirm new var `varname'
removequotes,file(`using')
local file `r(file)'
di _n 

if "`origin'"!=""{
    ncread2 `0'
    exit
}

if `=_N'!=0{
    di as error "no; dataset in memory is not empty."
    exit
}

java clear
java: /cp "netcdfAll-5.6.0.jar"
java: /open "StNetCDFReader.java"

java: NetCDFReader.printVarStructure("`file'","`varname'");
if "`nocheck'"==""{
    local dimensions `dimensions'
    if "`dimensions'"=="" exit
    local coordinates `coordinates'
    local size  = 1 
    foreach dim of local dimensions {
        local size = `size' * `dim'
    }

    if(`size'>50000) {
        di "The size of the variable exceeds 50000, recommend read variables by sections with origin() and count() options."
        di "You can ignore this limitation by specifying nocheck option. It is not recommended and should be used with cautions"
        exit
    }

    qui set obs `size'
}
else qui set obs 1

local datatype `datatype'
local flag = ustrpos("`datatype'","str") +  ustrpos("`datatype'","char") + ustrpos("`datatype'","Str")

if `flag' == 0  qui gen double `varname' = .
else qui gen str `varname' = ""

java: NetCDFReader.writeVariableToStata("`file'","`varname'")

if "`addaxes'"!="" & "`coordinates'"!=""{
    tempname tempfr
    qui pwf
    local pwf `r(pwf)'
    qui frame create `tempfr' 
    frame `tempfr' {
        foreach coord of local coordinates {
            qui gen `coord' = .
            java: NetCDFReader.writeVariableToStata("`file'","`coord'")
            qui putmata `coord' = `coord', replace
            qui drop `coord'
        } 
    }
    local nc: word count `coordinates'
    if `nc'==1 {
        qui getmata `coordinates' = `coordinates'
        order `coordinates'
        cap mata mata drop `coordinates'
    }
    else{
        gettoken first coordinates : coordinates
        gettoken next coordinates : coordinates
        genaxis `first' `next'
        local vars `first'`next'
        local first `first'`next'
        cap mata mata drop `first' `next'
        while "`coordinates'"!="" {
            gettoken next coordinates : coordinates
            genaxis  `next' _a_b
            local vars `vars' `next'
            cap mata mata drop `next'
        }
        qui getmata  (`vars') = _a_b
        order `vars'
        cap mata mata drop _a_b 
    }
    
}
end

cap program drop removequotes
program define removequotes,rclass
version 16
syntax, file(string) 
return local file `file'
end


*! 2024-07-29

*! 2024-07-28
cap program drop ncread2
program define ncread2,rclass
version 18
syntax anything using/ , [NOCheck Count(numlist integer) ADDaxes ] Origin(numlist integer >0) 
removequotes,file(`anything')
local varname `r(file)'
confirm new var `varname'
removequotes,file(`using')
local file `r(file)'

if `=_N'!=0{
    di as error "no; dataset in memory is not empty."
    exit
}

java clear
java: /cp "netcdfAll-5.6.0.jar"
java: /open "StNetCDFReader.java"

local no: word count `origin'
if "`count'"==""{
    forv j=1/`no'{
        local count `count' -1
    }
}
local nc: word count `count'
if `nc' != `no' {
    di as error "The number of origin and count should be the same."
    exit
}


local size = 1
di _n 

java: NetCDFReader.printVarStructure("`file'","`varname'")

if "`nocheck'"==""{

local dimensions `dimensions'
if "`dimensions'"=="" exit

local nd: word count `dimensions'
if `nc' != `nd' {
    di as error "The number of origin and count should be equal # of axises."
    exit
}

local size = 1
local count2 
forv i =1/`no'{
    local oi: word `i' of `origin'
    local di : word `i' of `dimensions'
    if  `di' < `oi' {
        di "The origin should be less than the corresponding dimension lenth."
        exit
    }
    // java is zoro-based, so the origin should be minus 1
    local origin0 `origin0' `=`oi'-1'
    local ci: word `i' of `count'
    local size = `size' * `ci'
    local endi = `oi' + `ci'-1
    if (`endi'> `di'){
        di as error "Requested section is out of range"
        di as error "(`origin') + (`count') - 1 > (`dimensions')" 
        exit
    }
    if (`ci'==-1) local count2 `count2' `=`di'-`oi'+1'
    else local count2 `count2' `ci'

}

 local count `count2'

if(`size'>50000) {
    di "The size of the variable exceeds 50000, recommend read variables with a samll section."
    exit
}
}
else{

    forv i =1/`no'{
        local oi: word `i' of `origin'
        // java is zoro-based, so the origin should be minus 1
        local origin0 `origin0' `=`oi'-1'
        local ci: word `i' of `count'
        local size = `size' * `ci'
    }


}


qui set obs `size'
local datatype `datatype'
local flag = ustrpos("`datatype'","str") +  ustrpos("`datatype'","char") + ustrpos("`datatype'","Str")

if `flag' == 0  qui gen double `varname' = .
else qui gen str `varname' = ""

java: NetCDFReader.writeVarsubsetToStata("`file'","`varname'","`origin0'","`count'")

if "`addaxes'"!="" & "`coordinates'"!=""{
    tempname tempfr
    qui pwf
    local pwf `r(pwf)'
    qui frame create `tempfr' 
    frame `tempfr' {
        foreach coord of local coordinates {
            qui gen `coord' = .
            java: NetCDFReader.writeVariableToStata("`file'","`coord'")
            qui putmata `coord' = `coord', replace
            qui drop `coord'
        } 
    }
    local nc: word count `coordinates'
    if `nc'==1 {
        qui getmata `coordinates' = `coordinates'
        order `coordinates'
        cap mata mata drop `coordinates'
    }
    else{
        gettoken first coordinates : coordinates
        gettoken next coordinates : coordinates
        genaxis `first' `next'
        local vars `first'`next'
        local first `first'`next'
        cap mata mata drop `first' `next'
        while "`coordinates'"!="" {
            gettoken next coordinates : coordinates
            genaxis  `next' _a_b
            local vars `vars' `next'
            cap mata mata drop `next'
        }
        qui getmata  (`vars') = _a_b
        order `vars'
        cap mata mata drop _a_b 
    }
    
}



end


program define genaxis
version 16

args a b 
mata {
    _n_1 = rows(`a')
    _n_2 = rows(`b')
    _a_b = `a'#J(_n_2,1,1), J(_n_1,1,1)#`b'
}
//qui getmata (`a' `b') = ab
//cap mata mata drop ab _n_1 _n_2
end


program define ncreadinit

findfile netcdfAll-5.6.0.jar
local fn  `"`r(fn)'"'
local path `c(sysdir_plus)' 
copy `fn' `"`path'/netcdfAll-5.6.0.jar"',replace
findfile StNetCDFReader.java
local fn  `"`r(fn)'"'
copy `fn' `"`path'/StNetCDFReader.java"',replace

end