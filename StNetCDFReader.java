import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import java.io.IOException;
import java.util.List;
import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import com.stata.sfi.*;

public class NetCDFReader {

    public static void printNetCDFStructure(String ncFileName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            // 打印文件全局属性
            System.out.println("Global Attributes:");
            for (Attribute attr : netcdfDataset.getGlobalAttributes()) {
                System.out.println(attr.toString());
            }

            // 打印维度信息
            System.out.println("\nDimensions:");
            for (Dimension dim : netcdfDataset.getDimensions()) {
                System.out.println(dim.getName() + " (" + dim.getLength() + ")");
            }

            // 打印变量信息
            System.out.println("\nVariables:");
            for (Variable var : netcdfDataset.getVariables()) {
                System.out.println("Variable Name: " + var.getFullName());
                System.out.println("Dimensions: " + var.getDimensionsString());
                System.out.println("Attributes:");
                for (Attribute attr : var.getAttributes()) {
                    System.out.println("  " + attr.getName() + ": " + attr.getStringValue());
                }
                System.out.println();
            }

            // 打印组信息
            System.out.println("\nGroups:");
            for (Group group : netcdfDataset.getRootGroup().getGroups()) {
                System.out.println("Group Name: " + group.getFullName());
                System.out.println("Dimensions:");
                for (Dimension dim : group.getDimensions()) {
                    System.out.println("  " + dim.getName() + " (" + dim.getLength() + ")");
                }
                System.out.println("Variables:");
                for (Variable var : group.getVariables()) {
                    System.out.println("  Variable Name: " + var.getFullName());
                    System.out.println("  Dimensions: " + var.getDimensionsString());
                    System.out.println("  Attributes:");
                    for (Attribute attr : var.getAttributes()) {
                        System.out.println("    " + attr.getName() + ": " + attr.getStringValue());
                    }
                }
                System.out.println();
                netcdfDataset.close();
            }
        } catch (IOException e) {
            System.err.println("Error opening NetCDF file: " + e.getMessage());
        } 
    }


    public static void printVarStructure(String ncFileName,String variableName) {
        try (NetcdfDataset netcdfDataset = NetcdfDataset.openDataset(ncFileName)) {
            Variable variable = netcdfDataset.findVariable(variableName);
            if (variable == null) {
                System.out.println("Variable " + variableName + " not found in the file.");
                netcdfDataset.close();
                return;
            }
            //System.out.println("Variable Name: ");
            // 打印变量信息
            System.out.println("Variable Name: " + variable.getNameAndDimensions());
            System.out.println("Variable Attributes:");
            variable.getAttributes().forEach(attribute -> 
                System.out.println(attribute.getName() + ": " + attribute.getStringValue()));

            // 检查并打印坐标轴信息
            String coordinateAxesInfo = variable.getDimensions().stream()
                .map(dimension -> dimension.getName())
                .reduce((a, b) -> a + " " + b)
                .orElse("No coordinate axes");

            System.out.println("Coordinate Axes: " + coordinateAxesInfo);

            // 将坐标轴信息设置为Stata的local宏
            Macro.setLocal("coordAxes", coordinateAxesInfo);
            //Macro.setLocal("coordAxes2", coordinateAxesInfo);
            // 获取变量各个维度的长度
            int[] shape = variable.getShape();
            for (int i = 0; i < shape.length; i++) {
                System.out.println("Dimension " + i + " length: " + shape[i]);
            }
            // 将各个维度的长度返回到stata的local dimensions中
            String dimensions = "";
            for (int i = 0; i < shape.length; i++) {
                dimensions += shape[i] + " ";
            }
            Macro.setLocal("dimensions", dimensions);

            //获取变量数据类型
            System.out.println("Data Type: " + variable.getDataType());
            Macro.setLocal("datatype", variable.getDataType().toString());
            netcdfDataset.close();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        } 
    }
    
    public static void writeVariableToStata(String filePath, String variableName) {
        try {
            //NetcdfFile ncfile = NetcdfFile.open(filePath);
            NetcdfDataset ncfile = NetcdfDataset.openDataset(filePath);
            Variable var = ncfile.findVariable(variableName);
            
            if (var != null) {
                Array data = var.read();
                IndexIterator iter = data.getIndexIterator();
                
                // Get the total number of elements in the array
                long size = data.getSize();
                
                // Resize Stata dataset to accommodate the data
                Data.setObsTotal(size);
                
                // Write data to Stata's "tas" variable
                long i = 1;
                while (iter.hasNext()) {
                    double value = iter.getDoubleNext();
                    Data.storeNumFast(1, i, value);
                    i++;
                }
                
                // System.out.println("Data written to Stata variable 'tas'.");
            } else {
                System.out.println("Variable " + variableName + " not found in the file.");
            }
            ncfile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

    public static void writeVarsubsetToStata(String filePath, String variableName, String strorigin, String strsize) {
        try {
            //NetcdfFile ncfile = NetcdfFile.open(filePath);
            NetcdfDataset ncfile = NetcdfDataset.openDataset(filePath);
            Variable var = ncfile.findVariable(variableName);
            String[] originStr = strorigin.split(" ");
            String[] sizeStr =   strsize.split(" ");
            int[] origin = new int[originStr.length];
            int[] size = new int[sizeStr.length];
            
            for (int i = 0; i < originStr.length; i++) {
                origin[i] = Integer.parseInt(originStr[i]);
            }
            
            for (int i = 0; i < sizeStr.length; i++) {
                size[i] = Integer.parseInt(sizeStr[i]);
            }            
            if (var != null) {
                Array data = var.read(origin, size);
                IndexIterator iter = data.getIndexIterator();
                
                // Get the total number of elements in the array
                long totobs = data.getSize();
                
                // Resize Stata dataset to accommodate the data
                Data.setObsTotal(totobs);
                
                // Write data to Stata's "tas" variable
                long i = 1;
                while (iter.hasNext()) {
                    double value = iter.getDoubleNext();
                    Data.storeNumFast(1, i, value);
                    i++;
                }
                
                // System.out.println("Data written to Stata variable 'tas'.");
            } else {
                System.out.println("Variable " + variableName + " not found in the file.");
            }
            ncfile.close();
        } catch (IOException | InvalidRangeException  e) {
            e.printStackTrace();
        }
        //ncfile.close(); 
    }
    
    
}






