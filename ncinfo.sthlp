{smcl}
{*}
{hline}
{title:Title}
{phang}
{bf:ncinfo} {hline 2} Provide information about a NetCDF file

{hline}

{title:Syntax}
{phang}
{cmd:ncinfo} {it:filename}

{title:Description}
{pstd}
{cmd:ncinfo} provides information about a NetCDF file. It reads the file path provided as an argument and prints the structure of the NetCDF file.

{title:Arguments}
{phang}
{it:filename} specifies the file path of the NetCDF file.

{title:Examples}
{phang}
{cmd:. ncinfo "path/to/netcdf/file.nc"}

{title:Author}
{phang}
Unknown

{title:Date}
{phang}
2024-07-28